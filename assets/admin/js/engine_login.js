$(function(){
	
	$('#form_login').submit(function(e){
		e.preventDefault();
	});
			
	$('#btn_login').click(function(){
		$.ajax({
			url: '/admin/login/auth/',
			type: 'post',
			data: $('#form_login').serialize(),
			success: function(data){
				
				if(data=='logado'){
					window.location.href = '/admin/';
				} else {					
					alert('Usuário ou senha incorretos. Tente novamente!');
				}
			
			}
		
		});
	});
	
	$('#btn_esqueceu_senha').click(function(){
		
		email = $("input[name='email']").val();
		
		if (email == "") {
			
			alert("Por favor, digite seu e-mail.");
		
		} else {
			
			$.ajax({
				
				url: '/admin/login/reenvia_senha/',
				type: 'post',
				data: {"email": email},
				success: function(data){
						
					if (data == "senha_enviada") {
						alert('A senha foi enviada para o seu e-mail!');
					}
					
					if (data == "email_nao_encontrado") {
						alert('E-mail não encontrado :(');
					}
						
				}
			
			});			
			
		}
	
	});		

});