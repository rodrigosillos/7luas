$(function(){
	
	$("input[name='bt_novo']").click(function(){
		window.location.href = '/admin/banners/novo/';
	});	
	
	$("input[name='bt_editar']").click(function(){
		produto_id = $(this).attr("rel");
		window.location.href = '/admin/banners/editar/' + produto_id;
	});
	
	$("input[name='bt_excluir']").click(function(){
		alert("Você não tem permissão.");
	});
	
	
	$("form[name='form_banner']").submit(function(e){
	
		var erro = false;
		
		$('input.obrigatorio').each(function(){
			
			var input_name = $(this).attr("name");
			
			if($(this).val()==''){
				alert('Campo "'+$(this).attr('title')+'" obrigatório!');
				$(this).focus();
				erro = true;
			}
		
        });
        
 		if(erro){
			e.preventDefault();
		}        
        
    });
    
});