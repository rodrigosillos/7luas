$(function(){	
	
	$("#religiao").change(function(){
		$("form[name='form_produtos']").attr("action", "/admin/produtos/");
		$("form[name='form_produtos']").submit();
	});
	
	$("#btn_alterar").click(function(){
		
		var acao = $("#acao").val();
		
		if (acao) {
			
			var produto_sel = $('input[name="cbx_item[]"]:checked').length;
			
			if (produto_sel == 0) {
				
				alert("Por favor, selecione no mínimo um produto.");
			
			} else {
			
				$("form[name='form_produtos']").attr("action", "/admin/produtos/excluir_promocao/");
				$("form[name='form_produtos']").submit();
				
			}
        }

	});
	
	$("#btn_relatorio").click(function(){
		
		var produto_sel = $('input[name="cbx_item[]"]:checked').length;
		
		if (produto_sel == 0) {
		
			alert("Por favor, selecione no mínimo um produto.");
		
		} else {
			
			$("form[name='form_produtos']").attr("action", "/admin/produtos/gerar_excel/");
			$("form[name='form_produtos']").submit();
		}
	
	});	
	
	$("input[name='btn_excluir_imagem']").click(function(){
		
		imagem_id = $(this).attr("rel");
		
		var acao = confirm("Deseja excluir a imagem?");
		
		if (acao == true) {

			$.ajax({
				url: "/admin/produtos/excluir_imagem/",
				type: "post",
				data: {"imagem_id": imagem_id},
				success: function(data){
					alert("Imagem excluída!");
					window.location.reload();
				}
				
			});		
		
		}	
		
	});	
	
	$("input[name='btn_adicionar_imagem']").click(function(){
		produto_id = $(this).attr("rel");
		window.location.href = '/admin/produtos/imagens/' + produto_id;
	});	
	
	$("input[name='btn_voltar_imagem']").click(function(){
		window.location.href = '/admin/produtos/';
	});	
	
	$("input[name='btn_novo']").click(function(){
		window.location.href = '/admin/produtos/novo/';
	});	
	
	$("input[name='btn_editar']").click(function(){
		produto_id = $(this).attr("rel");
		window.location.href = '/admin/produtos/editar/' + produto_id;
	});
	
	$("input[name='btn_excluir']").click(function(){
		
		var acao = confirm("Deseja excluir o produto?");
		
		if (acao == true) {
		
			produto_id = $(this).attr("rel");
			window.location.href = '/admin/produtos/excluir/' + produto_id;		
		
		}
	});
	
	$("input[name='btn_imagens']").click(function(){
		produto_id = $(this).attr("rel");
		window.location.href = '/admin/produtos/imagens/' + produto_id;
	});
	
	$("form[name='form_produto_imagens']").submit(function(e){
	
		var erro = false;
		
		if($('input[name="secundaria"]:checked').length > 0){
			$('input[rel="55x67"]').removeClass("obrigatorio");
			$('input[rel="140x180"]').removeClass("obrigatorio");
			$('input[rel="220x270"]').removeClass("obrigatorio");
			$('input[rel="300x180"]').removeClass("obrigatorio");
			$('input[rel="430x528"]').removeClass("obrigatorio");
		}
		
		$('input.obrigatorio').each(function(){
			
			var input_name = $(this).attr("name");
			
			if($(this).val()==''){
				alert('Imagem "'+$(this).attr('title')+'" obrigatória!');
				$(this).focus();
				erro = true;
			}
		});	
		
		if(erro){
			e.preventDefault();
		}   
	
	});	
	
	
	$("form[name='form_produto']").submit(function(e){
	
		var erro = false;
		
		$('input.obrigatorio').each(function(){
			
			var input_name = $(this).attr("name");
			
			if($(this).val()==''){
				alert('Campo "'+$(this).attr('title')+'" obrigatório!');
				$(this).focus();
				erro = true;
			}
		});
		
		$('textarea.obrigatorio').each(function(){
			
			var input_name = $(this).attr("name");
			
			if($(this).val()==''){
				alert('Campo "'+$(this).attr('title')+'" obrigatório!');
				$(this).focus();
				erro = true;
			}
		});
		
		$('select.obrigatorio').each(function(){
			
			var input_name = $(this).attr("name");
			
			if($(this).val()==''){
				alert('Campo "'+$(this).attr('title')+'" obrigatório!');
				$(this).focus();
				erro = true;
			}
		});		
		
		if(erro){
			e.preventDefault();
		}   
	
	});
	
	$("select[name='categoria_id']").change(function(){
	
		categoria_id = $(this).val();
		
		$.ajax({
			url: "/admin/produtos/get_subcategoria/",
			type: "post",
			data: {"categoria_id": categoria_id},
			success: function(data){
			
				$("select[name='subcategoria_id']").html("");
				$("select[name='subcategoria_id']").attr("disabled", false);
				$("select[name='subcategoria_id']").append(data);
			
			}
			
		});
	
	});	

});