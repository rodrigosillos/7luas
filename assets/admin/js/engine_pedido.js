$(function(){	
	
	$("#status").change(function(){
		$("form[name='form_pedidos']").attr("action", "/admin/pedidos/");
		$("form[name='form_pedidos']").submit();
	});
	
	$("input[name='btn_detalhes']").click(function(){
		pedido_id = $(this).attr("rel");
		window.location.href = '/admin/pedidos/detalhes/' + pedido_id;
	});		
	
	$("#btn_alterar").click(function(){
		
		var status = $("#acao_status").val();
		
		if (status) {
			
			var pedido_sel = $('input[name="cbx_item[]"]:checked').length;
			
			if (pedido_sel == 0) {
				
				alert("Por favor, selecione no mínimo um pedido.");
			
			} else {
			
				$("form[name='form_pedidos']").attr("action", "/admin/pedidos/alterar_status/");
				$("form[name='form_pedidos']").submit();
				
			}
        }

	});
	
	$("#btn_relatorio").click(function(){
		
		var pedido_sel = $('input[name="cbx_item[]"]:checked').length;
		
		if (pedido_sel == 0) {
		
			alert("Por favor, selecione no mínimo um pedido.");
		
		} else {
			
			$("form[name='form_pedidos']").attr("action", "/admin/pedidos/gerar_excel/");
			$("form[name='form_pedidos']").submit();
		}
	
	});	
    
});