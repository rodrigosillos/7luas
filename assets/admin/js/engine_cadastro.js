$(function(){
	
	$(".numero").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			// Allow: Ctrl+A
			(e.keyCode == 65 && e.ctrlKey === true) || 
			// Allow: home, end, left, right
			(e.keyCode >= 35 && e.keyCode <= 39)) {
			// let it happen, don't do anything
				return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
  });
		
	//var re_email = new RegExp(/^[a-zA-Z0-9._%+-]+@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,4}$/gi);
	//if(!email.match(reEmail)) {	}
	
	/*
	function calc_digitos_posicoes( digitos, posicoes = 10, soma_digitos = 0 ) {
	 
		// Garante que o valor é uma string
		digitos = digitos.toString();
 
		// Faz a soma dos dígitos com a posição
		// Ex. para 10 posições:
		//   0    2    5    4    6    2    8    8   4
		// x10   x9   x8   x7   x6   x5   x4   x3  x2
		//   0 + 18 + 40 + 28 + 36 + 10 + 32 + 24 + 8 = 196
		for ( var i = 0; i < digitos.length; i++  ) {
				// Preenche a soma com o dígito vezes a posição
				soma_digitos = soma_digitos + ( digitos[i] * posicoes );
 
				// Subtrai 1 da posição
				posicoes--;
 
				// Parte específica para CNPJ
				// Ex.: 5-4-3-2-9-8-7-6-5-4-3-2
				if ( posicoes < 2 ) {
						// Retorno a posição para 9
						posicoes = 9;
				}
		}
 
		// Captura o resto da divisão entre soma_digitos dividido por 11
		// Ex.: 196 % 11 = 9
		soma_digitos = soma_digitos % 11;
 
		// Verifica se soma_digitos é menor que 2
		if ( soma_digitos < 2 ) {
				// soma_digitos agora será zero
				soma_digitos = 0;
		} else {
				// Se for maior que 2, o resultado é 11 menos soma_digitos
				// Ex.: 11 - 9 = 2
				// Nosso dígito procurado é 2
				soma_digitos = 11 - soma_digitos;
		}
 
		// Concatena mais um dígito aos primeiro nove dígitos
		// Ex.: 025462884 + 2 = 0254628842
		var cpf = digitos + soma_digitos;
 
		// Retorna
		return cpf;
			
	} // calc_digitos_posicoes	
	
	function valida_cpf( valor ) {
	 
			// Garante que o valor é uma string
			valor = valor.toString();
			
			// Remove caracteres inválidos do valor
			valor = valor.replace(/[^0-9]/g, '');
	 
	 
			// Captura os 9 primeiros dígitos do CPF
			// Ex.: 02546288423 = 025462884
			var digitos = valor.substr(0, 9);
	 
			// Faz o cálculo dos 9 primeiros dígitos do CPF para obter o primeiro dígito
			var novo_cpf = calc_digitos_posicoes( digitos );
	 
			// Faz o cálculo dos 10 dígitos do CPF para obter o último dígito
			var novo_cpf = calc_digitos_posicoes( novo_cpf, 11 );
	 
			// Verifica se o novo CPF gerado é idêntico ao CPF enviado
			if ( novo_cpf === valor ) {
					// CPF válido
					return true;
			} else {
					// CPF inválido
					return false;
			}
			
	}
	*/
	
	$("form[name='form_meu_cadastro']").submit(function(e){
	
		var erro = false;
		var base_url = $('#base_url').val();
		
		$('input.obrigatorio').each(function(){
			
			var input_name = $(this).attr("name");
			
			/*
			if(input_name == "cpf"){
				if(valida_cpf($(this).val())){
					erro = false;
				} else {
					alert('Campo "CPF" inválido!');
					erro = true;
				}
			}
			*/
		
			if(input_name == "email"){
			
				email = $(this).val();
				
				if (email != "") {
					
					$.ajax({
						
						url: base_url + "meu_cadastro/verifica_email",
						type: "post",
						data: {"email": email},
						success: function(data){
							if (data == "email_cadastrado") {
								alert('"E-mail" já cadastrado!');
								erro = true;
							}
						}
					
					});
				
				}			
			
			}
			
			if($(this).val()==''){
				alert('Campo "'+$(this).attr('title')+'" obrigatório!');
				$(this).focus();
				erro = true;
			}
		});
		
		if(erro){
			e.preventDefault();
		}   
	
	});
	
	$("input[name='forma_recebimento']").click(function(){
		
		var selecionada = $(this).val();
		
		/*
		if(selecionada == "pagseguro"){
			$("#paypal").hide();
			$("#itau").hide();
			$("#"+selecionada).show();
		}
		*/
		
		if(selecionada == "itau"){
			$("#santander").hide();
			$("#paypal").hide();
			$("#"+selecionada).show();
		}
		
		if(selecionada == "santander"){
			$("#itau").hide();
			$("#paypal").hide();
			$("#"+selecionada).show();
		}		
		
		if(selecionada == "paypal"){
			$("#itau").hide();
			$("#santander").hide();
			$("#"+selecionada).show();
		}
	
	});
	
	$("form[name='form_termo_uso']").submit(function(e){
	
		var erro = false;
		
		if ($('#check_aceito').is(":checked") == false){
			erro = true;
			alert('É necessário aceitar os Termos de Uso antes de expor seus trabalhos.');
		}
		
		if(erro){
			e.preventDefault();
		}   
	
	});
	
	/*
	$('#bt_aceito').click(function(){
		window.location.href = base_url+'meu_cadastro/cadastro';
	});
	*/

});