$(function(){
    
	$("#religiao").change(function(){
		$("form[name='form_clientes']").attr("action", "/admin/clientes/");
		$("form[name='form_clientes']").submit();
	});    
    
	$("input[name='btn_editar']").click(function(){
		cliente_id = $(this).attr("rel");
		window.location.href = '/admin/clientes/editar/' + cliente_id;
	});
    
	$("#btn_relatorio").click(function(){
		
		var pedido_sel = $('input[name="cbx_item[]"]:checked').length;
		
		if (pedido_sel == 0) {
		
			alert("Por favor, selecione no mínimo um pedido.");
		
		} else {
			
			$("form[name='form_clientes']").attr("action", "/admin/clientes/gerar_excel/");
			$("form[name='form_clientes']").submit();
		}
	
	});	    
    
});