$(function () {
  
  $('input.only_number').keypress(function (event) {
    var key = event.charCode || event.keyCode || 0,
    elm = $(this);          
    if (key > 31 && (key < 48 || key > 57)) {
      return false;
    }
      return true;
  });
  
  $("#sort").change(function() {
    var menu = $(this).val();
    window.location.href = "/produtos/" + menu;
  });

});