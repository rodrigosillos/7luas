$(function(){
    
	$("form[name='form_login']").submit(function(e){
		e.preventDefault();
	});
			
	$('#btn_login').click(function(){
		
		$.ajax({
			url: "/login/auth/",
			type: 'post',
			data: $("form[name='form_login']").serialize(),
			success: function(data){
				
                var arr = data.split(";");
                
                response = arr[0];
                redirect = arr[1];
                
                //console.log(response);
                
				if(response=='logado'){
                    
                    if (redirect=='redirect_pagamento') {
                        window.location.href = '/pedidos/pagamento/';
                    } else if (redirect=='redirect_desejos') {
                        window.location.href = '/conta/lista_desejos/';
                    } else {
                        window.location.href = '/conta/';
                    }
				
                } else {
					alert('E-mail ou Senha incorretos!');
				}
			
			}
		
		});
	
	});    
    
	$("#btn_cadastrar").click(function(){
		
		var email = $("#email_cadastrar").val();
		
		if(email){
			
			$.ajax({
				
				url: '/cadastro/verifica_email/',
				type: 'post',
				data: {"email": email},
				success: function(data){
				
                    if (data) {
                        alert('Esse e-mail já está cadastrado!');
                    } else {
                        window.location.href = '/cadastro/';
                    }
                
				}
			
			});
        
		}
		
	});
    
});