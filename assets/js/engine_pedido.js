$(function(){

    $("#btn_finalizar_pedido").click(function() {
         
        if($('input:radio[name="payment"]:checked').val() == "cartao"){
        
            var data = $("#cartao_validade").val();
            var arr = data.split('/');
            
            var validade_mes = arr[0];
            var validade_ano = arr[1];
            
            var cc = new Moip.CreditCard({
                number  : $("#cartao_numero").val(),
                cvc     : $("#cartao_seguranca").val(),
                expMonth: validade_mes,
                expYear : validade_ano,
                pubKey  : $("#public_key").val()
            });
            
            //console.log(cc);
            
            if( cc.isValid() ){
                $("#encrypted_value").val(cc.hash());
                $("#erro_cartao").val("");
            } else{
                $("#encrypted_value").val('');
                $("#erro_cartao").val(true);
            }
           
        }
        
        if($('input:radio[name="payment"]:checked').val() == "boleto"){
            $("#erro_cartao").val("");
        }
         
        var erro = false;
        
        var novo_endereco = $("#novo_endereco").val();
        var erro_cartao = $("#erro_cartao").val();
        
        var mesmo_endereco = "";
        
        if ($("#mesmo_endereco_cobranca").is(':checked')) {
            mesmo_endereco = "sim";
        }
        
        if (mesmo_endereco == "" && novo_endereco == "") {
            
            alert("Por favor, escolha o endereço de entrega.");
            erro = true;
        
        }
        
        if (erro_cartao) {
            
            alert("Cartão de crédito inválido. Verifique os dados: número, código de segurança e validade.");
            erro = true;
        
        }
        
        if (novo_endereco == "sim") {
        
            $('input.obrigatorio').each(function(){
            
                if ($(this).val() == "") {
                    alert('Campo "'+$(this).attr('title')+'" obrigatório!');
                    $(this).focus();
                    erro = true;                
                }         
             
            });
        
        }         
        
        if (erro == false) {
            
            $("form[name='form_pedido']").submit();
        
        }                    
     
    });
    
    $("#phone").mask("(99) 9999-9999?9");
    $("#cartao_numero").mask("99999999999999?99999");
    $("#cartao_validade").mask("99/99");
    
    $(function(){
        $('[data-toggle="tooltip"]').tooltip()
    });
    
    $('input:radio[name="payment"]').change(function(){
        
        if ($(this).is(':checked') && $(this).val() == 'boleto') {
            $('.boleto').show()
            $('.cartao').hide()
            $('.paypal').hide()
        } else if($(this).is(':checked') && $(this).val() == 'cartao'){
            $('.boleto').hide()
            $('.cartao').show()
            $('.paypal').hide()
        } else if($(this).is(':checked') && $(this).val() == 'paypal'){
            $('.boleto').hide()
            $('.cartao').hide()
            $('.paypal').show()
        }
    
    });
    
    $("#mesmo_endereco_cobranca").click(function(){
        
        if($(this).is(":checked")) {
            $('.address').hide();
            $("#novo_endereco").val("");        
        }        

    });
    
    $('.new-add').click(function(){
      
      if ($("#novo_endereco").val() == "sim") {
      
        $('.address').hide();
        $("#novo_endereco").val("");
      
      } else {
      
        $('.address').show();
        $('#nome').focus();
        $("#novo_endereco").val("sim");
        $("#mesmo_endereco_cobranca").attr("checked", false);
        
      }      

    });   

});