$(function(){
	
	$("#btn_alterar_senha").click(function(){
		
		erro = false;
		
		var senha_atual = $("#senha_atual").val();
		var nova_senha = $("#nova_senha").val();
		var confirma_nova_senha = $("#confirma_nova_senha").val();
		
		if (nova_senha == "") {
            erro = true;
			alert("Por favor, digite a nova senha.");
        }
		
		if (confirma_nova_senha == "") {
            erro = true;
			alert("Por favor, confirme a nova senha.");
        }		
		
		if (nova_senha != confirma_nova_senha) {
            erro = true;
			alert("A nova senha não confere!");
        }
		
		$.ajax({
			
			url: '/cadastro/verifica_senha/',
			type: 'post',
			data: {"senha_atual": senha_atual},
			success: function(data){
				
				if (data == false) {
					erro = true;
                    alert("A senha atual está incorreta!");
				}
			
			}
		
		});
		
		if (erro == false) {

			$.ajax({
				
				url: '/cadastro/altera_senha/',
				type: 'post',
				data: {"nova_senha": nova_senha},
				success: function(data){
					
					alert("Senha alterada com sucesso!");
				
				}
			
			});		

        }
	
	});	

	$("form[name='form_cadastro']").submit(function(e){
	
		var erro = false;
		var erro_duplicidade = false;
		
		var tipo = $("#tipo").val();
		
		if (tipo == "") {
            var tipo = $("[name=tipo]:checked").val();
        }

		$('input.obrigatorio').each(function(){
			
			var input_name = $(this).attr("name");
			
			if($(this).val()==''){
                
                if (tipo == "fisica") {
                    
                    if (input_name == "nome" || input_name == "cpf" || input_name == "data_nascimento" || input_name == "sexo" || input_name == "email" || input_name == "senha" || input_name == "confirma_senha") {
                        
                        alert('Campo "'+$(this).attr('title')+'" obrigatório!');
                        $(this).focus();
                        erro = true;                        
                    
                    }
                    
                }
                
                if (tipo == "juridica") {
                    
                    if (input_name == "razao_social" || input_name == "nome_fantasia" || input_name == "cnpj" || input_name == "im" || input_name == "ie" || input_name == "email_juridica" || input_name == "senha_juridica" || input_name == "confirma_senha_juridica") {
                        
                        alert('Campo "'+$(this).attr('title')+'" obrigatório!');
                        $(this).focus();
                        erro = true;                        
                    
                    }
                    
                }
                
                if (input_name == "cep" || input_name == "endereco" || input_name == "numero" || input_name == "complemento" || input_name == "bairro" || input_name == "cidade" || input_name == "estado" || input_name == "telefone") {
                    
                    alert('Campo "'+$(this).attr('title')+'" obrigatório!');
                    $(this).focus();
                    erro = true;                        
                
                }                

			}
		
        });
		
		$('select.obrigatorio').each(function(){
			
			var input_name = $(this).attr("name");
			
			if($(this).val()==''){
                        
                alert('Campo "'+$(this).attr('title')+'" obrigatório!');
                $(this).focus();
				erro = true;
			
            }
		
        });
		
		if ($("#cliente_id").val() == "") {
			
			if($('input[name="termos_uso"]:checked').length == 0){
				alert("Você precisa concordar com os termos de uso.");
				erro = true;
			}            
        
		}
		
		if(erro || erro_duplicidade){
			e.preventDefault();
		}
	
	});
	
	$("#cpf").blur(function(e){
		
		if($(this).val() != ""){
			
			$.ajax({
				url: '/cadastro/verifica_duplicidade/',
				type: 'post',
				data: {"param": "cpf", "data": $(this).val()},
				success: function(data){
					if (data) {
						alert("Esse CPF já está cadastrado!");
						
						$("#cpf").val("");
						$("#cpf").focus();
						$("#erro_duplicidade").val(true);
					}				
				}
			});			
			
		}
		
	});
	
	$("#cnpj").blur(function(e){
		
		if($(this).val() != ""){
			
			$.ajax({	
				url: '/cadastro/verifica_duplicidade/',
				type: 'post',
				data: {"param": "cnpj", "data": $(this).val()},
				success: function(data){
					if (data) {
						alert("Esse CNPJ já está cadastrado!");
						
						$("#cnpj").val("");
						$("#cnpj").focus();
						$("#erro_duplicidade").val(true);
					}				
				}
			});			
			
		}
		
	});
	
	$("#email").blur(function(e){
		
		if($(this).val() != ""){
			
			$.ajax({	
				url: '/cadastro/verifica_duplicidade/',
				type: 'post',
				data: {"param": "email", "data": $(this).val()},
				success: function(data){
					if (data) {
						alert("Esse e-mail já está cadastrado!");
						
						$("#email").val("");
						$("#email").focus();
						$("#erro_duplicidade").val(true);
					}				
				}
			});			
			
		}
		
	});	
    
});