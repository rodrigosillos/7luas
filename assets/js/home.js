var tpj=jQuery;
					var revapi2;
					tpj(document).ready(function() {
						if(tpj("#7-luas").revolution == undefined){
							revslider_showDoubleJqueryError("#7-luas");
						}else{
							revapi2 = tpj("#7-luas").show().revolution({
								sliderType:"standard",
								jsFileLocation:"../../assets/revolution/js/",
								sliderLayout:"auto",
								delay:6000,
								navigation: {
									keyboardNavigation:"on",
									keyboard_direction: "horizontal",
									mouseScrollNavigation:"off",
									onHoverStop:"off",
									bullets: {
										enable:true,
										hide_onmobile:true,
										hide_under:800,
										style:"zeus",
										hide_onleave:false,
										direction:"horizontal",
										h_align:"right",
										v_align:"bottom",
										h_offset:40,
										v_offset:10,
										space:10,
										tmp:'',
									},
								},
								carousel: {
									horizontal_align: "center",
									vertical_align: "center",
									fadeout: "on",
									maxVisibleItems: 10,
									infinity: "on",
									space: 0,
									stretch: "on"
								},
								viewPort: {
									enable:true,
									outof:"pause",
									visible_area:"80%"
								},
								responsiveLevels:[1000,1000,778,480],
								gridwidth:[1000,1000,778,480],
								gridheight:[400,400,300,200],
								lazyType:"none",
								parallax: {
									type:"mouse",
									origo:"slidercenter",
									speed:2000,
									levels:[2,3,4,5,6,7,12,16,10,50],
								},
								shadow:0,
								spinner:"off",
								stopLoop:"off",
								stopAfterLoops:-1,
								stopAtSlide:-1,
								shuffle:"on",
								autoHeight:"off",
								hideThumbsOnMobile:"on",
								hideSliderAtLimit:0,
								hideCaptionAtLimit:0,
								hideAllCaptionAtLilmit:0,
								debugMode:false,
								fallbacks: {
									simplifyAll:"off",
									nextSlideOnWindowFocus:"off",
									disableFocusListener:false,
								}
							});
						}
					});	/*ready*/

var tpj=jQuery;
					var revapi54;
					tpj(document).ready(function() {
						if(tpj("#prod-slider").revolution == undefined){
							revslider_showDoubleJqueryError("#clients");
						}else{
							revapi54 = tpj("#prod-slider").show().revolution({
								sliderType:"carousel",
								jsFileLocation:"../../assets/revolution/js/",
								sliderLayout:"auto",
								dottedOverlay:"none",
								delay:2000,
								carousel: {
									maxRotation: 0,
									vary_rotation: "on",
									minScale: 0,
									vary_scale: "off",
									horizontal_align: "center",
									vertical_align: "center",
									fadeout: "off",
									vary_fade: "off",
									maxVisibleItems: 5,
									infinity: "on",
									space: 15,
									stretch: "off",
									h_offset: 20,
                                    v_offset: 0,
								},
								navigation: {
									arrows: {
                                    style: "custom",
                                    enable: true,
                                    hide_onmobile: true,
                                    hide_under: 0,
                                    hide_over: 9999,
                                    hide_onleave: false,
                                    hide_delay: 10,
                                    hide_delay_mobile: 1200,
                                    tmp: '',
                                        left: {
                                            h_align: "left",
                                            v_align: "center",
                                            h_offset: 15,
                                            v_offset: -70
                                        },
                                        right: {
                                            h_align: "right",
                                            v_align: "center",
                                            h_offset: 0,
                                            v_offset: -70
                                        }
                                    }
								},
								responsiveLevels:[1920,500,778,480],
								gridwidth:[1920,500,778,480],
								gridheight:[700,600,500,400],
								gridwidth:192,
								gridheight:320,
								lazyType:"none",
								shadow:0,
								spinner:"off",
								stopLoop:"on",
								shuffle:"off",
								autoHeight:"off",
								disableProgressBar:"on",
								hideThumbsOnMobile:"off",
								hideSliderAtLimit:0,
								hideCaptionAtLimit:0,
								hideAllCaptionAtLilmit:0,
								debugMode:false,
								fallbacks: {
									simplifyAll:"off",
									nextSlideOnWindowFocus:"off",
									disableFocusListener:true,
								}
							});
						}
					});	/*ready*/

var tpj=jQuery;
					var revapi54;
					tpj(document).ready(function() {
						if(tpj("#prod-slider-2").revolution == undefined){
							revslider_showDoubleJqueryError("#clients");
						}else{
							revapi54 = tpj("#prod-slider-2").show().revolution({
								sliderType:"carousel",
								jsFileLocation:"../../assets/revolution/js/",
								sliderLayout:"auto",
								dottedOverlay:"none",
								delay:2200,
								carousel: {
									maxRotation: 0,
									vary_rotation: "on",
									minScale: 0,
									vary_scale: "off",
									horizontal_align: "center",
									vertical_align: "center",
									fadeout: "off",
									vary_fade: "off",
									maxVisibleItems: 5,
									infinity: "on",
									space: 15,
									stretch: "off",
									h_offset: 20,
                                    v_offset: 0,
								},
								navigation: {
									arrows: {
                                    style: "custom",
                                    enable: true,
                                    hide_onmobile: true,
                                    hide_under: 0,
                                    hide_over: 9999,
                                    hide_onleave: false,
                                    hide_delay: 10,
                                    hide_delay_mobile: 1200,
                                    tmp: '',
                                        left: {
                                            h_align: "left",
                                            v_align: "center",
                                            h_offset: 15,
                                            v_offset: -70
                                        },
                                        right: {
                                            h_align: "right",
                                            v_align: "center",
                                            h_offset: 0,
                                            v_offset: -70
                                        }
                                    }
								},
								responsiveLevels:[1920,500,778,480],
								gridwidth:[1920,500,778,480],
								gridheight:[700,600,500,400],
								gridwidth:192,
								gridheight:320,
								lazyType:"none",
								shadow:0,
								spinner:"off",
								stopLoop:"on",
								shuffle:"off",
								autoHeight:"off",
								disableProgressBar:"on",
								hideThumbsOnMobile:"off",
								hideSliderAtLimit:0,
								hideCaptionAtLimit:0,
								hideAllCaptionAtLilmit:0,
								debugMode:false,
								fallbacks: {
									simplifyAll:"off",
									nextSlideOnWindowFocus:"off",
									disableFocusListener:true,
								}
							});
						}
					});	/*ready*/