$(function(){	

	$("#btn_comprar").click(function(){
		
		var produto_id = $(this).attr('rel');
		
		$.ajax({
			
			url: '/carrinho/insere_item/',
			type: 'post',
			data: {"produto_id": produto_id},
			success: function(data){
				
				window.location.href = '/carrinho/';
			
			}
		
		});
	
	});
	
	$("#btn_add_wishlist").click(function(){
		
        $.ajax({
            url: "/produtos/add_wishlist/",
            type: "post",
            data: {"produto_id": $("#produto_id").val()},
            success: function(data){
				if (data) {
                    alert(data);
                }
            }
        });		
		
	});
	
	$("#btn_atualizar_valores").click(function(){
        $.ajax({
            url: "/carrinho/atualizar/",
            type: "post",
            data: $("form[name='form_carrinho']").serialize(),
            success: function(data){
              alert("Carrinho atualizado com sucesso!");
              window.location.href = "/carrinho/";
            }
        });
	}); 
	
	$("#btn_finalizar_pedido").click(function(){
		
		var tipo_frete = $("#tipo_frete").val();
		var valor_frete = $("#valor_frete_sessao").val();
		var prazo_frete = $("#prazo_frete").val();
		var cep_calculo_frete = $("#cep_calculo_frete").val();
		var base_url = $("#base_url").val();
		
		if (tipo_frete == "" || valor_frete == "" || prazo_frete == "" || cep_calculo_frete == "") {
            
			alert("Digite o CEP de entrega para calcular o frete e prazo de entrega!");
			
		} else {
			
			$.ajax({
				url: '/carrinho/set_frete_sessao/',
				type: 'post',
				data: {"tipo_frete": tipo_frete, "valor_frete": valor_frete, "prazo_frete": prazo_frete, "cep_calculo_frete": cep_calculo_frete},
				complete: function(data){
					window.location.href = base_url + "pedidos/pagamento/";
				}		
			});			
		}
		
		
	});
	
	$("#btn_calcular").click(function(){
		
		var cep = $("#cep").val();
		var peso_total = $("#peso_total").val();
		
		if (cep && peso_total) {
			
			$("#loading").show();
			
			$.ajax({
				
				url: '/carrinho/calcula_frete/',
				type: 'post',
				data: {"cep_entrega": cep, "peso_total": peso_total},
				success: function(data){
					
					if(data!='cep_vazio' && data!=''){
						
						$('.forma-envio').html("");
						$('.forma-envio').append(data);
						$('.forma-envio').show();
						
						$('#btn_calcular').attr('disabled', false);
						
						$("#loading").hide();
					}
				
				}
			
			});
        
		}
		
	});
  
});