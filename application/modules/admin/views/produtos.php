<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Administrador 7 Luas</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/reset.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/960.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css" />
    <style>
      tr:nth-child(even) {background: #CCC}
      tr:nth-child(odd) {background: #FFF}
    </style>
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/engine_produto.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/engine.js"></script>
  </head>
  <body>    
      <div class="container_12 bg_branco">
          <?php include_once('includes/topo.php'); ?>
          <!-- Menu -->
          <?php include_once('includes/menu.php'); ?>
          <div class="grid_12 aba">
            <input type="button" name="btn_novo" value="Novo Produto">
            <form name="form_produtos" action="" method="post">
              <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
              <h2 class="uppercase"></h2>
              <div class="grid_12 alpha">
                  Filtrar por Religião:
                  <select name="religiao" id="religiao">
                    <option value="">Selecione ...</option>
                    <option value="todos"<?php if(isset($_POST['religiao'])){ if($_POST['religiao']=="todos"){ echo " selected"; } } ?>>- Todos -</option>
                    <option value="1"<?php if(isset($_POST['religiao'])){ if($_POST['religiao']==1){ echo " selected"; } } ?>>Budismo</option>
                    <option value="2"<?php if(isset($_POST['religiao'])){ if($_POST['religiao']==2){ echo " selected"; } } ?>>Cambomblé</option>
                    <option value="3"<?php if(isset($_POST['religiao'])){ if($_POST['religiao']==3){ echo " selected"; } } ?>>Umbanda</option>
                    <option value="5"<?php if(isset($_POST['religiao'])){ if($_POST['religiao']==5){ echo " selected"; } } ?>>Kardecismo</option>
                    <option value="6"<?php if(isset($_POST['religiao'])){ if($_POST['religiao']==6){ echo " selected"; } } ?>>Esoterismo</option>
                  </select>
                <!--<h2 class="uppercase"></h2>-->
              </div>
              <div class="grid_12 alpha" style="margin-top: 15px;">
                  Ação:
                  <select name="acao" id="acao">
                    <option value="">Selecione ...</option>
                    <option value="excluir_promocao">Excluir promoção do produto</option>
                  </select>
                  <input type="button" id="btn_alterar" value="Alterar">
                  <input type="button" id="btn_relatorio" value="Relatório de Produtos">
                <h2 class="uppercase"></h2>
              </div>
              <?php if($produtos){ ?>
              <ul>
                <li>
                  <table>
                    <tbody>
                      <tr>
                        <th><input type="checkbox" value="" id="cbx_todos"></th>
                        <th>Código</th>
                        <th>Foto</th>
                        <th>Produto</th>
                        <th>Valor Venda</th>
                        <th>Estoque</th>
                        <th></th>
                        <th></th>
                        <th></th>
                      </tr>
                      <?php
                      foreach($produtos as $produto){
                      ?>
                      <tr>
                        <td><input type="checkbox" value="<?php echo $produto->id; ?>" name="cbx_item[]" class="cbx_item"></td>
                        <td><?php echo $produto->codigo; ?></td>
                        <td><img src="/upload/produtos/<?php echo $produto->codigo; ?>/<?php echo $produto->codigo; ?>_55x67_1.jpg"></td>
                        <td><?php echo $produto->nome; ?></td>
                        <td>R$ <?php echo $produto->valor_venda; ?></td>
                        <td><?php echo $produto->estoque; ?></td>
                        <td><input type="button" name="btn_editar" rel="<?php echo $produto->id; ?>" value="Editar"></td>
                        <td><input type="button" name="btn_excluir" rel="<?php echo $produto->id; ?>" value="Excluir" disabled></td>
                        <td><input type="button" name="btn_imagens" rel="<?php echo $produto->id; ?>" value="Imagens"></td>
                        <!--<td><input type="button" name="btn_opcoes" rel="<?php echo $produto->id; ?>" value="Opções"></td>-->
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>                  
                </li>
              </ul>
              <?php } ?>
            </form>
          </div>
      </div>    
  </body>
</html>