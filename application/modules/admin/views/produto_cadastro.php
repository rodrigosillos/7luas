<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Administrador 7 Luas</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/reset.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/960.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css" />
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/engine_produto.js"></script>
  </head>
  <body>
      <div class="container_12 bg_branco">
          <?php include_once('includes/topo.php'); ?>
          <!-- Menu -->
          <?php include_once('includes/menu.php'); ?>
          <div class="grid_8 aba">
            <form name="form_produto" action="/admin/produtos/salvar/" method="post">
              <input type="hidden" name="produto_id" value="<?php echo isset($produto[0]->id) ? $produto[0]->id : null; ?>">
              <h2 class="uppercase">Informações do Produto</h2>
              <ul>
                <li>
                <label>Código</label>
                  <div>
                    <input type="text" name="codigo" value="<?php echo isset($produto[0]->codigo) ? $produto[0]->codigo : $produto_novo_codigo; ?>" class="campo_texto obrigatorio" title="Nome" readonly>
                  </div>
                </li>                
                <li>
                <label>Nome <span class="label_obrigatorio">*</span></label>
                  <div>
                    <input type="text" name="nome" value="<?php echo isset($produto[0]->nome) ? $produto[0]->nome : null; ?>" class="campo_texto obrigatorio" title="Nome">
                  </div>
                </li>
                <li>
                  <label>Tags</label>
                  <div>
                    <textarea name="tags" id="tags" title="Tags" style="width: 350px; height: 150px;"><?php echo isset($produto[0]->tags) ? $produto[0]->tags : null; ?></textarea>
                  </div>
                </li>
                <li>
                  <label>Descrição</label>
                  <div>
                    <textarea name="descricao" id="descricao" class="obrigatorio" title="Descrição" style="width: 350px; height: 150px;"><?php echo isset($produto[0]->descricao) ? $produto[0]->descricao : null; ?></textarea>
                  </div>
                </li>
                <li>
                  <label>Destaque</label>
                  <div>
                    <input type="checkbox" name="destaque" value="1" <?php if(isset($produto[0]->destaque)){ if($produto[0]->destaque==1){ echo "checked"; } } ?>>
                  </div>
                </li>
                <li>
                  <label>Ativo</label>
                  <div>
                    <input type="checkbox" name="ativo" value="1" <?php if(isset($produto[0]->ativo)){ if($produto[0]->ativo==1){ echo "checked"; } } ?>>
                  </div>
                </li>                
                <li>
                  <label>Estoque</label>
                  <div>
                    <input type="text" name="estoque" value="<?php echo isset($produto[0]->estoque) ? $produto[0]->estoque : null; ?>" class="campo_texto _200px">
                  </div>
                </li>
                <li>
                  <label>Peso</label>
                  <div>
                    <input type="text" name="peso" value="<?php echo isset($produto[0]->peso) ? $produto[0]->peso : null; ?>" class="campo_texto _200px">
                  </div>
                </li>
                <li>
                  <label>Medidas (Largura x Altura x Profundidade)</label>
                  <div>
                    <input type="text" name="medidas" value="<?php echo isset($produto[0]->medidas) ? $produto[0]->medidas : null; ?>" class="campo_texto _200px">
                  </div>
                </li>
                <li>
                  <label>Valor Venda</label>
                  <div>
                    <input type="text" name="valor_venda" value="<?php echo isset($produto[0]->valor_venda) ? $produto[0]->valor_venda : null; ?>" class="campo_texto _200px">
                  </div>
                </li>
                <li>
                  <label>Valor Promoção</label>
                  <div>
                    <input type="text" name="valor_promocao" value="<?php echo isset($produto[0]->valor_promocao) ? $produto[0]->valor_promocao : null; ?>" class="campo_texto _200px">
                  </div>
                </li>                
                <li>
                  <label>Valor Atacado</label>
                  <div>
                    <input type="text" name="valor_atacado" value="<?php echo isset($produto[0]->valor_atacado) ? $produto[0]->valor_atacado : null; ?>" class="campo_texto _200px">
                  </div>
                </li>                
                <li>
                  <label>Fornecedor</label>
                  <div>
                    <?php $fornecedor_bd = isset($produto[0]->fornecedor_id) ? $produto[0]->fornecedor_id : false; ?>
                    <select name="fornecedor_id" title="Fornecedor">
                      <option value="">Selecione...</option>
                      <?php foreach($fornecedores as $fornecedor){ ?>
                      <option value="<?php echo $fornecedor->id; ?>" <?php echo $fornecedor->id == $fornecedor_bd ? "selected" : ""; ?>><?php echo $fornecedor->nome; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </li>
                <li>
                  <label>Religião</label>
                  <div>
                    <?php $religiao_bd = isset($produto[0]->religiao_id) ? $produto[0]->religiao_id : false; ?>
                    <select name="religiao_id" class="obrigatorio" title="Religião">
                      <option value="">Selecione...</option>
                      <?php foreach($religioes as $religiao){ ?>
                      <option value="<?php echo $religiao->id; ?>" <?php echo $religiao->id == $religiao_bd ? "selected" : ""; ?>><?php echo $religiao->nome; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </li>
                <li>
                  <label>Categoria</label>
                  <div>
                    <?php $categoria_bd = isset($produto[0]->categoria_id) ? $produto[0]->categoria_id : false; ?>
                    <select name="categoria_id" class="obrigatorio" title="Categoria">
                      <option value="">Selecione...</option>
                      <?php foreach($categorias as $categoria){ ?>
                      <option value="<?php echo $categoria->id; ?>" <?php echo $categoria->id == $categoria_bd ? "selected" : ""; ?>><?php echo $categoria->nome; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </li>
                <li>
                  <label>Sub Categoria</label>
                  <div>
                    <?php $subcategoria_bd = isset($produto[0]->subcategoria_id) ? $produto[0]->subcategoria_id : false; ?>
                    <select name="subcategoria_id">
                      <option value="">Selecione...</option>
                      <?php foreach($subcategorias as $subcategoria){ ?>
                      <option value="<?php echo $subcategoria->id; ?>" <?php echo $subcategoria->id == $subcategoria_bd ? "selected" : ""; ?>><?php echo $subcategoria->nome; ?></option>
                      <?php } ?>                      
                    </select>
                  </div>
                </li>
                <li>
                  <label>Cores</label>
                  <div>
                    <?php $cor_bd = isset($produto[0]->cor_id) ? $produto[0]->cor_id : false; ?>
                    <select name="cor_id">
                      <option value="">Selecione...</option>
                      <?php foreach($cores as $cor){ ?>
                      <option value="<?php echo $cor->id; ?>" <?php echo $cor->id == $cor_bd ? "selected" : ""; ?>><?php echo $cor->nome; ?></option>
                      <?php } ?>                      
                    </select>
                  </div>
                </li>                
              </ul>
              <ul>
                <li>
                  <input type="submit" name="bt_salvar" value="SALVAR">
                </li>
              </ul>
            </form>
          </div>
      </div>    
  </body>
  <script>
  CKEDITOR.replace( 'descricao', {
    fullPage: true,
    allowedContent: true
  });
</script>
</html>