<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Administrador 7 Luas</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/reset.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/960.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css" />
    <style>
      tr:nth-child(even) {background: #CCC}
      tr:nth-child(odd) {background: #FFF}
    </style>
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.11.1.min.js"></script>
  </head>
  <body>    
      <div class="container_12 bg_branco">
          <?php include_once('includes/topo.php'); ?>
          <!-- Menu -->
          <?php include_once('includes/menu.php'); ?>
          <div class="grid_12 aba">
            <form name="form_banner" action="" method="post">
              <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
              <h2 class="uppercase">Últimos Pedidos</h2>
              <ul>
                <li>
                  <table>
                    <tbody>
                      <tr>
                        <th>Número</th>
                        <th>Data</th>
                        <th>Cliente</th>
                        <th>Status</th>
                        <th>Forma de Pagamento</th>
                      </tr>
                      <?php
                      foreach($pedidos as $pedido){
                      ?>
                      <tr>
                        <td><?php echo $pedido->pedido_id; ?></td>
                        <td><?php echo date("d/m/Y H:i:s", strtotime($pedido->pedido_data)); ?></td>
                        <td><?php echo $pedido->cliente_nome; ?></td>
                        <td><?php echo $pedido->status_nome; ?></td>
                        <td><?php echo $pedido->pagamento_nome; ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>                  
                </li>
              </ul>              
              <h2 class="uppercase">Últimos Clientes</h2>
             <ul>
                <li>
                  <table>
                    <tbody>
                      <tr>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Estado</th>
                      </tr>
                      <?php
                      foreach($clientes as $cliente){
                      ?>
                      <tr>
                        <td><?php echo $cliente->nome; ?></td>
                        <td><?php echo $cliente->email; ?></td>
                        <td><?php echo $cliente->telefone; ?></td>
                        <td><?php echo $cliente->estado; ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>                  
                </li>
              </ul>              
            </form>
          </div>
      </div>    
  </body>
</html>