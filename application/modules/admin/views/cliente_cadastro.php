<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Administrador 7 Luas</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/reset.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/960.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css" />
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/engine_cliente.js"></script>
  </head>
  <body>
      <div class="container_12 bg_branco">
          <?php include_once('includes/topo.php'); ?>
          <!-- Menu -->
          <?php include_once('includes/menu.php'); ?>
          <div class="grid_8 aba">
            <form name="form_cliente" action="/admin/clientes/salvar/" method="post">
              <input type="hidden" name="cliente_id" value="<?php echo isset($cliente[0]->id) ? $cliente[0]->id : null; ?>">
              <h2 class="uppercase">Informações do Cliente</h2>
              <ul>
                <li>
                  <label>Nome <span class="label_obrigatorio">*</span></label>
                  <div>
                    <input type="text" name="nome" value="<?php echo isset($cliente[0]->nome) ? $cliente[0]->nome : null; ?>" class="campo_texto obrigatorio" title="Nome">
                  </div>
                </li>
                <li>
                  <label>E-mail <span class="label_obrigatorio">*</span></label>
                  <div>
                    <input type="text" name="email" value="<?php echo isset($cliente[0]->email) ? $cliente[0]->email : null; ?>" class="campo_texto obrigatorio" title="E-mail">
                  </div>
                </li>
                <li>
                  <label>CPF <span class="label_obrigatorio">*</span></label>
                  <div>
                    <input type="text" name="cpf" value="<?php echo isset($cliente[0]->cpf) ? $cliente[0]->cpf : null; ?>" class="campo_texto obrigatorio" title="CPF">
                  </div>
                </li>
                <li>
                  <label>Data de Nascimento <span class="label_obrigatorio">*</span></label>
                  <div>
                    <input type="text" name="data_nascimento" value="<?php echo isset($cliente[0]->data_nascimento) ? $cliente[0]->data_nascimento : null; ?>" class="campo_texto obrigatorio" title="Data de Nascimento">
                  </div>
                </li>                
              </ul>
              <ul>
                <li>
                  <input type="submit" name="btn_salvar" value="SALVAR">
                </li>
              </ul>
            </form>
          </div>
      </div>    
  </body>
</html>