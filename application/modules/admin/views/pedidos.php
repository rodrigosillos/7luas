<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Administrador 7 Luas</title>
    <link rel="stylesheet" href="/assets/admin/css/reset.css" />
    <link rel="stylesheet" href="/assets/admin/css/960.css" />
    <link rel="stylesheet" href="/assets/admin/css/style.css" />
    <style>
      tr:nth-child(even) {background: #CCC}
      tr:nth-child(odd) {background: #FFF}
    </style>
    <script src="/assets/admin/js/jquery-1.12.2.min.js"></script>
    <script src="/assets/admin/js/engine_pedido.js"></script>
    <script src="/assets/admin/js/engine.js"></script>
  </head>
  <body>
      <div class="container_12 bg_branco">
          <?php include_once('includes/topo.php'); ?>
          <!-- Menu -->
          <?php include_once('includes/menu.php'); ?>
          <div class="grid_12 aba">
            <!--<input type="button" name="bt_novo" value="Novo Pedido">-->
            <form name="form_pedidos" action="" method="post">
              <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
              <h2 class="uppercase"></h2>
              <div class="grid_12 alpha">
                Filtrar por Status:
                <select name="status" id="status">
                  <option value="">Selecione ...</option>
                  <option value="todos"<?php if(isset($_POST['status'])){ if($_POST['status']=="todos"){ echo " selected"; } } ?>>- Todos -</option>
                  <option value="1"<?php if(isset($_POST['status'])){ if($_POST['status']==1){ echo " selected"; } } ?>>Aguardando Pagamento</option>
                  <option value="2"<?php if(isset($_POST['status'])){ if($_POST['status']==2){ echo " selected"; } } ?>>Análise de Risco</option>
                  <option value="3"<?php if(isset($_POST['status'])){ if($_POST['status']==3){ echo " selected"; } } ?>>Aprovado</option>
                  <option value="4"<?php if(isset($_POST['status'])){ if($_POST['status']==4){ echo " selected"; } } ?>>Cancelado</option>
                  <option value="5"<?php if(isset($_POST['status'])){ if($_POST['status']==5){ echo " selected"; } } ?>>Despachado</option>
                  <option value="6"<?php if(isset($_POST['status'])){ if($_POST['status']==6){ echo " selected"; } } ?>>Efetuado</option>
                  <option value="7"<?php if(isset($_POST['status'])){ if($_POST['status']==7){ echo " selected"; } } ?>>Entregue</option>
                </select>
                <!--<h2 class="uppercase"></h2>-->
              </div>
              <div class="grid_12 alpha" style="margin-top: 15px;">
                Ação:
                <select name="acao_status" id="acao_status">
                  <option value="">Selecione ...</option>
                  <option value="6">Alterar status para: Efetuado</option>
                  <option value="1">Alterar status para: Aguardando Pagamento</option>
                  <option value="2">Alterar status para: Análise de Risco</option>
                  <option value="3">Alterar status para: Aprovado</option>
                  <option value="5">Alterar status para: Despachado</option>
                  <option value="7">Alterar status para: Entregue</option>
                </select>
                <input type="button" id="btn_alterar" value="Alterar">
                <input type="button" id="btn_relatorio" value="Relatório de Pedidos">
                <h2 class="uppercase"></h2>
              </div>
              <?php if($pedidos){ ?>
              <ul>
                <li>
                  <table>
                    <tbody>
                      <tr>
                        <th><input type="checkbox" value="" id="cbx_todos"></th>
                        <th>Número</th>
                        <th>Data</th>
                        <th>Cliente</th>
                        <th>Status</th>
                        <th>Forma de Pagamento</th>
                        <th>Valor</th>
                        <th></th>
                      </tr>
                      <?php
                      foreach($pedidos as $pedido){
                      ?>
                      <tr>
                        <td><input type="checkbox" value="<?php echo $pedido->pedido_id; ?>" name="cbx_item[]" class="cbx_item"></td>
                        <td><?php echo $pedido->pedido_id; ?></td>
                        <td><?php echo date("d/m/Y H:i:s", strtotime($pedido->pedido_data)); ?></td>
                        <td><?php echo $pedido->cliente_nome; ?></td>
                        <td><?php echo $pedido->status_nome; ?></td>
                        <td><?php echo $pedido->pagamento_nome; ?></td>
                        <td>R$ <?php echo $pedido->pedido_valor; ?></td>
                        <td><input type="button" name="btn_detalhes" rel="<?php echo $pedido->pedido_id; ?>" value="Detalhes"></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>                  
                </li>
              </ul>
              <?php } ?>
            </form>
          </div>
      </div>    
  </body>
</html>