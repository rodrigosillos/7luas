<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Administrador 7 Luas</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/reset.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/960.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css" />
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/engine_login.js"></script>
  </head>
  <body>
      <div class="container_12 bg_branco">
          <?php include_once('includes/topo.php'); ?>
          <div class="grid_6 aba">
            <h2 class="uppercase">Login</h2>
            <form name="form_login" id="form_login" action="" method="post">
              <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
              <ul>
                <li>
                  <label>E-mail</label>
                  <div>
                    <input type="text" name="email" class="campo_texto">
                  </div>
                </li>
                <li>
                  <label>Senha</label>
                  <div>
                    <input type="password" name="senha" class="campo_texto">
                  </div>
                </li>
                <li>
                  <a href="javascript:void(0)" id="btn_esqueceu_senha" style="color:#5c5c5c;">Esqueceu sua senha?</a>
                </li>
                <li>
                  <input type="submit" id="btn_login" value="ENTRAR">
                </li>
              </ul>
            </form>
          </div>        
          <!--
          <div class="grid_6 aba">
            <h2 class="uppercase">Não tem cadastro?</h2>
              <input type="hidden" id="base_url" value="<?php //echo base_url(); ?>">
              <ul>
                <li>
                  <input type="button" id="bt_cadastro" value="CADASTRE-SE">
                </li>
              </ul>
          </div>
          -->
      </div>    
  </body>
</html>