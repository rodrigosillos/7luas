<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Administrador 7 Luas</title>
        <link rel="stylesheet" href="/assets/admin/css/reset.css" />
        <link rel="stylesheet" href="/assets/admin/css/960.css" />
        <link rel="stylesheet" href="/assets/admin/css/style.css" />
        <script src="/assets/admin/js/jquery-1.12.2.min.js"></script>
        <script src="/assets/admin/js/engine_pedido.js"></script>
        <script src="/assets/admin/js/engine.js"></script>
    </head>
    <body>
        <div class="container_12 bg_branco">
            <?php include_once('includes/topo.php'); ?>
            <!-- Menu -->
            <?php include_once('includes/menu.php'); ?>
            <div class="grid_12 aba">
                <h2 class="uppercase">Pedido <?php echo $pedido[0]->pedido_id; ?></h2>
                <div class="grid_4">
                    <p><b>Data</b> <?php echo date("d/m/Y H:i:s", strtotime($pedido[0]->pedido_data)); ?></p>
                    <p><b>Cliente</b> <?php echo $pedido[0]->cliente_nome; ?></p>
                    <p><b>Forma de Envio</b> <?php echo $pedido[0]->envio_nome; ?></p>
                    <p><b>Peso</b> <?php echo $pedido[0]->peso; ?></p>
                </div>
                <div class="grid_4">
                    <p><b>Status</b> <?php echo $pedido[0]->status_nome; ?></p>
                    <p><b>Forma de Pagamento</b> <?php echo $pedido[0]->pagamento_nome; ?></p>
                    <p><b>Prazo de entrega</b> <?php echo $pedido[0]->prazo; ?> dia(s) útil(eis)</p>
                </div>
                <div class="grid_12">
                    <table>
                        <tbody>
                            <tr>
                                <th></th>
                                <th><b>Produto</b></th>
                                <th><b>Quantidade</b></th>
                                <th><b>Valor</b></th>
                                <th><b>Valor Total</b></th>
                            </tr>
                            <?php foreach($itens as $item){ ?>
                            <tr>
                                <th><img src="/upload/produtos/<?php echo $item->produto_codigo; ?>/<?php echo $item->produto_codigo; ?>_70x86_1.jpg" alt="ALT AQUI" title="TITLE AQUI" class="prod2 img-responsive"></th>
                                <th><?php echo $item->produto_nome; ?></th>
                                <th><?php echo $item->item_quantidade; ?></th>
                                <th>R$ <?php echo $item->item_valor_unitario; ?></th>
                                <th>R$ <?php echo $item->item_valor_total; ?></th>
                            </tr>
                            <?php } ?>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><b>Valor Frete</b></th>
                                <th>R$ <?php echo $pedido[0]->valor_frete; ?></th>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><b>Valor Total</b></th>
                                <th>R$ <?php echo $pedido[0]->pedido_valor; ?></th>
                            </tr>                            
                        </tbody>
                    </table>
                </div>
                <div class="grid_6 alpha">
                    <p><b>Endereço de Cobrança</b></p>
                    <p>
                        <?php echo $cliente[0]->nome; ?><br/>
                        <?php echo $cliente[0]->endereco; ?>, <?php echo $cliente[0]->numero; ?> - <?php echo $cliente[0]->complemento; ?><br/>
                        <?php echo $cliente[0]->bairro; ?> - <?php echo $cliente[0]->cidade; ?> - <?php echo $cliente[0]->estado; ?><br/>
                        CEP <?php echo $cliente[0]->cep; ?><br/>
                        Telefone <?php echo $cliente[0]->telefone; ?><br/>
                    </p>
                </div>
                <div class="grid_6 alpha">
                    <p><b>Endereço de Entrega</b></p>
                    <p>
                        <?php echo $pedido_endereco[0]->nome; ?><br/>
                        <?php echo $pedido_endereco[0]->endereco; ?>, <?php echo $pedido_endereco[0]->numero; ?> - <?php echo $pedido_endereco[0]->complemento; ?><br/>
                        <?php echo $pedido_endereco[0]->bairro; ?> - <?php echo $pedido_endereco[0]->cidade; ?> - <?php echo $pedido_endereco[0]->estado; ?><br/>
                        CEP <?php echo $pedido_endereco[0]->cep; ?><br/>
                        Telefone <?php echo $pedido_endereco[0]->telefone; ?><br/>
                    </p>
                </div>                
            </div>
        </div>    
    </body>
</html>