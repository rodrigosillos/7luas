<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Administrador 7 Luas</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/reset.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/960.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css" />
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/engine_produto.js"></script>
  </head>
  <body>
      <div class="container_12 bg_branco">
          <?php include_once('includes/topo.php'); ?>
          <!-- Menu -->
          <?php include_once('includes/menu.php'); ?>
          <div class="grid_8 aba">
            <form name="form_produto" action="/admin/produtos/salvar/" method="post">
              <input type="hidden" name="produto_id" value="<?php echo isset($produto[0]->id) ? $produto[0]->id : null; ?>">
              <h2 class="uppercase">Produto cadastrado com sucesso!</h2>
              <ul>
                <li>
                  <div>
                    <input type="button" name="btn_novo" value="CADASTRAR NOVO PRODUTO">
                    <input type="button" name="btn_voltar" value="VOLTAR">
                  </div>
                </li>           
              </ul>
            </form>
          </div>
      </div>    
  </body>
</html>