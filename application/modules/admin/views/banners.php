<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Administrador 7 Luas</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/reset.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/960.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css" />
    <style>
      tr:nth-child(even) {background: #CCC}
      tr:nth-child(odd) {background: #FFF}
    </style>
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/engine_banner.js"></script>
  </head>
  <body>    
      <div class="container_12 bg_branco">
          <?php include_once('includes/topo.php'); ?>
          <!-- Menu -->
          <?php include_once('includes/menu.php'); ?>
          <div class="grid_12 aba">
            <input type="button" name="bt_novo" value="Novo Banner">
            <form name="form_banner" action="" method="post">
              <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
              <h2 class="uppercase"></h2>
              <ul>
                <li>
                  <table>
                    <tbody>
                      <tr>
                        <th></th>
                        <th>Nome</th>
                        <th>Imagem</th>
                        <th>Posição</th>
                        <th></th>
                        <th></th>
                        <th></th>
                      </tr>
                      <?php
                      foreach($banners as $banner){
                      ?>
                      <tr>
                        <td><input type="checkbox" value="<?php echo $banner->id; ?>" name="cbx_banner"></td>
                        <td><?php echo $banner->nome; ?></td>
                        <td><img src="/upload/<?php echo $banner->imagem; ?>" width="50" height="50"></td>
                        <td><?php echo $banner->posicao; ?></td>
                        <td><input type="button" name="bt_editar" rel="<?php echo $banner->id; ?>" value="Editar"></td>
                        <td><input type="button" name="bt_excluir" rel="<?php echo $banner->id; ?>" value="Excluir"></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>                  
                </li>
              </ul>
            </form>
          </div>
      </div>    
  </body>
</html>