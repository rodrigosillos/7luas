<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Administrador 7 Luas</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/reset.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/960.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css" />
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/engine_banner.js"></script>
  </head>
  <body>
      <div class="container_12 bg_branco">
          <?php include_once('includes/topo.php'); ?>
          <!-- Menu -->
          <?php include_once('includes/menu.php'); ?>
          <div class="grid_8 aba">
            <?php echo form_open_multipart('/admin/banners/salvar/', array("name" => "form_banner")); ?>
              <input type="hidden" name="banner_id" value="<?php echo isset($banner[0]->id) ? $banner[0]->id : null; ?>">
              <h2 class="uppercase">Informações do Banner</h2>
              <p><?php echo isset($error) ? $error : false; ?></p>
              <ul>                
                <li>
                    <label>Nome <span class="label_obrigatorio">*</span></label>
                    <div>
                        <input type="text" name="nome" value="<?php echo isset($banner[0]->nome) ? $banner[0]->nome : null; ?>" class="campo_texto obrigatorio" title="Nome">
                    </div>
                </li>               
                <li>
                  <label>Imagem</label>
                  <div>
                    <input type="file" name="imagem" class="campo_texto">
                  </div>
                </li>
                <li>
                  <label>Posição</label>
                  <div>
                    <input type="text" name="posicao" value="<?php echo isset($banner[0]->posicao) ? $banner[0]->posicao : null; ?>" class="campo_texto obrigatorio" title="Posição">
                  </div>
                </li>                              
              </ul>
              <ul>
                <li>
                  <input type="submit" name="bt_salvar" value="SALVAR">
                </li>
              </ul>
            </form>
          </div>
      </div>    
  </body>
</html>