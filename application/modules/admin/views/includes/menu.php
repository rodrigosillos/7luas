<?php

$segment1 = $this->uri->segment(1);
$segment2 = $this->uri->segment(2);

if($segment2){
  $aba_selecionada = $segment2;
} else {
  $aba_selecionada = $segment1;
}

?>
<div class="grid_12 menu">
  <ul class="menu uppercase">
    <li><a href="<?php echo base_url(); ?>admin/produtos" <?php if($aba_selecionada=="produtos"){ ?>class="selecionado"<?php } ?>>Produtos</a></li>
    <li><a href="<?php echo base_url(); ?>admin/pedidos" <?php if($aba_selecionada=="pedidos"){ ?>class="selecionado"<?php } ?>>Pedidos</a></li>
    <li><a href="<?php echo base_url(); ?>admin/clientes" <?php if($aba_selecionada=="clientes"){ ?>class="selecionado"<?php } ?>>Clientes</a></li>
    <!--<li><a href="<?php echo base_url(); ?>admin/banners" <?php if($aba_selecionada=="banners"){ ?>class="selecionado"<?php } ?>>Banners</a></li>-->
    <!--<li><a href="<?php echo base_url(); ?>admin/configuracoes" <?php if($aba_selecionada=="configuracoes"){ ?>class="selecionado"<?php } ?>>Configurações</a></li>-->
  </ul>
</div>