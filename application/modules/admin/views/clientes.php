<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Administrador 7 Luas</title>
    <link rel="stylesheet" href="/assets/admin/css/reset.css" />
    <link rel="stylesheet" href="/assets/admin/css/960.css" />
    <link rel="stylesheet" href="/assets/admin/css/style.css" />
    <style>
      tr:nth-child(even) {background: #CCC}
      tr:nth-child(odd) {background: #FFF}
    </style>
    <script src="/assets/admin/js/jquery-1.11.1.min.js"></script>
    <script src="/assets/admin/js/engine_cliente.js"></script>
    <script src="/assets/admin/js/engine.js"></script>
  </head>
  <body>
      <div class="container_12 bg_branco">
          <?php include_once('includes/topo.php'); ?>
          <!-- Menu -->
          <?php include_once('includes/menu.php'); ?>
          <div class="grid_12 aba">
            <!--<input type="button" name="bt_novo" value="Novo Cliente">-->
            <form name="form_clientes" action="" method="post">
              <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
              <h2 class="uppercase"></h2>
              <div class="grid_12 alpha">
                  Filtrar por Religião:
                  <select name="religiao" id="religiao">
                    <option value="">Selecione ...</option>
                    <option value="todos"<?php if(isset($_POST['religiao'])){ if($_POST['religiao']=="todos"){ echo " selected"; } } ?>>- Todos -</option>
                    <option value="1"<?php if(isset($_POST['religiao'])){ if($_POST['religiao']==1){ echo " selected"; } } ?>>Budismo</option>
                    <option value="2"<?php if(isset($_POST['religiao'])){ if($_POST['religiao']==2){ echo " selected"; } } ?>>Cambomblé</option>
                    <option value="3"<?php if(isset($_POST['religiao'])){ if($_POST['religiao']==3){ echo " selected"; } } ?>>Umbanda</option>
                    <option value="5"<?php if(isset($_POST['religiao'])){ if($_POST['religiao']==5){ echo " selected"; } } ?>>Kardecismo</option>
                    <option value="6"<?php if(isset($_POST['religiao'])){ if($_POST['religiao']==6){ echo " selected"; } } ?>>Esoterismo</option>
                    <option value="7"<?php if(isset($_POST['religiao'])){ if($_POST['religiao']==7){ echo " selected"; } } ?>>Não tenho religião</option>
                    <option value="8"<?php if(isset($_POST['religiao'])){ if($_POST['religiao']==8){ echo " selected"; } } ?>>Todas</option>
                  </select>
                <!--<h2 class="uppercase"></h2>-->
              </div>              
              <div class="grid_12 alpha" style="margin-top: 15px;">
                  Ação:
                  <select name="acao" id="acao">
                    <option value="">Selecione ...</option>
                    <option value="ativar_cliente">Ativar cliente</option>
                    <option value="desativar_cliente">Desativar cliente</option>
                  </select>
                  <input type="button" id="btn_alterar" value="Alterar">
                  <input type="button" id="btn_relatorio" value="Relatório de Clientes">
                <h2 class="uppercase"></h2>
              </div>              
              <?php if($clientes){ ?>
              <ul>
                <li>
                  <table>
                    <tbody>
                      <tr>
                        <th><input type="checkbox" value="" id="cbx_todos"></th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Estado</th>
                        <th></th>
                        <th></th>
                      </tr>
                      <?php
                      foreach($clientes as $cliente){
                      ?>
                      <tr>
                        <td><input type="checkbox" value="<?php echo $cliente->id; ?>" name="cbx_item[]" class="cbx_item"></td>
                        <td><?php echo $cliente->nome; ?></td>
                        <td><?php echo $cliente->email; ?></td>
                        <td><?php echo $cliente->telefone; ?></td>
                        <td><?php echo $cliente->estado; ?></td>
                        <td><input type="button" name="btn_editar" rel="<?php echo $cliente->id; ?>" value="Editar"></td>
                        <td><input type="button" name="btn_excluir" rel="<?php echo $cliente->id; ?>" value="Excluir" disabled></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>                  
                </li>
              </ul>
              <?php } ?>
            </form>
          </div>
      </div>    
  </body>
</html>