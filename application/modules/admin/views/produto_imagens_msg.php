<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Administrador 7 Luas</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/reset.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/960.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css" />
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/engine_produto.js"></script>
  </head>
  <body>
      <div class="container_12 bg_branco">
          <?php include_once('includes/topo.php'); ?>
          <!-- Menu -->
          <?php include_once('includes/menu.php'); ?>
          <div class="grid_8 aba">
            <h2 class="uppercase">
            <?php
            if(is_array($mensagem))
            {
                print_r($mensagem);
            }
            else
            {
                echo $mensagem;    
            }            
            ?>
            </h2>
            <ul>
              <li>
                <div>
                  <input type="button" name="btn_adicionar_imagem" rel="<?php echo $produto_id; ?>" value="ADICIONAR + IMAGENS">
                  <input type="button" name="btn_voltar_imagem" rel="<?php echo $produto_id; ?>" value="VOLTAR P/ PRODUTOS">
                </div>
              </li>           
            </ul>
          </div>
      </div>    
  </body>
</html>