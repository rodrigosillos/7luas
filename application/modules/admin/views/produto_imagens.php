<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Administrador 7 Luas</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/reset.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/960.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css" />
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
    <script src="/assets/admin/js/engine_produto.js"></script>
  </head>
  <body>
      <div class="container_12 bg_branco">
          <?php include_once('includes/topo.php'); ?>
          <!-- Menu -->
          <?php include_once('includes/menu.php'); ?>
          <div class="grid_8 aba">
            <?php echo form_open_multipart('/admin/produtos/upload/', array("name" => "form_produto_imagens")); ?>
              <input type="hidden" name="produto_id" value="<?php echo isset($produto[0]->id) ? $produto[0]->id : null; ?>">
              <input type="hidden" name="produto_codigo" value="<?php echo isset($produto[0]->codigo) ? $produto[0]->codigo : null; ?>">
              <input type="hidden" name="posicao" value="<?php echo $posicao; ?>">
              <h2 class="uppercase">Adicionar Imagens Produto: <?php echo isset($produto[0]->nome) ? $produto[0]->codigo . " - " .$produto[0]->nome : null; ?></h2>
              <p><?php echo isset($error) ? $error : false; ?></p>
              <ul>
                <li>
                  <label>Imagem secundária</label>
                  <div>
                    <input type="checkbox" name="secundaria" value="1">
                  </div>
                </li>                
                <li>
                  <label>Fechamento do Pedido (55x67)</label>
                  <div>
                    <input type="file" name="imagem[]" rel="55x67" title="Fechamento do Pedido (55x67)" class="campo_texto obrigatorio">
                  </div>
                </li>
                <li>
                  <label>Carrinho / Wishlist / Secundária (70x86)</label>
                  <div>
                    <input type="file" name="imagem[]" rel="70x86" title="Carrinho / Wishlist (70x86)" class="campo_texto obrigatorio">
                  </div>
                </li>
                <li>
                  <label>Vitrine Carrossel (140x180)</label>
                  <div>
                    <input type="file" name="imagem[]" rel="140x180" title="Vitrine Carrossel (140x180)" class="campo_texto obrigatorio">
                  </div>
                </li>
                <li>
                  <label>Vitrine Listagem (220x270)</label>
                  <div>
                    <input type="file" name="imagem[]" rel="220x270" title="Vitrine Listagem (220x270)" class="campo_texto obrigatorio">
                  </div>
                </li>                
                <li>
                  <label>Destaque Home (300x180)</label>
                  <div>
                    <input type="file" name="imagem[]" rel="300x180" title="Destaque Home (300x180)" class="campo_texto obrigatorio">
                  </div>
                </li>           

                <li>
                  <label>Detalhe Produto (430x528)</label>
                  <div>
                    <input type="file" name="imagem[]" rel="430x528" title="Detalhe Produto (430x528)" class="campo_texto obrigatorio">
                  </div>
                </li>
                <li>
                  <label>Ampliada Produto (832x1022)</label>
                  <div>
                    <input type="file" name="imagem[]" rel="832x1022" title="Ampliada Produto (832x1022)" class="campo_texto obrigatorio">
                  </div>
                </li>                
              </ul>
              <ul>
                <li>
                  <input type="button" name="btn_voltar" value="VOLTAR">
                  <input type="submit" name="btn_salvar" value="SALVAR">
                </li>
              </ul>
            </form>
          </div>
          <div class="grid_8 aba">
            <h2 class="uppercase">Imagens</h2>
              <ul>
                <li>
                  <table>
                    <tbody>
                      <tr>
                        <th>Imagem</th>
                        <th>Posição</th>
                        <th></th>
                        <th></th>
                      </tr>
                      <?php
                      foreach($imagens as $imagem){
                        //$explode = explode("_", $imagem->imagem);
                        $produto_codigo = $imagem->codigo;
                      ?>
                      <tr>
                        <td><img src="/upload/produtos/<?php echo $produto_codigo; ?>/<?php echo $produto_codigo; ?>_70x86_<?php echo $imagem->posicao; ?>.jpg"></td>
                        <td><?php echo $imagem->posicao; ?></td>
                        <td><input type="button" name="btn_editar_imagem" rel="<?php echo $imagem->id; ?>" value="Editar"></td>
                        <td><input type="button" name="btn_excluir_imagem" rel="<?php echo $imagem->id; ?>" value="Excluir"></td>
                      </tr>
                      <?php
                      }
                      ?>
                    </tbody>
                  </table>                  
                </li>
              </ul>            
          </div>
      </div>    
  </body>
</html>