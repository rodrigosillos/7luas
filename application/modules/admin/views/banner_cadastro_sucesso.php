<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Administrador 7 Luas</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/reset.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/960.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css" />
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/engine_banner.js"></script>
  </head>
  <body>
      <div class="container_12 bg_branco">
          <?php include_once('includes/topo.php'); ?>
          <!-- Menu -->
          <?php include_once('includes/menu.php'); ?>
          <div class="grid_8 aba">
            <h2 class="uppercase"><?php echo $mensagem; ?></h2>
            <ul>
              <li>
                <div>
                  <input type="button" name="bt_novo" value="CADASTRAR NOVO BANNER">
                  <input type="button" name="bt_voltar" value="VOLTAR">
                </div>
              </li>           
            </ul>
          </div>
      </div>    
  </body>
</html>