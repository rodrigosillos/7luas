<?php

class Cadastros_Model extends CI_Model {
    
    public $table = "clientes";
    
    function __construct()
    {
      parent::__construct();
    }
    
    function get($data=false, $data2=false)
    {
      if(!$data2)
      {
        $data2 = $this->table;
      }
      
      $this->db->from($data2);
      
      if($data)
      {
        $this->db->where($data);
      }
      
      $this->db->order_by('id', 'desc');
      
      $query = $this->db->get();
      return $query->result();
    }
    
    function set($data=false, $data2=false, $data3=false)
    {    
      if(!$data3)
      {
        $data3 = $this->table;
      }
      
      if($data)
      {
        $this->db->where($data);
        $this->db->update($data3, $data2);
      }
      else
      {
        $this->db->insert($data3, $data2);
        $insert_id = $this->db->insert_id();      
        return $insert_id;
      }   
    }
    
    function auth($data)
    {
      $this->db->select('id, nome, nome_fantasia, email');
      $this->db->from($this->table);
      $this->db->where($data);
      
      $query = $this->db->get();
      //echo $this->db->last_query();
      return $query->result();
    }
    
    function detalhes($data=false, $where_in=false)
    {
        $this->db->select("tb_cliente.*, tb_religiao.nome religiao_nome");
        $this->db->from($this->table . " tb_cliente");
        $this->db->join("religioes tb_religiao", "tb_cliente.religiao_id = tb_religiao.id");
        
        if($data && !$where_in)
        {
            $this->db->where($data);
        }
        
        if($data && $where_in)
        {
            $this->db->where_in('tb_cliente.id', $data);
        }
        
        $this->db->order_by('tb_cliente.id', 'desc');
        
        //echo $this->db->last_query();
        return $this->db->get()->result();        
    }
    
}