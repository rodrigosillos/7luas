<?php

class Pedidos_Model extends CI_Model {
    
    public $table = "pedidos";
    public $table_item = "pedidos_item";
    public $table_produto = "produtos";
    public $table_cliente = "clientes";
    public $table_forma_envio = "formas_envio";
    public $table_forma_pagamento = "formas_pagamento";
    public $table_status = "pedidos_status";
    
    function __construct()
    {
      parent::__construct();
    }
    
    function get($data=false, $data2=false)
    {
      if(!$data2)
      {
        $data2 = $this->table;
      }
      
      $this->db->from($data2);
      
      if($data)
      {
        $this->db->where($data);
      }
      
      $this->db->order_by('id', 'desc');
      
      $query = $this->db->get();
      return $query->result();
    }
    
    function set($data=false, $data2=false, $data3=false)
    {    
        if(!$data3)
        {
            $data3 = $this->table;
        }
        
        if($data)
        {
            $this->db->where($data);
            $this->db->update($data3, $data2);
        }
        else
        {
            $this->db->insert($data3, $data2);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }   
    }
    
    function detalhes($data=false, $where_in=false, $limit=false, $offset=false)
    {
        $this->db->select("
                        tb_pedido.id pedido_id,
                        tb_pedido.data pedido_data,
                        tb_pedido.peso,
                        tb_pedido.prazo,
                        tb_pedido.valor_total pedido_valor,
                        tb_pedido.valor_frete valor_frete,
                        tb_pedido.parcelas,
                        tb_cliente.id cliente_id,
                        tb_cliente.nome cliente_nome,
                        tb_status.nome status_nome,
                        tb_pagamento.nome pagamento_nome,
                        tb_forma_envio.nome envio_nome
                        ");
        
        $this->db->from($this->table . " tb_pedido");
        
        $this->db->join("clientes tb_cliente", "tb_pedido.cliente_id = tb_cliente.id");
        $this->db->join("pedidos_status tb_status", "tb_pedido.status_id = tb_status.id");
        $this->db->join("formas_pagamento tb_pagamento", "tb_pedido.forma_pagamento_id = tb_pagamento.id");
        $this->db->join("formas_envio tb_forma_envio", "tb_pedido.forma_envio_id = tb_forma_envio.id");
        
        if($data && !$where_in)
        {
            $this->db->where($data);
        }
        
        if($data && $where_in)
        {
            $this->db->where_in($data);
        }        
        
        $this->db->order_by('tb_pedido.id', 'desc');
        
        if($limit || $offset)
        {
          $this->db->limit($limit, $offset);
        }        
        
        //$query = $this->db->get();
        //echo $this->db->last_query();
        return $this->db->get()->result();
    }
    
    function get_itens($data=false, $where_in=false)
    {
        $this->db->select("
                        tb_pedido.id,
                        tb_pedido.data,
                        tb_produto.id produto_id,
                        tb_produto.codigo produto_codigo,
                        tb_produto.nome produto_nome,
                        tb_produto.slug produto_slug,
                        tb_produto.descricao produto_descricao,
                        tb_item.quantidade item_quantidade,
                        tb_item.valor_unitario item_valor_unitario,
                        tb_item.valor_total item_valor_total,
                        tb_pedido.peso,
                        tb_pedido.prazo,
                        tb_pedido.valor_frete,
                        tb_pedido.valor_desconto,
                        tb_pedido.valor_subtotal,
                        tb_pedido.valor_total,
                        tb_forma_envio.nome forma_envio,
                        tb_cliente.nome cliente_nome,
                        tb_forma_pagamento.nome forma_pagamento,
                        tb_status.nome status
                        ");
        
        $this->db->from($this->table_item . " tb_item");
        
        $this->db->join($this->table . " tb_pedido", "tb_item.pedido_id = tb_pedido.id");
        $this->db->join($this->table_produto . " tb_produto", "tb_item.produto_id = tb_produto.id");
        $this->db->join($this->table_cliente . " tb_cliente", "tb_pedido.cliente_id = tb_cliente.id");
        $this->db->join($this->table_forma_envio . " tb_forma_envio", "tb_pedido.forma_envio_id = tb_forma_envio.id");
        $this->db->join($this->table_forma_pagamento . " tb_forma_pagamento", "tb_pedido.forma_pagamento_id = tb_forma_pagamento.id");
        $this->db->join($this->table_status . " tb_status", "tb_pedido.status_id = tb_status.id");
        
        if($data && !$where_in)
        {
            $this->db->where($data);
        }
        
        if($data && $where_in)
        {
            $this->db->where_in('tb_item.pedido_id', $data);
        }        
        
        $this->db->order_by('tb_pedido.id', 'desc');
        
        //$query = $this->db->get();
        //echo $this->db->last_query();
        return $this->db->get()->result();
    }
    
}