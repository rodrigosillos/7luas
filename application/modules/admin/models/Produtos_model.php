<?php

class Produtos_Model extends CI_Model {
    
  public $table = "produtos";
  public $table_fornecedor = "fornecedores";
  public $table_religiao = "religioes";
  public $table_categoria = "categorias";
  public $table_subcategoria = "subcategorias";
  public $table_cor = "cores";
  public $table_imagens = "produtos_imagens";
  public $table_lista_desejos = "lista_desejos";
  
  function __construct()
  {
    parent::__construct();
  }
  
  function get_last_record($data=false, $data2=false)
  {
    if(!$data2)
    {
      $data2 = $this->table;
    }
    
    $this->db->from($data2);
    
    if($data)
    {
      $this->db->where($data);
    }    
    
    $this->db->order_by('id', 'desc');
    $this->db->limit(1);
    
    $query = $this->db->get();
    return $query->result();
  }
  
  function get($data=false, $data2=false, $where_in=false)
  {
    if(!$data2)
    {
      $data2 = $this->table;
    }
    
    $this->db->from($data2);
    
    if($data and !$where_in)
    {
      $this->db->where($data);
    }
    
    if($data and $where_in)
    {
      $this->db->where_in('id', $data);
    }    
    
    $this->db->order_by('id', 'desc');
    
    $query = $this->db->get();
    //echo $this->db->last_query();
    return $query->result();
  }
  
  function get_subcategoria($data=false)
  {
    $this->db->from($this->table_subcategoria . ' tb_subcategoria');
    
    if($data)
    {
      $this->db->where($data);
    }    
    
    $query = $this->db->get();
    return $query->result();
  }
  
  function set($data=false, $data2=false, $data3=false)
  {    
    if(!$data3)
    {
      $data3 = $this->table;
    }
    
    if($data)
    {
      $this->db->where($data);
      $this->db->update($data3, $data2);
    }
    else
    {
      $this->db->insert($data3, $data2);
      $insert_id = $this->db->insert_id();      
      return $insert_id;
    }   
  }
  
  function delete($data=false, $data2=false)
  {
    if(!$data2)
    {
      $data2 = $this->table;
    }
    
    $this->db->delete($data2, $data);
  }
  
  function lista($data=false, $limit=false, $offset=false, $count=false, $orderby=false, $ordering="asc", $regexp=false, $where_in=false)
  {
    if($count)
    {
      $this->db->select('tb_produto.id');
    }
    else
    {
      $this->db->select('tb_produto.id, tb_produto.codigo, tb_produto.nome, tb_produto.slug, tb_produto.valor_venda, tb_produto.valor_promocao, tb_produto.promocao, tb_categoria.nome categoria, tb_categoria.slug categoria_slug');
    }
    
    $this->db->join('categorias tb_categoria', 'tb_produto.categoria_id = tb_categoria.id', 'left');
    $this->db->join('religioes tb_religiao', 'tb_produto.religiao_id = tb_religiao.id', 'left');    
    
    $this->db->from($this->table . ' tb_produto');
    
    if($data and !$regexp)
    {
      $this->db->where($data);
    }
    
    if($data and $regexp)
    {
      $this->db->where("tb_produto.tags REGEXP '".$data."'");
    }
    
    if($data and $where_in)
    {
      $this->db->where_in('tb_produto.id', $data);
    }     
    
    if($orderby)
    {
      $this->db->order_by($orderby[0], $orderby[1]);
    }
    else
    {
      $this->db->order_by('tb_produto.nome', $ordering);
    }    

    if($limit || $offset)
    {
      $this->db->limit($limit, $offset);
    }
    
    $query = $this->db->get();
    //echo $this->db->last_query();
    
    if($count)
    {
      return $query->num_rows();
      //return $this->db->count_all_results();
    }
    else
    {
      return $query->result();
    }
  
  }
  
  function detalhes($data, $where_in=false)
  {
    $this->db->select('tb_produto.id,
                      tb_produto.codigo,
                      tb_produto.nome,
                      tb_produto.tags,
                      tb_produto.descricao,
                      tb_produto.valor_venda,
                      tb_produto.valor_promocao,
                      tb_produto.promocao,
                      tb_categoria.nome categoria,
                      tb_subcategoria.nome subcategoria,
                      tb_religiao.nome religiao,
                      tb_fornecedor.nome fornecedor,
                      tb_produto.visitas,
                      tb_produto.categoria_id,
                      tb_cor.id cor_id,
                      tb_cor.nome cor_nome,
                      tb_cor.slug cor_slug,
                      tb_produto.peso,
                      tb_produto.medidas,
                      tb_produto.slug,
                      tb_produto.estoque,
                      tb_produto.valor_atacado,
                      tb_produto.valor_custo,
                      tb_produto.destaque,
                      tb_produto.ativo');
    
    $this->db->join('categorias tb_categoria', 'tb_produto.categoria_id = tb_categoria.id');
    $this->db->join('subcategorias tb_subcategoria', 'tb_produto.subcategoria_id = tb_subcategoria.id', 'left');
    $this->db->join('cores tb_cor', 'tb_produto.cor_id = tb_cor.id', 'left');
    $this->db->join('religioes tb_religiao', 'tb_produto.religiao_id = tb_religiao.id');
    $this->db->join('fornecedores tb_fornecedor', 'tb_produto.fornecedor_id = tb_fornecedor.id', 'left');
    
    $this->db->from($this->table . ' tb_produto');

    if($data and !$where_in)
    {
      $this->db->where($data);
    }
    
    if($data and $where_in)
    {
      $this->db->where_in('tb_produto.id', $data);
    }
    
    $query = $this->db->get();
    //echo $this->db->last_query();
    return $query->result();
  }
  
  function get_fornecedor($data=false)
  {
    $this->db->select('tb_fornecedor.id, tb_fornecedor.nome, tb_fornecedor.slug');
    
    $this->db->from($this->table_fornecedor . ' tb_fornecedor');
    
    if($data)
    {
      $this->db->where($data);
    }
    
    $query = $this->db->get();
    return $query->result();
  }
  
  function get_religiao($data=false)
  {
    $this->db->select('tb_religiao.id, tb_religiao.nome');
    
    $this->db->from($this->table_religiao . ' tb_religiao');
    
    if($data)
    {
      $this->db->where($data);
    }
    
    $query = $this->db->get();
    return $query->result();
  }
  
  function get_categoria($data=false)
  {
    $this->db->select('tb_categoria.id, tb_categoria.nome, tb_categoria.slug');
    
    $this->db->from($this->table_categoria . ' tb_categoria');
    
    if($data)
    {
      $this->db->where($data);
    }
    
    $query = $this->db->get();
    return $query->result();
  }
  
  function get_cor($data=false)
  {
    $this->db->select('tb_cor.id, tb_cor.nome, tb_cor.slug');
    
    $this->db->from($this->table_cor . ' tb_cor');
    
    if($data)
    {
      $this->db->where($data);
    }
    
    $this->db->order_by('tb_cor.nome');
    
    $query = $this->db->get();
    return $query->result();
  }  
  
  function busca_fornecedor($data)
  {
    $this->db->select('id');
    $this->db->from($this->table_fornecedor . ' tb_fornecedor');
    
    if($data)
    {
      $this->db->or_like($data);
    }    
    
    $query = $this->db->get();
    //echo $this->db->last_query();
    return $query->result();
  }  
  
  function busca($data=false, $data2=false)
  {
    $this->db->select('tb_produto.id,
                      tb_produto.codigo,
                      tb_produto.nome,
                      tb_produto.slug,
                      tb_produto.valor_venda,
                      tb_produto.valor_promocao,
                      tb_produto.promocao,
                      tb_produto.codigo,
                      tb_categoria.nome categoria,
                      tb_categoria.slug categoria_slug');
    
    $this->db->from($this->table . ' tb_produto');
    $this->db->join($this->table_categoria . ' tb_categoria', 'tb_produto.categoria_id = tb_categoria.id');
    
    if($data2)
    {
      //$this->db->where(array('tb_produto.ativo' => 1));
      //$this->db->where_in('tb_produto.fornecedor_id', $data2);
    }
    
    $this->db->where(array('tb_produto.ativo' => 1));
    $this->db->like($data);
    
    $this->db->order_by('tb_produto.id', 'desc');
    
    $query = $this->db->get();
    //echo $this->db->last_query();
    return $query->result();
  }
  
  function get_by($data)
  {
    $this->db->select('tb_produto.id, tb_produto.codigo, tb_produto.nome, tb_produto.valor_venda, tb_produto.valor_promocao, tb_produto.promocao, tb_produto.subcategoria_id, tb_categoria.nome categoria, tb_categoria.slug categoria_slug');
    $this->db->from($this->table . ' tb_produto');
    $this->db->join($this->table_categoria . ' tb_categoria', 'tb_produto.categoria_id = tb_categoria.id');
    //$this->db->join($this->table_subcategoria . ' tb_subcategoria', 'tb_produto.subcategoria_id = tb_subcategoria.id');
    $this->db->join($this->table_religiao . ' tb_religiao', 'tb_produto.religiao_id = tb_religiao.id');
    
    if($data)
    {
      $this->db->where($data);
    }
    
    $this->db->order_by('tb_produto.id', 'desc');
    
    $query = $this->db->get();
    
    //echo $this->db->last_query();
    
    return $query->result();
  }
  
  function get_by_preco($data)
  {
    $this->db->select('tb_produto.id, tb_produto.codigo, tb_produto.nome, tb_produto.slug, tb_produto.valor_venda, tb_produto.valor_promocao, tb_produto.promocao, tb_categoria.nome categoria, tb_categoria.slug categoria_slug');
    $this->db->from($this->table . ' tb_produto');
    $this->db->join($this->table_categoria . ' tb_categoria', 'tb_produto.categoria_id = tb_categoria.id');
    $this->db->join($this->table_religiao . ' tb_religiao', 'tb_produto.religiao_id = tb_religiao.id');
    
    if($data)
    {
      $this->db->where($data);
    }
    
    $this->db->order_by('tb_produto.valor_venda');
    
    $query = $this->db->get();
    
    //echo $this->db->last_query();
    
    return $query->result();
  }
  
  function get_wishlist($data)
  {
    $this->db->select('tb_produto.id, tb_produto.codigo, tb_produto.nome, tb_produto.slug, tb_produto.valor_venda, tb_categoria.nome categoria, tb_categoria.slug categoria_slug');
    $this->db->from($this->table_lista_desejos . ' tb_lista');
    $this->db->join($this->table . ' tb_produto', 'tb_lista.produto_id = tb_produto.id');
    $this->db->join($this->table_categoria . ' tb_categoria', 'tb_produto.categoria_id = tb_categoria.id');
    
    if($data)
    {
      $this->db->where($data);
    }
    
    $query = $this->db->get();
    
    //echo $this->db->last_query();
    
    return $query->result();    
  }

}