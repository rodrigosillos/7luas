<?php

class Banners_Model extends CI_Model {
    
    public $table = "banners";
    
    function __construct()
    {
      parent::__construct();
    }
    
    function get($data=false)
    {
      $this->db->from($this->table);
      
      if($data)
      {
        $this->db->where($data);
      }    
      
      $this->db->order_by('id', 'desc');
      
      $query = $this->db->get();
      return $query->result();
    }
    
    function set($data=false, $data2=false)
    {    
      if($data)
      {
        $this->db->where($data);
        $this->db->update($this->table, $data2);
      }
      else
      {
        $this->db->insert($this->table, $data2);
        $insert_id = $this->db->insert_id();      
        return $insert_id;
      }   
    }     
    
}