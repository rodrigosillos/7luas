<?php

class Usuarios_Model extends CI_Model {
    
  public $table = "usuarios";
  
  function __construct(){
    parent::__construct();
  }
  
  function auth($data){
    
    $this->db->select('email, senha');
    $this->db->from($this->table);
    $this->db->where($data);
    
    $query = $this->db->get();
    //echo $this->db->last_query();
    return $query->result();
    
  }
  
  function buscar($data){
    
    $this->db->from($this->table);
    $this->db->where($data);
    
    $query = $this->db->get();
    
    return $query->result();
  
  }
  
  function salvar($data, $param=false){
    
    if($param){
      $this->db->where($param);
      $this->db->update($this->table, $data);
    } else {
      $this->db->insert($this->table, $data);
    }
  
  }  

}