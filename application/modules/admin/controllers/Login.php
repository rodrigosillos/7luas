<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('usuarios_model');
	}

	public function index()
	{
		$this->load->view('login');
	}
	
	public function auth()
	{
		$email = !empty($_POST["email"]) ? $_POST["email"] : false;
		$senha = !empty($_POST["senha"]) ? $_POST["senha"] : false;
		
		$retorno = "nao_logado";
		
		if($email and $senha)
		{
			$data = array('email' => $email, 'senha' => $senha, 'ativo' => 1);
			
			$usuario = $this->usuarios_model->auth($data);
			
			if($usuario)
			{
				$retorno = "logado";
				$this->session->set_userdata('usuario_sessao', $usuario);
			}
		}
		
		echo $retorno;		
	}	
	
	public function teste_servidor()
	{
		$this->output->enable_profiler(TRUE);
	}	

}