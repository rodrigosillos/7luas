<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends MY_Controller {

	public $fornecedores;
	public $religioes;
	public $categorias;
	public $cores;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('produtos_model');
		$this->load->model('usuarios_model');
		
		$this->fornecedores = $this->produtos_model->get_fornecedor();
		$this->religioes = $this->produtos_model->get_religiao();
		$this->categorias = $this->produtos_model->get_categoria();
		$this->cores = $this->produtos_model->get_cor();
	}

	public function index()
	{
		$produtos = array();
		
		if($_POST)
		{
			$data = false;
			
			$religiao_id = $_POST["religiao"];
			
			if($religiao_id <> "todos" and $religiao_id <> "")
			{
				$data = array("religiao_id" => $religiao_id);
			}
			
			if($religiao_id == "")
			{
				$produtos = array();
			}
			else
			{
				$produtos = $this->produtos_model->get($data);
			}			
		}
		
		$data = array('produtos' => $produtos);
		
		$this->load->view('produtos', $data);
	}
	
	public function excluir_promocao()
	{
		if($_POST)
		{
			$acao = $_POST["acao"];
			$cbx_item = $_POST["cbx_item"];
			
			foreach($cbx_item as $produto_id)
			{
				$data = array("id" => $produto_id);
				$data2 = array("promocao" => null);
				
				$this->produtos_model->set($data, $data2);
			}
			
			redirect("/admin/produtos/");
		}
	}
	
	public function gerar_excel()
	{
		//print_r($_POST);
		//die();
		
		$this->load->library('PHPExcel');
		$sheet = new PHPExcel();
		$sheet->getProperties()->setTitle('Relatório de Produtos')->setDescription('Relatório de Produtos');
		
		//conteudo
		$sheet->setActiveSheetIndex(0)
		->setCellValue('A1', 'ID')
		->setCellValue('B1', 'Código')
		->setCellValue('C1', 'Nome')
		->setCellValue('D1', 'Tags')
		->setCellValue('E1', 'Descrição')
		->setCellValue('F1', 'Estoque')
		->setCellValue('G1', 'Peso')
		->setCellValue('H1', 'Medidas')
		->setCellValue('I1', 'Valor Venda')
		->setCellValue('J1', 'Valor Promocao')
		->setCellValue('K1', 'Valor Atacado')
		->setCellValue('L1', 'Valor Custo')
		->setCellValue('M1', 'Destaque')
		->setCellValue('N1', 'Categoria')
		->setCellValue('O1', 'Sub Categoria')
		->setCellValue('P1', 'Religiao')
		->setCellValue('Q1', 'Fornecedor')
		->setCellValue('R1', 'Cor')
		->setCellValue('S1', 'Ativo')
		->setCellValue('T1', 'Promoção')
		;
		
		$i = 2;
		
		$arr_produto = array();
		
		$cbx_item = $_POST["cbx_item"];
		
		foreach($cbx_item as $produto_id)
		{
			array_push($arr_produto, $produto_id);
		}
		
		//print_r($arr_produto);
		//die();
		
		$produtos = $this->produtos_model->detalhes($arr_produto, true);
		
		foreach($produtos as $produto)
		{
			$ativo = $produto->ativo == 1 ? "Sim" : "Não";
			$destaque = $produto->destaque == 1 ? "Sim" : "Não";
			$promocao = $produto->promocao == 1 ? "Sim" : "Não";
			
			$sheet->setActiveSheetIndex(0)
			->setCellValue('A'.$i, $produto->id)
			->setCellValue('B'.$i, $produto->codigo)
			->setCellValue('C'.$i, $produto->nome)
			->setCellValue('D'.$i, $produto->tags)
			->setCellValue('E'.$i, strip_tags($produto->descricao))
			->setCellValue('F'.$i, $produto->estoque)
			->setCellValue('G'.$i, $produto->peso)
			->setCellValue('H'.$i, $produto->medidas)
			->setCellValue('I'.$i, $produto->valor_venda)
			->setCellValue('J'.$i, $produto->valor_promocao)
			->setCellValue('K'.$i, $produto->valor_atacado)
			->setCellValue('L'.$i, $produto->valor_custo)
			->setCellValue('M'.$i, $destaque)
			->setCellValue('N'.$i, $produto->categoria)
			->setCellValue('O'.$i, $produto->subcategoria)
			->setCellValue('P'.$i, $produto->religiao)
			->setCellValue('Q'.$i, $produto->fornecedor)
			->setCellValue('R'.$i, $produto->cor_nome)
			->setCellValue('S'.$i, $ativo)
			->setCellValue('T'.$i, $promocao)
			;
			
			$i++;
		}
		//final conteudo
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="relatorio_produtos.xls"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');
		
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		
		$objWriter = PHPExcel_IOFactory::createWriter($sheet, 'Excel5');
		$objWriter->save('php://output');
		exit;		
	}
	
	public function novo_codigo($data=false)
	{
		$produto = $this->produtos_model->get_last_record();
		$novo_codigo = $produto[0]->codigo + 1;
		
		if($data)
		{
			$novo_codigo = $data;
		}
		
		if(strlen($novo_codigo)==1)
		{
			$novo_codigo = "00000" . $novo_codigo;
		}
		
		if(strlen($novo_codigo)==2)
		{
			$novo_codigo = "0000" . $novo_codigo;
		}
		
		if(strlen($novo_codigo)==3)
		{
			$novo_codigo = "000" . $novo_codigo;
		}
		
		if(strlen($novo_codigo)==4)
		{
			$novo_codigo = "00" . $novo_codigo;
		}
		
		if(strlen($novo_codigo)==5)
		{
			$novo_codigo = "0" . $novo_codigo;
		}
		
		return $novo_codigo;
	}
	
	public function novo()
	{		
		$novo_codigo = $this->novo_codigo();
		
		$data = array('produto_novo_codigo' => $novo_codigo,
					  'fornecedores' => $this->fornecedores,
					  'religioes' => $this->religioes,
					  'categorias' => $this->categorias);
		
		$this->load->view('produto_cadastro', $data);
	}
	
	public function excluir()
	{
		$produto_id = $this->uri->segment(4);
		
		if($this->produtos_model->delete(array('id' => $produto_id)))
		{
			
		}
		
		redirect("/admin/produtos/");
	}
	
	public function editar()
	{
		$produto_id = $this->uri->segment(4);
		
		if($produto_id)
		{
			$produto = $this->produtos_model->get(array('id' => $produto_id));
			
			$categoria_id = $produto[0]->categoria_id;
			$subcategorias = $this->produtos_model->get_subcategoria(array('categoria_id' => $categoria_id));
			
			$data = array('produto' => $produto,
						  'fornecedores' => $this->fornecedores,
						  'religioes' => $this->religioes,
						  'categorias' => $this->categorias,
						  'subcategorias' => $subcategorias,
						  'cores' => $this->cores);
			
			$this->load->view('produto_cadastro', $data);
		}					
	}
	
	public function salvar()
	{		
		$produto_id = isset($_POST['produto_id']) ? $_POST['produto_id'] : false;
		
		unset($_POST['produto_id']);
		unset($_POST['bt_salvar']);
		
		$data = false;
		
		$_POST['slug'] = $this->slugify($_POST['nome']);
		
		if(!isset($_POST['ativo']))
		{
			$_POST['ativo'] = 0;
		}

		if(!isset($_POST['destaque']))
		{
			$_POST['destaque'] = 0;
		}
		
		$post = (array) $_POST;
		
		$msg = "novo";
		
		if($produto_id)
		{
			$msg = "editar";
			$data = array("id" => $produto_id);
		}
		
		$this->produtos_model->set($data, $post);
		
		if($msg=="novo")
		{
			$this->load->view('produto_msg_novo');
		}
		else
		{
			$this->load->view('produto_msg_editar');
		}
		
	}
	
	public function slugify($text)
	{	
		$text = preg_replace('~[^\\pL\d]+~u', '-', $text);
		$text = trim($text, '-');
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		$text = strtolower($text);
		$text = preg_replace('~[^-\w]+~', '', $text);
		
		if(empty($text))
		{
			return 'n-a';
		}
		
		return $text;
	}
	
	public function get_subcategoria()
	{
		if($_POST)
		{
			$opcoes = "<option value=''>Selecione...</option>";
			
			$categoria_id = $_POST["categoria_id"];
			
			$subcategorias = $this->produtos_model->get_subcategoria(array("tb_subcategoria.categoria_id" => $categoria_id));
			
			if($subcategorias)
			{
				foreach($subcategorias as $subcategoria)
				{					
					$opcoes .= "<option value='".$subcategoria->id."'>".$subcategoria->nome."</option>";					
				}				
				echo $opcoes;
			}
			
		}
	}
	
	public function imagens()
	{
		$produto_id = $this->uri->segment(4);
		
		if($produto_id)
		{
			$produto = $this->produtos_model->get(array('id' => $produto_id));
			$imagens = $this->produtos_model->get(array('produto_id' => $produto_id), "produtos_imagens");
			
			$imagem = $this->produtos_model->get_last_record(array("produto_id" => $produto_id), "produtos_imagens");
			
			$posicao = 1;
			
			if($imagem)
			{
				$posicao = $imagem[0]->posicao + 1;
			}			
			
			$data = array('posicao' => $posicao,
						  'produto' => $produto,
						  'imagens' => $imagens);
			
			$this->load->view('produto_imagens', $data);
		}
	}
	
	public function excluir_imagem()
	{
		if($_POST)
		{
			$imagem_id = $_POST["imagem_id"];
			
			$data = array("id" => $imagem_id);
			$imagem = $this->produtos_model->get($data, "produtos_imagens");
			
			if($imagem)
			{
				$imagem_posicao = $imagem[0]->posicao;
				$codigo_produto = $imagem[0]->codigo;
				
				$arr_file = array($codigo_produto . "_55x67_".$imagem_posicao.".jpg",								  
								  $codigo_produto . "_70x86_".$imagem_posicao.".jpg",
								  $codigo_produto . "_140x180_".$imagem_posicao.".jpg",
								  $codigo_produto . "_220x270_".$imagem_posicao.".jpg",
								  $codigo_produto . "_300x180_".$imagem_posicao.".jpg",
								  $codigo_produto . "_430x528_".$imagem_posicao.".jpg",
								  $codigo_produto . "_832x1022_".$imagem_posicao.".jpg");
				
				foreach($arr_file as $file)
				{			
					@unlink("/var/www/htdocs/7luas/upload/produtos/".$codigo_produto."/" . $file);
				}
				
				$this->produtos_model->delete(array('id' => $imagem_id), "produtos_imagens");
			
			}			
		}
	}
	
	public function rm_file()
	{
		///
		
		$arr_file = array("000217__2.jpg");
		
		foreach($arr_file as $file)
		{			
			unlink("/var/www/htdocs/7luas/upload/produtos/000217/" . $file);
		}	
		
	}
	
	public function rename_file()
	{
		rename("/var/www/htdocs/7luas/upload/produtos/000157/000157_430x528_2.jpg",
			   "/var/www/htdocs/7luas/upload/produtos/000157/000157_430x528_1.jpg");
	}
	
	private function set_upload_options($diretorio)
	{
		//upload an image options
        $config['upload_path'] = './upload/produtos/' . $diretorio . '/' ;
        $config['allowed_types'] = 'jpg';
		$config['overwrite'] = true;
		
		return $config;
	}	
	
	public function upload()
	{	
		$this->load->library('upload');
		
		$files = $_FILES;
		$cpt = count($_FILES['imagem']['name']);
		
		$produto_id = $_POST["produto_id"];
		$produto_codigo = $_POST["produto_codigo"];
		$posicao = $_POST["posicao"];
		//$secundaria = $_POST["secundaria"];
		
		$tamanhos = array('55x67', '70x86', '140x180', '220x270', '300x180', '430x528', '832x1022');
		
		$error = false;
		$display_errors = array();
		
		for($i=0; $i<$cpt; $i++)
		{
			$_FILES['imagem']['name'] = $files['imagem']['name'][$i];
			$_FILES['imagem']['type'] = $files['imagem']['type'][$i];
			$_FILES['imagem']['tmp_name'] = $files['imagem']['tmp_name'][$i];
			$_FILES['imagem']['error'] = $files['imagem']['error'][$i];
			$_FILES['imagem']['size'] = $files['imagem']['size'][$i];
			
			if($_FILES['imagem']['size'] > 0)
			{
				//echo $tamanhos[$i] . "<br/>";
				//echo $i . "<br/>";
				
				$_FILES['imagem']['name'] = $produto_codigo . "_" . $tamanhos[$i] . "_" . $posicao .".jpg";
				
				$imagem_nome = $_FILES['imagem']['name'];
				
				$this->upload->initialize($this->set_upload_options($produto_codigo));
				
				$new_path = "/var/www/htdocs/7luas/upload/produtos/" . $produto_codigo;
				
				if (!file_exists($new_path))
				{
					mkdir($new_path, 0770);
				}
				
				if(!$this->upload->do_upload('imagem'))
				{
					$error = true;
					array_push($display_errors, $this->upload->display_errors());
				}
				
			}				

		}
		
		if(!$error)
		{
			$data = array("produto_id" => $produto_id,
						  "codigo" => $produto_codigo,
						  "posicao" => $posicao);
			
			$this->produtos_model->set(false, $data, "produtos_imagens");			
			
			
			$data = array("mensagem" => "Imagem adicionada com sucesso!",
						  "produto_id" => $produto_id);
		}
		else
		{
			$data = array("mensagem" => $display_errors,
						  "produto_id" => $produto_id);
		}
		
		$this->load->view('produto_imagens_msg', $data);
		
	}

}