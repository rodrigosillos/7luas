<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends MY_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->model('cadastros_model');
	}

	public function index()
	{
		$clientes = array();
		
		if($_POST)
		{
			$data = false;
			
			$religiao_id = $_POST["religiao"];
			
			if($religiao_id <> "todos" && $religiao_id <> "")
			{
				$data = array("religiao_id" => $religiao_id);
			}			
			
			if($religiao_id == "")
			{
				$clientes = array();
			}
			else
			{
				$clientes = $this->cadastros_model->get($data);
			}			
		}
		
		$data = array('clientes' => $clientes);
		
		$this->load->view('clientes', $data);
	}
	
	public function editar()
	{
		$cliente_id = $this->uri->segment(4);
		
		if($cliente_id)
		{
			$cliente = $this->cadastros_model->get(array('id' => $cliente_id));
			
			$data = array('cliente' => $cliente);
			
			$this->load->view('cliente_cadastro', $data);
		}					
	}
	
	public function gerar_excel()
	{
		$this->load->library('PHPExcel');
		$sheet = new PHPExcel();
		$sheet->getProperties()->setTitle('Relatório de Clientes')->setDescription('Relatório de Clientes');
		
		//conteudo
		$sheet->setActiveSheetIndex(0)
		->setCellValue('A1', 'ID')
		->setCellValue('B1', 'Tipo')
		->setCellValue('C1', 'Nome')		
		->setCellValue('D1', 'CPF')
		->setCellValue('E1', 'Data de Nascimento')
		->setCellValue('F1', 'Sexo')
		->setCellValue('G1', 'Razão Social')
		->setCellValue('H1', 'Nome Fantasia')
		->setCellValue('I1', 'CNPJ')
		->setCellValue('J1', 'IM')
		->setCellValue('K1', 'IE')
		->setCellValue('L1', 'Email')
		->setCellValue('M1', 'CEP')
		->setCellValue('N1', 'Endereço')
		->setCellValue('O1', 'Número')
		->setCellValue('P1', 'Complemento')
		->setCellValue('Q1', 'Tipo de Endereço')
		->setCellValue('R1', 'Bairro')
		->setCellValue('S1', 'Cidade')
		->setCellValue('T1', 'Estado')
		->setCellValue('U1', 'Newsletter')
		->setCellValue('V1', 'Data Cadastro')
		->setCellValue('W1', 'Ativo')
		->setCellValue('X1', 'Religião');
		
		$i = 2;
		
		$arr_cliente = array();
		
		$cbx_item = $_POST["cbx_item"];
		
		foreach($cbx_item as $cliente_id)
		{
			array_push($arr_cliente, $cliente_id);
		}
		
		$clientes = $this->cadastros_model->detalhes($arr_cliente, true);
		
		foreach($clientes as $cliente)
		{
			$newsletter = $cliente->newsletter == 1 ? "Sim" : "Não";
			
			$sheet->setActiveSheetIndex(0)
			->setCellValue('A'.$i, $cliente->id)
			->setCellValue('B'.$i, $cliente->tipo)
			->setCellValue('C'.$i, $cliente->nome)
			->setCellValue('D'.$i, $cliente->cpf)
			->setCellValue('E'.$i, date("d/m/Y", strtotime($cliente->data_nascimento)))
			->setCellValue('F'.$i, $cliente->sexo)
			->setCellValue('G'.$i, $cliente->razao_social)
			->setCellValue('H'.$i, $cliente->nome_fantasia)
			->setCellValue('I'.$i, $cliente->cnpj)
			->setCellValue('J'.$i, $cliente->im)
			->setCellValue('K'.$i, $cliente->ie)
			->setCellValue('L'.$i, $cliente->email)
			->setCellValue('M'.$i, $cliente->cep)
			->setCellValue('N'.$i, $cliente->endereco)
			->setCellValue('O'.$i, $cliente->numero)
			->setCellValue('P'.$i, $cliente->complemento)
			->setCellValue('Q'.$i, $cliente->tipo_endereco)
			->setCellValue('R'.$i, $cliente->bairro)
			->setCellValue('S'.$i, $cliente->cidade)
			->setCellValue('T'.$i, $cliente->estado)
			->setCellValue('U'.$i, $newsletter)
			->setCellValue('V'.$i, date("d/m/Y H:i:s", strtotime($cliente->data_cadastro)))
			->setCellValue('W'.$i, $cliente->ativo)
			->setCellValue('X'.$i, $cliente->religiao_nome);
			
			$i++;
		}
		//final conteudo
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="relatorio_7luas_clientes.xls"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');
		
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		
		$objWriter = PHPExcel_IOFactory::createWriter($sheet, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}	
    
}