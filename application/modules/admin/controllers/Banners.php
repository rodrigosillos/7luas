<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banners extends MY_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->model('banners_model');
	}

	public function index()
	{
		$banners = $this->banners_model->get();
		
		$data = array('banners' => $banners);
		
		$this->load->view('banners', $data);
	}
    
	public function novo()
	{		
		$this->load->view('banner_cadastro');
	}
    
	public function salvar()
	{		
        $config['upload_path'] = './upload/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 20000;
        $config['max_width'] = 2000;
        $config['max_height'] = 2000;
        
        $this->load->library('upload', $config);
        
        if(!$this->upload->do_upload('imagem'))
        {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('banner_cadastro', $error);
        }
        else
        {
            $banner_id = isset($_POST['banner_id']) ? $_POST['banner_id'] : false;
            
            unset($_POST['banner_id']);
            unset($_POST['bt_salvar']);
            
            $data = false;
            
            $upload_data = $this->upload->data();
            
            $_POST['ativo'] = 1;
            $_POST['imagem'] = $upload_data['file_name'];
            
            $post = (array) $_POST;
            
            //print_r ( $post );            
            //print_r ( $this->upload->data() );
            
            $mensagem = "Banner cadastrado com sucesso!";
            
            if($banner_id)
            {
                $mensagem = "Banner editado com sucesso!";
                $data = array("id" => $banner_id);
            }
            
            $this->banners_model->set($data, $post);
            
            $data = array('mensagem' => $mensagem);
            $this->load->view('banner_cadastro_sucesso', $data);
        }        
	}     
    
}