<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos extends MY_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('pedidos_model','cadastros_model'));
	}

	public function index()
	{
		$pedidos = array();
		
		if($_POST)
		{
			$data = false;
			
			$status_id = $_POST["status"];
			
			if($status_id <> "todos" && $status_id <> "")
			{
				$data = array("status_id" => $status_id);
			}			
			
			if($status_id == "")
			{
				$pedidos = array();
			}
			else
			{
				$pedidos = $this->pedidos_model->detalhes($data);
			}			
		}
		
		$data = array('pedidos' => $pedidos);
		
		$this->load->view('pedidos', $data);
	}
	
	public function detalhes()
	{
		$pedido_id = $this->uri->segment(4);
		
		if($pedido_id)
		{
			$pedido = $this->pedidos_model->detalhes(array("tb_pedido.id" => $pedido_id));
			$pedido_endereco = $this->pedidos_model->get(array("pedido_id" => $pedido_id), "pedidos_endereco");
			$itens = $this->pedidos_model->get_itens(array("tb_item.pedido_id" => $pedido_id));
			$cliente = $this->cadastros_model->get(array("id" => $pedido[0]->cliente_id));
			
			$data = array("pedido" => $pedido,
						  "pedido_endereco" => $pedido_endereco,
						  "cliente" => $cliente,
						  "itens" => $itens);
			
			$this->load->view('pedido_detalhes', $data);
		}
	}
	
	public function alterar_status()
	{
		if($_POST)
		{
			$acao_status = $_POST["acao_status"];
			$cbx_item = $_POST["cbx_item"];
			
			foreach($cbx_item as $pedido_id)
			{
				$data = array("id" => $pedido_id);
				$data2 = array("status_id" => $acao_status);
				
				$this->pedidos_model->set($data, $data2);
			}
			
			redirect("/admin/pedidos/");
		}
	}
	
	public function gerar_excel()
	{
		$this->load->library('PHPExcel');
		$sheet = new PHPExcel();
		$sheet->getProperties()->setTitle('Relatório de Pedidos')->setDescription('Relatório de Pedidos');
		
		//conteudo
		$sheet->setActiveSheetIndex(0)
		->setCellValue('A1', 'Pedido')
		->setCellValue('B1', 'Data')		
		->setCellValue('C1', 'Código Produto')
		->setCellValue('D1', 'Produto')
		->setCellValue('E1', 'Quantidade')
		->setCellValue('F1', 'Valor Unitário')
		->setCellValue('G1', 'Valor Total Produto')
		->setCellValue('H1', 'Peso')
		->setCellValue('I1', 'Prazo')
		->setCellValue('J1', 'Valor Frete')
		->setCellValue('K1', 'Valor Desconto')
		->setCellValue('L1', 'Valor SubTotal')
		->setCellValue('M1', 'Valor Total')
		->setCellValue('N1', 'Forma de Envio')
		->setCellValue('O1', 'Cliente')
		->setCellValue('P1', 'Forma de Pagamento')
		->setCellValue('Q1', 'Status');
		
		$i = 2;
		
		$arr_pedido = array();
		
		$cbx_item = $_POST["cbx_item"];
		
		foreach($cbx_item as $pedido_id)
		{
			array_push($arr_pedido, $pedido_id);
		}
		
		$pedidos = $this->pedidos_model->get_itens($arr_pedido, true);
		
		foreach($pedidos as $pedido)
		{
			$sheet->setActiveSheetIndex(0)
			->setCellValue('A'.$i, $pedido->id)
			->setCellValue('B'.$i, date("d/m/Y H:i:s", strtotime($pedido->data)))
			->setCellValue('C'.$i, $pedido->produto_codigo)
			->setCellValue('D'.$i, $pedido->produto_nome)
			->setCellValue('E'.$i, $pedido->item_quantidade)
			->setCellValue('F'.$i, $pedido->item_valor_unitario)
			->setCellValue('G'.$i, $pedido->item_valor_total)
			->setCellValue('H'.$i, $pedido->peso)
			->setCellValue('I'.$i, $pedido->prazo)
			->setCellValue('J'.$i, $pedido->valor_frete)
			->setCellValue('K'.$i, $pedido->valor_desconto)
			->setCellValue('L'.$i, $pedido->valor_subtotal)
			->setCellValue('M'.$i, $pedido->valor_total)
			->setCellValue('N'.$i, $pedido->forma_envio)
			->setCellValue('O'.$i, $pedido->cliente_nome)
			->setCellValue('P'.$i, $pedido->forma_pagamento)
			->setCellValue('Q'.$i, $pedido->status);
			
			$i++;
		}
		//final conteudo
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="relatorio_pedidos.xls"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');
		
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		
		$objWriter = PHPExcel_IOFactory::createWriter($sheet, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}
    
}