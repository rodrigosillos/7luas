<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
    
	public function __construct()
	{
		parent::__construct();
        $this->load->model(array('pedidos_model',
                                 'cadastros_model'));
	}

	public function index()
	{
		$pedidos = $this->pedidos_model->detalhes(false, false, 6);
        $clientes = $this->cadastros_model->get();
		
		$data = array("pedidos" => $pedidos,
                      "clientes" => $clientes);
        
        $this->load->view('dashboard', $data);
	}    
    
}