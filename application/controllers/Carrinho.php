<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Carrinho extends MY_Controller {

	public $fornecedores;
	public $categorias;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/produtos_model');
		
		$this->fornecedores = $this->produtos_model->get_fornecedor();
		$this->categorias = $this->produtos_model->get_categoria();
		
		$this->load->helper(array('correios_helper'));
	}

	public function index()
	{
		$data = array("cliente_sessao" => $this->cliente_sessao);
		$this->load->view('carrinho', $data);
	}
	
	public function calcula_frete()
	{
		/*
		 * 41106 PAC
		 * 40010 SEDEX
		 * 40045 SEDEX a Cobrar
		 * 40215 SEDEX 10
		*/
		
		if($_POST)
		{			
			$retorno = "";
			
			$cep_entrega = $_POST["cep_entrega"];
			$peso_total = $_POST["peso_total"];
			$cep_origem = '05074000';
			
			if($cep_origem AND $cep_entrega){
				
				$pac = correios_frete('41106', $cep_origem, $cep_entrega, $peso_total);
				$sedex = correios_frete('40010', $cep_origem, $cep_entrega, $peso_total);
				$sedex10 = correios_frete('40215', $cep_origem, $cep_entrega, $peso_total);
				
				if($pac or $sedex or $sedex10)
				{
					$retorno .= "<h5>Formas de Envio:</h5>";
				}
				
				if(!empty($pac))
				{
					$ex_pac = explode("|", $pac);
					$pac_valor = $ex_pac[0];
					$pac_prazo = $ex_pac[1];
					
					$label_prazo = $pac_prazo == 1 ? " dia útil" : " dias úteis";
					
					if($pac_valor <> "0.00")
					{
						$retorno .= "
						<div class=\"radio\">
							<label>
								<input onclick=\"javascript:set_frete('PAC', '".$pac_valor."', '".$pac_prazo."');\" type=\"radio\" name=\"opcao_frete\" id=\"opcao_frete_pac\" value=\"PAC|".$pac_prazo."|".$pac_valor."\">
								PAC (".$pac_prazo.$label_prazo." após a aprovação do pedido) <span>R$ ".$pac_valor."</span>
							</label>
						</div>";						
					}				
				}
				
				if(!empty($sedex))
				{
					$ex_sedex = explode("|", $sedex);
					$sedex_valor = $ex_sedex[0];
					$sedex_prazo = $ex_sedex[1];
					
					$label_prazo = $sedex_prazo == 1 ? " dia útil" : " dias úteis";
					
					if($sedex_valor <> "0.00")
					{
						$retorno .= "
						<div class=\"radio\">
							<label>
								<input onclick=\"set_frete('SEDEX', '".$sedex_valor."', '".$sedex_prazo."');\" type=\"radio\" name=\"opcao_frete\" id=\"opcao_frete_sedex\" value=\"SEDEX|".$sedex_prazo."|".$sedex_valor."\">
								Sedex (".$sedex_prazo.$label_prazo." após a aprovação do pedido) <span>R$ ".$sedex_valor."</span>
							</label>
						</div>";						
					}				
				}
				
				if(!empty($sedex10))
				{
					$ex_sedex10 = explode("|", $sedex10);
					$sedex10_valor = $ex_sedex10[0];
					$sedex10_prazo = $ex_sedex10[1];
					
					$label_prazo = $sedex10_prazo == 1 ? " dia útil" : " dias úteis";
					
					if($sedex10_valor <> "0.00")
					{
						$retorno .= "
						<div class=\"radio\">
							<label>
								<input onclick=\"set_frete('SEDEX10', '".$sedex10_valor."', '".$sedex10_prazo."')\" type=\"radio\" name=\"opcao_frete\" id=\"opcao_frete_sedex\" value=\"SEDEX10|".$sedex10_prazo."|".$sedex10_valor."\">
								Sedex 10 (".$sedex10_prazo.$label_prazo." após a aprovação do pedido) <span>R$ ".$sedex10_valor."</span>
							</label>
						</div>";						
					}
				}
				
				echo $retorno;
			
			} else {
			  
			  echo 'cep_vazio';
			  
			}			
		}
	}
	
	public function set_frete_sessao()
	{
		if($_POST)
		{
			$tipo_frete = $_POST["tipo_frete"];
			$valor_frete = $_POST["valor_frete"];
			$prazo_frete = $_POST["prazo_frete"];
			$cep_calculo_frete = $_POST["cep_calculo_frete"];
			$peso_frete = $_POST["peso_frete"];
			
			$data = array("tipo_frete" => $tipo_frete,
						  "valor_frete" => $valor_frete,
						  "prazo_frete" => $prazo_frete,
						  "cep_calculo_frete"=> $cep_calculo_frete,
						  "peso_frete" => $peso_frete);
			
			$this->session->set_userdata('frete_sessao', $data);
		}
	}
	
	public function insere_item()
	{
		if($_POST)
		{
			$produto_id = $_POST['produto_id'];
			
			$produto = $this->produtos_model->get(array('id' => $produto_id));
			
			if($produto)
			{
				$produto_nome = $produto[0]->nome;
				$produto_codigo = $produto[0]->codigo;
				$produto_descricao = $produto[0]->descricao;
				$produto_preco = $produto[0]->valor_venda;
				
				if($produto[0]->promocao)
				{
					$produto_preco = $produto[0]->valor_promocao;
				}
				
				$produto_peso = $produto[0]->peso;
				
				$data = array(
					'id'      => $produto_id,
					'qty'     => 1,
					'price'   => $produto_preco,
					'name'    => $produto_nome,
					'options' => array('descricao' => $produto_descricao,
									   'peso' => $produto_peso,
									   'codigo' => $produto_codigo)
				);
		
				$this->cart->insert($data);
			}		
		}
	}
	
	public function atualizar()
	{
		$quantidade = isset($_POST['quantidade']) ? explode(",", implode(",", $_POST['quantidade'])) : false;
		$row_id = isset($_POST['row_id']) ? explode(",", implode(",", $_POST['row_id'])) : false;
		
		foreach($quantidade as $key => $value)
		{			
			$data = array('rowid' => $row_id[$key], 'qty' => $quantidade[$key]);			
			$this->cart->update($data);
		}
	}
	
	public function excluir_item()
	{
		$item_id = $this->uri->segment(3);
		
		$data = array(
				'rowid' => $item_id,
				'qty'   => 0
		);
		
		$this->cart->update($data);
		
		redirect("/carrinho/");
	}
	
	public function apaga_carrinho()
	{
		 $this->cart->destroy();
	}

}