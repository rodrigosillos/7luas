<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Suporte extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
        $data = array("cliente_sessao" => $this->cliente_sessao);
		$this->load->view('suporte_contato', $data);
    }    
	
	public function faq()
	{
        $data = array("cliente_sessao" => $this->cliente_sessao);
		$this->load->view('suporte_faq', $data);
    }
	
	public function troca_devolucao()
	{
        $data = array("cliente_sessao" => $this->cliente_sessao);
		$this->load->view('suporte_troca', $data);
    }
	
	public function envio_entrega()
	{
        $data = array("cliente_sessao" => $this->cliente_sessao);
		$this->load->view('suporte_envio', $data);
    }
	
	public function como_comprar()
	{
        $data = array("cliente_sessao" => $this->cliente_sessao);
		$this->load->view('suporte_comprar', $data);
    }
	
	public function contato()
	{
        $data = array("cliente_sessao" => $this->cliente_sessao);
		$this->load->view('suporte_contato', $data);
    }
	
	public function enviar_contato()
	{
		if($_POST)
		{			
			$assunto = $_POST["assunto"];
			$nome = $_POST["nome"];
			$email = $_POST["email"];
			$telefone = $_POST["telefone"];
			$mensagem = $_POST["mensagem"];
			
			$subject = 'Contato Site 7 Luas - ' . $assunto;
			
			$mensagem_mail = "Assunto: " . $assunto . "<br/>";
			$mensagem_mail .= "Nome: " . $nome . "<br/>";
			$mensagem_mail .= "E-mail: " . $email . "<br/>";
			$mensagem_mail .= "Telefone: " . $telefone . "<br/>";
			$mensagem_mail .= "Mensagem: " . $mensagem . "<br/>";
			
			$result = $this->email
				->from('admin@7luas.com', '7 Luas')
				->to('sac@7luas.com')
				->subject($subject)
				->message($mensagem_mail)
				->send();
				
			if($result)
			{
				$this->session->set_flashdata('mensagem', 'Contato enviado com sucesso!');
				redirect("/suporte/contato/");
			}
			
			//echo $this->email->print_debugger();			
		}
	}
    
}