<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastro extends MY_Controller {

    public $now;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/cadastros_model');
        $this->now = date("Y-m-d H:i:s");
	}

	public function index()
	{
		$data = array("cliente_sessao" => $this->cliente_sessao);
		$this->load->view('cadastro', $data);
	}
    
    public function salvar()
    {        
        if($_POST)
		{
			unset($_POST['confirma_senha']);
			unset($_POST['confirma_senha_juridica']);
			
			if ($_POST['tipo'] == "fisica")
			{
				unset($_POST['email_juridica']);
				unset($_POST['senha_juridica']);
				
				$explode = explode("/", $_POST['data_nascimento']);
				$_POST['data_nascimento'] = $explode[2]."-".$explode[1]."-".$explode[0];
			}
			
			if ($_POST['tipo'] == "juridica")
			{
				$_POST["email"] = $_POST['email_juridica'];
				$_POST["senha"] = $_POST['senha_juridica'];
				
				unset($_POST['email_juridica']);
				unset($_POST['senha_juridica']);
			}
			
			$_POST['senha'] = md5($_POST['senha']);
			
			$_POST['data_cadastro'] = $this->now;
			$_POST['ativo'] = 1;
			
			if($_POST['tipo'] == "juridica")
			{
				$_POST['ativo'] = 0;
			}
			
			$cliente_id_update = $_POST['cliente_id'];
			unset($_POST['cliente_id']);
			
			if($cliente_id_update)
			{
				unset($_POST['senha']);
			}
			
			$post = (array) $_POST;
			
			if($post)
			{
				if(isset($cliente_id_update))
				{
					$data = array('id' => $cliente_id_update);
					$this->cadastros_model->set($data, $post);
				}
				else
				{
					$cliente_id = $this->cadastros_model->set(false, $post);
				}				
				
				if($_POST['tipo'] == "juridica")
				{
					$base_url = str_replace("http://", "https://", base_url());
					redirect($base_url . "cadastro/bemvindo");
				}
				else
				{
					if(isset($cliente_id_update))
					{
						$this->session->set_flashdata('msg_cadastro', 'Dados alterados com sucesso!');
						redirect($base_url . "conta/meus_dados/");
					}
					else
					{
						$data = array('email' => $_POST["email"], 'senha' => $_POST['senha']);				
						$cliente = $this->cadastros_model->auth($data);
						
						$this->session->set_userdata('cliente_sessao', $cliente);
						
						$base_url = str_replace("http://", "https://", base_url());
						redirect($base_url . "conta/");						
					}
				}				
			}
		}
        
    }
	
	public function bemvindo()
	{
		$this->load->view('cadastro_bemvindo');
	}
    
	public function verifica_email()
	{
		if($_POST)
		{
			$retorno = false;
			
			$email = $_POST["email"];
			$cadastro = $this->cadastros_model->get(array('email' => $email));
			
			if($cadastro)
			{
				$retorno = true;
			}
			
			echo $retorno;
		}		
	}
	
	public function verifica_duplicidade()
	{
		if($_POST)
		{
			$retorno = false;
			
			$param = $_POST["param"];
			$data = $_POST["data"];
			
			$cadastro = $this->cadastros_model->get(array($param => $data));
			
			if($cadastro)
			{
				$retorno = true;
			}
			
			echo $retorno;			
		}
	}
	
	public function verifica_senha()
	{
		if($_POST)
		{
			$retorno = false;
			
			$senha_atual = $_POST["senha_atual"];
			$cliente = $this->cadastros_model->get(array('id' => $this->cliente_sessao[0]->id));
			
			if($cliente)
			{
				$senha_bd = $cliente[0]->senha;
				$senha_atual = md5($senha_atual);
				
				if($senha_atual == $senha_bd)
				{
					$retorno = true;
				}
			}
			
			echo $retorno;
		}		
	}
	
	public function altera_senha()
	{
		$nova_senha = md5($_POST["nova_senha"]);
		
		if($nova_senha)
		{
			$data = array("senha" => $nova_senha);
			$this->cadastros_model->set(array("id" => $this->cliente_sessao[0]->id), $data);
			
			$subject = '7 Luas - Alteração de senha';			
			
			$cliente_nome = $this->cliente_sessao[0]->nome;
			$cliente_email = $this->cliente_sessao[0]->email;
			
			$data = array('nome_cliente' => $cliente_nome);
			
			$mensagem_mail = $this->load->view('email/alterar_senha', $data, TRUE);
			
			$result = $this->email
				->from('admin@7luas.com', '7 Luas')
				->to($cliente_email)
				->subject($subject)
				->message($mensagem_mail)
				->send();
				
			if($result)
			{
				//$this->session->set_flashdata('mensagem', 'Senha alterada com sucesso!');
				//redirect("/conta/alterar_senha/");
			}			
			
		}
	}
	
	public function add_newsletter()
	{
		if($_POST)
		{
			$email = $_POST["email_newsletter"];
			
			$data = array("email" => $email);
			
			$email_ja_existe = $this->cadastros_model->get($data, "newsletter");
			
			if($email_ja_existe)
			{
				$msg = "Ops! Seu e-mail já está cadastrado.";
			}
			else
			{
				$this->cadastros_model->set(false, $data, "newsletter");
				$msg = "E-mail cadastrado com sucesso!";
			}
			
			echo $msg;
		}
	}
	
	public function login_facebook()
	{
		$this->load->view('login_facebook');
	}
    
}