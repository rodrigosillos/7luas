<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/produtos_model');
	}

	public function index()
	{
		$destaques = $this->produtos_model->lista(array('tb_produto.destaque' => 1), 3, false, false, false, "RANDOM");
		$mais_vendidos = $this->produtos_model->lista(array('tb_produto.categoria_id' => 12), 10, false, false, false, "RANDOM");
		$ofertas = $this->produtos_model->lista(array('tb_produto.promocao' => 1), 10, false, false, false, "RANDOM");
		$novidades = $this->produtos_model->lista(array('tb_produto.ativo' => 1), 8, false, false, false, "RANDOM");		
		
		$data = array("cliente_sessao" => $this->cliente_sessao,
					  "mais_vendidos" => $mais_vendidos,
					  "ofertas" => $ofertas,
					  "novidades" => $novidades,
					  "destaques" => $destaques);
		
		$this->load->view('home', $data);
	}

}