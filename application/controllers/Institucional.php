<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Institucional extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
        $data = array("cliente_sessao" => $this->cliente_sessao);
		$this->load->view('institucional_sobre', $data);
    }
	
	public function sobre()
	{
        $data = array("cliente_sessao" => $this->cliente_sessao);
		$this->load->view('institucional_sobre', $data);
    }	
	
	public function politica_privacidade()
	{
        $data = array("cliente_sessao" => $this->cliente_sessao);
		$this->load->view('institucional_politica', $data);
    }
	
	public function nossos_produtos()
	{
        $data = array("cliente_sessao" => $this->cliente_sessao);
		$this->load->view('institucional_produtos', $data);
    }
	
	public function trabalhe_conosco()
	{
        $data = array("cliente_sessao" => $this->cliente_sessao);
		$this->load->view('institucional_trabalhe', $data);
    }	
    
}