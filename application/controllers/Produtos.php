<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends MY_Controller {

	public $fornecedores;
	public $categorias;
	
	public $segment3;
	public $segment4;
	public $segment5;
	public $segment6;	

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model(array('admin/produtos_model'));
		
		$this->fornecedores = $this->produtos_model->get_fornecedor();
		$this->categorias = $this->produtos_model->get_categoria();
		
		$this->segment3 = $this->uri->segment(3);
		$this->segment4 = $this->uri->segment(4);
		$this->segment5 = $this->uri->segment(5);
		$this->segment6 = $this->uri->segment(6);			
	}
	
	public function index()
	{
		redirect("/produtos/todos/");
	}
	
	public function todos()
	{
		$data = array('tb_produto.ativo' => 1);
		
		$config['base_url'] = '/produtos/todos/';
		$config['total_rows'] = $this->produtos_model->lista($data, false, false, true);
		$config['per_page'] = $this->result_per_page;
		$config['uri_segment'] = 3;
		
		$this->pagination->initialize($config);
		
		$itens_encontrados = $this->produtos_model->lista($data, false, false, true);
		$produtos = $this->produtos_model->lista($data, $this->result_per_page, $this->uri->segment(3), false, false);
		
		$data = array('cliente_sessao' => $this->cliente_sessao,
					  'produtos' => $produtos,
					  'itens_encontrados' => $itens_encontrados,
					  'categorias' => $this->categorias);
		
		$this->load->view('produto_lista', $data);
	}

	public function detalhes()
	{
		$produto_id = $this->uri->segment(3);
		
		if($produto_id)
		{
			$produto = $this->produtos_model->detalhes(array('tb_produto.id' => $produto_id));
			
			if($produto)
			{				
				$visitas = $produto[0]->visitas;
				$categoria_id = $produto[0]->categoria_id;
				$tags = $produto[0]->tags;
				
				$regexp_tags = "";
				$arr_explode = explode(" ", $tags);
				
				foreach($arr_explode as $tag)
				{
					$regexp_tags .= $tag."|";
				}
				
				$regexp_tags = rtrim($regexp_tags, "|");
				
				//visitas
				$this->produtos_model->set(array('id' => $produto_id), array("visitas" => $visitas+1));
				
				$imagens = $this->produtos_model->get(array('produto_id' => $produto_id), "produtos_imagens");
				$quem_viu = $this->produtos_model->lista($regexp_tags, 6, false, false, false, "RANDOM", true);
				$vistos_recentes = $this->produtos_model->lista(array('tb_produto.categoria_id' => $categoria_id), 6, false, false, array('tb_produto.visitas', 'desc'), false);
				
				$data = array('cliente_sessao' => $this->cliente_sessao,
							  'produto_detalhe' => $produto,
							  'quem_viu' => $quem_viu,
							  'vistos_recentes' => $vistos_recentes,
							  'imagens' => $imagens);
				
				$this->load->view('produto_detalhes', $data);					
			}
			else
			{
				redirect(base_url());
			}
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function busca()
	{		
		if($_POST)
		{
			/*
			$fornecedores = $this->produtos_model->busca_fornecedor(array('nome' => $_POST['search']));
			
			$arr_fornecedor = array();

			foreach($fornecedores as $fornecedor)
			{
				$fornecedor_id = $fornecedor->id;
				array_push($arr_fornecedor, $fornecedor_id);
			}
			*/
			
			$data = array('tb_produto.nome' => $_POST['search']);
			
			$produtos = $this->produtos_model->busca($data);
			$itens_encontrados = $this->produtos_model->lista($data, false, false, true);
			
			$data = array('cliente_sessao' => $this->cliente_sessao,
						  'produtos' => $produtos,
						  'categorias' => $this->categorias,
						  'itens_encontrados' => $itens_encontrados);
			
			$this->load->view('produto_lista', $data);
		}
	}
	
	public function categoria()
	{
		$religiao_id = "";
		$categoria_id = "";
		$subcategoria_id = "";
		
		if($this->segment3 <> "false")
		{
			$religiao = $this->produtos_model->get(array('slug' => $this->segment3), "religioes");
			if($religiao){ $religiao_id = $religiao[0]->id; }
		}
		
		if($this->segment4 <> "false")
		{
			$categoria = $this->produtos_model->get(array('slug' => $this->segment4), "categorias");			
			if($categoria){ $categoria_id = $categoria[0]->id; }
		}
		
		if($this->segment5 <> "false")
		{
			$subcategoria = $this->produtos_model->get(array('slug' => $this->segment5), "subcategorias");			
			if($subcategoria){ $subcategoria_id = $subcategoria[0]->id; }
		}
		
		$data = array("tb_produto.religiao_id" => $religiao_id,
					  "tb_produto.categoria_id" => $categoria_id);
		
		$produtos = $this->produtos_model->lista($data);
		$itens_encontrados = $this->produtos_model->lista($data, false, false, true);
		
		// menu categorias
		$categorias_religiao = $this->produtos_model->get(array('religiao_id' => $religiao_id), "religiao_categoria");
		
		$arr_categoria = array();
		
		foreach($categorias_religiao as $categoria)
		{
			array_push($arr_categoria, $categoria->categoria_id);
		}
		
		$categorias = array();
		
		if($arr_categoria){
			
			$categorias = $this->produtos_model->get($arr_categoria, "categorias", true);
		}		
		// fim menu categorias
		
		$data = array('cliente_sessao' => $this->cliente_sessao,
					  'produtos' => $produtos,
					  'itens_encontrados' => $itens_encontrados,
					  'categorias' => $categorias);
		
		$this->load->view('produto_lista', $data);
	}
	
	public function subcategoria()
	{
		$religiao_id = "";
		$categoria_id = "";
		$subcategoria_id = "";
		
		if($this->segment3 <> "false")
		{
			$religiao = $this->produtos_model->get(array('slug' => $this->segment3), "religioes");
			if($religiao){ $religiao_id = $religiao[0]->id; }
		}
		
		if($this->segment4 <> "false")
		{
			$categoria = $this->produtos_model->get(array('slug' => $this->segment4), "categorias");			
			if($categoria){ $categoria_id = $categoria[0]->id; }
		}
		
		if($this->segment5 <> "false")
		{
			$subcategoria = $this->produtos_model->get(array('slug' => $this->segment5), "subcategorias");			
			if($subcategoria){ $subcategoria_id = $subcategoria[0]->id; }
		}
		
		$data = array("tb_produto.religiao_id" => $religiao_id,
					  "tb_produto.categoria_id" => $categoria_id,
					  "tb_produto.subcategoria_id" => $subcategoria_id);
		
		$produtos = $this->produtos_model->lista($data);
		$itens_encontrados = $this->produtos_model->lista($data, false, false, true);
		
		// menu categorias
		$categorias_religiao = $this->produtos_model->get(array('religiao_id' => $religiao_id), "religiao_categoria");
		
		$arr_categoria = array();
		
		foreach($categorias_religiao as $categoria)
		{
			array_push($arr_categoria, $categoria->categoria_id);
		}
		
		$categorias = array();
		
		if($arr_categoria){
			
			$categorias = $this->produtos_model->get($arr_categoria, "categorias", true);
		}
		// fim menu categorias
		
		$data = array('cliente_sessao' => $this->cliente_sessao,
					  'produtos' => $produtos,
					  'itens_encontrados' => $itens_encontrados,
					  'categorias' => $categorias);
		
		$this->load->view('produto_lista', $data);
	}
	
	public function religiao()
	{
		$slug = $this->segment3;
		
		if($slug)
		{
			$data = array('tb_religiao.slug' => $slug);
			
			$produtos = $this->produtos_model->lista($data);
			$itens_encontrados = $this->produtos_model->lista($data, false, false, true);
			
			$religiao = $this->produtos_model->get(array('slug' => $slug), "religioes");
			
			if($religiao)
			{
				#fixme
				$categorias_religiao = $this->produtos_model->get(array('religiao_id' => $religiao[0]->id), "religiao_categoria");
				
				$arr_categoria = array();
				
				foreach($categorias_religiao as $categoria)
				{
					array_push($arr_categoria, $categoria->categoria_id);
				}
				
				#fixme
				$categorias = array();
				
				if($arr_categoria)
				{
					$categorias = $this->produtos_model->get($arr_categoria, "categorias", true);
				}
				
				$data = array('cliente_sessao' => $this->cliente_sessao,
							  'produtos' => $produtos,
							  'itens_encontrados' => $itens_encontrados,
							  'categorias' => $categorias);
				
				$this->load->view('produto_lista', $data);				
			}
		}
		else
		{
			redirect(base_url());	
		}		
	}
	
	public function preco()	
	{		
		$religiao = "";
		$categoria = "";
		$subcategoria = "";
		
		if($this->segment3 <> "false")
		{
			$religiao = $this->produtos_model->get(array('slug' => $this->segment3), "religioes");			
			if($religiao){ $religiao = " AND tb_produto.religiao_id = " . $religiao[0]->id; }			
		}
		
		if($this->segment4 <> "false")
		{
			$categoria = $this->produtos_model->get(array('slug' => $this->segment4), "categorias");			
			if($categoria){ $categoria = " AND tb_produto.categoria_id = " . $categoria[0]->id; }			
		}
		
		if($this->segment5 <> "false")
		{
			$subcategoria = $this->produtos_model->get(array('slug' => $this->segment5), "subcategorias");			
			if($subcategoria){ $subcategoria = " AND tb_produto.categoria_id = " . $subcategoria[0]->id; }			
		}
		
		$faixa_preco = $this->segment6;
		
		if($faixa_preco)
		{
			$where = "";
			
			switch ($faixa_preco)
			{
				case "20":
					$where = "tb_produto.valor_venda BETWEEN '0.00' AND '20.00'".$religiao.$categoria.$subcategoria;
					break;
				case "20~50":
					$where = "tb_produto.valor_venda BETWEEN '20.00' AND '50.00'".$religiao.$categoria.$subcategoria;
					break;
				case "50~100":
					$where = "tb_produto.valor_venda BETWEEN '50.00' AND '100.00'".$religiao.$categoria.$subcategoria;
					break;
				case "100~150":
					$where = "tb_produto.valor_venda BETWEEN '100.00' AND '150.00'".$religiao.$categoria.$subcategoria;
					break;
				case "150~200":
					$where = "tb_produto.valor_venda BETWEEN '150.00' AND '200.00'".$religiao.$categoria.$subcategoria;
					break;
				case "200~250":
					$where = "tb_produto.valor_venda BETWEEN '200.00' AND '250.00'".$religiao.$categoria.$subcategoria;
					break;				
			}			
			
			$produtos = $this->produtos_model->get_by_preco($where);
			$itens_encontrados = $this->produtos_model->lista($where, false, false, true);
			
			$data = array('cliente_sessao' => $this->cliente_sessao,
						  'produtos' => $produtos,
						  'itens_encontrados' => $itens_encontrados,
						  'categorias' => $this->categorias);
			
			$this->load->view('produto_lista', $data);
		}
		else
		{
			redirect(base_url());
		}		
	}
	
	public function a_z()
	{
		$religiao_id = "";
		$categoria_id = "";
		$subcategoria_id = "";
		
		if($this->segment3 <> "false")
		{
			$religiao = $this->produtos_model->get(array('slug' => $this->segment3), "religioes");
			if($religiao){ $religiao_id = $religiao[0]->id; }
		}
		
		if($this->segment4 <> "false")
		{
			$categoria = $this->produtos_model->get(array('slug' => $this->segment4), "categorias");			
			if($categoria){ $categoria_id = $categoria[0]->id; }
		}
		
		if($this->segment5 <> "false")
		{
			$subcategoria = $this->produtos_model->get(array('slug' => $this->segment5), "subcategorias");			
			if($subcategoria){ $subcategoria_id = $subcategoria[0]->id; }
		}
		
		$data = array("tb_produto.religiao_id" => $religiao_id,
					  "tb_produto.categoria_id" => $categoria_id,
					  "tb_produto.subcategoria_id" => $subcategoria_id);
		
		$order_by = array("tb_produto.nome", "asc");
		$produtos = $this->produtos_model->lista($data, false, false, false, $order_by);
		
		$itens_encontrados = $this->produtos_model->lista($data, false, false, true);
		
		// menu categorias
		$categorias_religiao = $this->produtos_model->get(array('religiao_id' => $religiao_id), "religiao_categoria");
		
		$arr_categoria = array();
		
		foreach($categorias_religiao as $categoria)
		{
			array_push($arr_categoria, $categoria->categoria_id);
		}
		
		$categorias = array();
		
		if($arr_categoria){
			
			$categorias = $this->produtos_model->get($arr_categoria, "categorias", true);
		}
		// fim menu categorias
		
		$data = array('cliente_sessao' => $this->cliente_sessao,
					  'produtos' => $produtos,
					  'itens_encontrados' => $itens_encontrados,
					  'categorias' => $categorias);
		
		$this->load->view('produto_lista', $data);
	}	
	
	public function mais_populares()
	{
		$religiao_id = "";
		$categoria_id = "";
		$subcategoria_id = "";
		
		if($this->segment3 <> "false")
		{
			$religiao = $this->produtos_model->get(array('slug' => $this->segment3), "religioes");
			if($religiao){ $religiao_id = $religiao[0]->id; }
		}
		
		if($this->segment4 <> "false")
		{
			$categoria = $this->produtos_model->get(array('slug' => $this->segment4), "categorias");			
			if($categoria){ $categoria_id = $categoria[0]->id; }
		}
		
		if($this->segment5 <> "false")
		{
			$subcategoria = $this->produtos_model->get(array('slug' => $this->segment5), "subcategorias");			
			if($subcategoria){ $subcategoria_id = $subcategoria[0]->id; }
		}
		
		$data = array("tb_produto.religiao_id" => $religiao_id,
					  "tb_produto.categoria_id" => $categoria_id,
					  "tb_produto.subcategoria_id" => $subcategoria_id);
		
		$order_by = array("tb_produto.visitas", "asc");
		$produtos = $this->produtos_model->lista($data, false, false, false, $order_by);
		
		$itens_encontrados = $this->produtos_model->lista($data, false, false, true);
		
		// menu categorias
		$categorias_religiao = $this->produtos_model->get(array('religiao_id' => $religiao_id), "religiao_categoria");
		
		$arr_categoria = array();
		
		foreach($categorias_religiao as $categoria)
		{
			array_push($arr_categoria, $categoria->categoria_id);
		}
		
		$categorias = array();
		
		if($arr_categoria){
			
			$categorias = $this->produtos_model->get($arr_categoria, "categorias", true);
		}
		// fim menu categorias
		
		$data = array('cliente_sessao' => $this->cliente_sessao,
					  'produtos' => $produtos,
					  'itens_encontrados' => $itens_encontrados,
					  'categorias' => $categorias);
		
		$this->load->view('produto_lista', $data);
	}
	
	public function mais_vendidos()
	{
		$religiao_id = "";
		$categoria_id = "";
		$subcategoria_id = "";
		
		if($this->segment3 <> "false")
		{
			$religiao = $this->produtos_model->get(array('slug' => $this->segment3), "religioes");
			if($religiao){ $religiao_id = $religiao[0]->id; }
		}
		
		if($this->segment4 <> "false")
		{
			$categoria = $this->produtos_model->get(array('slug' => $this->segment4), "categorias");			
			if($categoria){ $categoria_id = $categoria[0]->id; }
		}
		
		if($this->segment5 <> "false")
		{
			$subcategoria = $this->produtos_model->get(array('slug' => $this->segment5), "subcategorias");			
			if($subcategoria){ $subcategoria_id = $subcategoria[0]->id; }
		}
		
		$data = array("tb_produto.religiao_id" => $religiao_id,
					  "tb_produto.categoria_id" => $categoria_id,
					  "tb_produto.subcategoria_id" => $subcategoria_id);
		
		$order_by = array("tb_produto.visitas", "asc");
		$produtos = $this->produtos_model->lista($data, false, false, false, $order_by);
		
		$itens_encontrados = $this->produtos_model->lista($data, false, false, true);
		
		// menu categorias
		$categorias_religiao = $this->produtos_model->get(array('religiao_id' => $religiao_id), "religiao_categoria");
		
		$arr_categoria = array();
		
		foreach($categorias_religiao as $categoria)
		{
			array_push($arr_categoria, $categoria->categoria_id);
		}
		
		$categorias = array();
		
		if($arr_categoria){
			
			$categorias = $this->produtos_model->get($arr_categoria, "categorias", true);
		}
		// fim menu categorias
		
		$data = array('cliente_sessao' => $this->cliente_sessao,
					  'produtos' => $produtos,
					  'itens_encontrados' => $itens_encontrados,
					  'categorias' => $categorias);
		
		$this->load->view('produto_lista', $data);	
	}
	
	public function menor_preco()
	{
		$religiao_id = "";
		$categoria_id = "";
		$subcategoria_id = "";
		
		if($this->segment3 <> "false")
		{
			$religiao = $this->produtos_model->get(array('slug' => $this->segment3), "religioes");
			if($religiao){ $religiao_id = $religiao[0]->id; }
		}
		
		if($this->segment4 <> "false")
		{
			$categoria = $this->produtos_model->get(array('slug' => $this->segment4), "categorias");			
			if($categoria){ $categoria_id = $categoria[0]->id; }
		}
		
		if($this->segment5 <> "false")
		{
			$subcategoria = $this->produtos_model->get(array('slug' => $this->segment5), "subcategorias");			
			if($subcategoria){ $subcategoria_id = $subcategoria[0]->id; }
		}
		
		$data = array("tb_produto.religiao_id" => $religiao_id,
					  "tb_produto.categoria_id" => $categoria_id,
					  "tb_produto.subcategoria_id" => $subcategoria_id);
		
		$order_by = array("tb_produto.valor_venda", "asc");
		$produtos = $this->produtos_model->lista($data, false, false, false, $order_by);
		
		$itens_encontrados = $this->produtos_model->lista($data, false, false, true);
		
		// menu categorias
		$categorias_religiao = $this->produtos_model->get(array('religiao_id' => $religiao_id), "religiao_categoria");
		
		$arr_categoria = array();
		
		foreach($categorias_religiao as $categoria)
		{
			array_push($arr_categoria, $categoria->categoria_id);
		}
		
		$categorias = array();
		
		if($arr_categoria){
			
			$categorias = $this->produtos_model->get($arr_categoria, "categorias", true);
		}
		// fim menu categorias
		
		$data = array('cliente_sessao' => $this->cliente_sessao,
					  'produtos' => $produtos,
					  'itens_encontrados' => $itens_encontrados,
					  'categorias' => $categorias);
		
		$this->load->view('produto_lista', $data);
	}
	
	public function add_wishlist()
	{
		$msg = "Você precisa ser cadastrado.";
		
		if(isset($this->cliente_sessao) and !empty($this->cliente_sessao))
		{
			$produto_id = $_POST["produto_id"];
			$cliente_id = $this->cliente_sessao[0]->id;		
			
			$data = array("produto_id" => $produto_id,
						  "cliente_id" => $cliente_id);
			
			$produto_ja_existe = $this->produtos_model->get($data, "lista_desejos");		
			
			if($produto_ja_existe)
			{
				$msg = "Você já adicionou esse produto na lista de desejos.";
			}
			else
			{
				$this->produtos_model->set(false, $data, "lista_desejos");			
				$msg = "Produto adicionado com sucesso!";				
			}
		}
		
		echo $msg;
	}	

}