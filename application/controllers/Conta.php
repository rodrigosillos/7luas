<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Conta extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('admin/cadastros_model', 'admin/produtos_model', 'admin/pedidos_model'));
	}
	
	public function index()
	{
		$ultimos_pedidos = $this->pedidos_model->detalhes(array("cliente_id" => $this->cliente_sessao[0]->id), false, 6);
		$cliente = $this->cadastros_model->get(array("id" => $this->cliente_sessao[0]->id));
		$lista_desejos = $this->produtos_model->get_wishlist(array('cliente_id' => $this->cliente_sessao[0]->id));
		
		$data = array("cliente_sessao" => $this->cliente_sessao,
					  "ultimos_pedidos" => $ultimos_pedidos,
					  "cliente" => $cliente,
					  "lista_desejos" => $lista_desejos);
		
		$this->load->view('minha_conta', $data);
    }
	
	public function meus_pedidos()
	{
        $pedidos = $this->pedidos_model->detalhes(array("cliente_id" => $this->cliente_sessao[0]->id));
		
		$data = array("cliente_sessao" => $this->cliente_sessao,
					  "pedidos" => $pedidos);
		
		$this->load->view('meus_pedidos', $data);
    }
	
	public function meus_dados()
	{        
		$cliente_sessao = $this->cliente_sessao;
		
		$cliente = $this->cadastros_model->get(array('id' => $cliente_sessao[0]->id));
		
		$data = array("cliente_sessao" => $cliente_sessao,
					  "cliente" => $cliente);
		
		$this->load->view('meus_dados', $data);
    }
	
	public function lista_desejos()
	{
        $lista_desejos = $this->produtos_model->get_wishlist(array('cliente_id' => $this->cliente_sessao[0]->id));
		
		$data = array("cliente_sessao" => $this->cliente_sessao,
					  "lista_desejos" => $lista_desejos);
		
		$this->load->view('lista_desejos', $data);
    }
	
	public function alterar_senha()
	{
        $data = array("cliente_sessao" => $this->cliente_sessao);
		$this->load->view('alterar_senha', $data);
    }
	
	public function pedido_detalhes()
	{
        $pedido_id = $this->uri->segment(3);
		
		$pedido = $this->pedidos_model->detalhes(array("tb_pedido.id" => $pedido_id));
		$pedido_endereco = $this->pedidos_model->get(array("pedido_id" => $pedido_id), "pedidos_endereco");
		$pedido_itens = $this->pedidos_model->get_itens(array("pedido_id" => $pedido_id));
		
		$cliente = $this->cadastros_model->get(array("id" => $this->cliente_sessao[0]->id));
		
		$data = array("cliente_sessao" => $this->cliente_sessao,
					  "pedido" => $pedido,
					  "pedido_endereco" => $pedido_endereco,
					  "pedido_itens" => $pedido_itens,
					  "cliente" => $cliente);
		
		$this->load->view('pedido_detalhes', $data);
    }	
    
}