<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('admin/cadastros_model', 'admin/pedidos_model'));
		$this->load->library(array('moip'));
		$this->now = date("Y-m-d H:i:s");
		$this->now_date = date("Y-m-d");
		$this->moip = new Moip();
	}
	
	public function pagamento()
	{
		$cliente = $this->cadastros_model->get(array("id" => $this->cliente_sessao[0]->id));
		
		$data = array("cliente_sessao" => $this->cliente_sessao,
					  "frete_sessao" => $this->frete_sessao,
					  "cliente" => $cliente);
		
		$this->load->view('pagamento', $data);
    }
	
	public function finalizar()
	{
		if($_POST)
		{
			//print_r($_POST);
			
			$mesmo_endereco = isset($_POST["mesmo_endereco_cobranca"]) ? $_POST["mesmo_endereco_cobranca"] : false;
			$novo_endereco = isset($_POST["novo_endereco"]) ? $_POST["novo_endereco"] : false;
			
			$tipo_frete = $this->frete_sessao["tipo_frete"];
			$valor_frete = $this->frete_sessao["valor_frete"];
			
			//forma de envio
			if($tipo_frete == "PAC")
			{
				$forma_envio_id = 1;			
			}
			elseif($tipo_frete == "SEDEX")
			{
				$forma_envio_id = 2;
			}
			else
			{
				$forma_envio_id = 3;
			}			
			
			//forma de pagamento
			$payment = $_POST["payment"];
			
			$forma_pagamento_id = 1;
			
			if($payment == "boleto")
			{
				$forma_pagamento_id = 2;
			}
			
			$pedido_parcelamento = isset($_POST["cartao_parcela"]) ? $_POST["cartao_parcela"] : 1;
			
			//insere pedido
			$data = array("data" => $this->now,
						  "peso" => $this->frete_sessao["peso_frete"],
						  "prazo" => $this->frete_sessao["prazo_frete"],
						  "valor_frete" => $valor_frete,
						  "valor_desconto" => 0.00,
						  "valor_subtotal" => $this->cart->total(),
						  "valor_total" => ($this->cart->total() + $this->frete_sessao["valor_frete"]),
						  "forma_envio_id" => $forma_envio_id,
						  "cliente_id" => $this->cliente_sessao[0]->id,
						  "forma_pagamento_id" => $forma_pagamento_id,
						  "status_id" => 6,
						  "parcelas" => $pedido_parcelamento);
			
			$pedido_id = $this->pedidos_model->set(false, $data);
			
			if($pedido_id)
			{
				//moip pedido
				//$moip = new Moip();
				
				$valor_frete = str_replace(".", "", $valor_frete);
				
				$postData = array(
					'ownId' => "mari@7luas.com",
					'amount' => array('currency' => "BRL",
									  'subtotals' => array('shipping' => $valor_frete)),
					'items' => array());
				
				//insere itens
				foreach ($this->cart->contents() as $items)
				{
					if ($this->cart->has_options($items['rowid']) == TRUE)
					{
						foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value)
						{
							${"item_$option_name"} = $option_value;
						}
					}
					
					$data = array("quantidade" => $items['qty'],
								  "valor_unitario" => $items['price'],
								  "valor_total" => ($items['price'] * $items['qty']),
								  "pedido_id" => $pedido_id,
								  "produto_id" => $items['id'],
								  "produto_codigo" => $item_codigo);
					
					$this->pedidos_model->set(false, $data, "pedidos_item");
					
					//moip itens
					$price = $this->cart->format_number($items['price']);
					$price = str_replace(".", "", $price);
					
					$item_moip = array('product' => $items['name'],
									   'quantity' => $items['qty'],
									   'detail' => "",
									   'price' => $price);
					
					array_push($postData["items"], $item_moip);
				}
				
				$cliente = $this->cadastros_model->get(array("id" => $this->cliente_sessao[0]->id));
				
				//endereco
				if($mesmo_endereco == 1)
				{
					if($cliente)
					{
						$data = array(
							"pedido_id" => $pedido_id,
							"nome" => $cliente[0]->nome,
							"endereco" => $cliente[0]->endereco,
							"numero" => $cliente[0]->numero,
							"complemento" => $cliente[0]->complemento,
							"bairro" => $cliente[0]->bairro,
							"cidade" => $cliente[0]->cidade,
							"estado" => $cliente[0]->estado,
							"cep" => $cliente[0]->cep,
							"telefone" => $cliente[0]->telefone
						);
						
						$this->cadastros_model->set(false, $data, "pedidos_endereco");
					}
				}
				
				if($novo_endereco == "sim")
				{
					$data = array(
						"pedido_id" => $pedido_id,
						"nome" => $_POST["nome"],
						"endereco" => $_POST["endereco"],
						"numero" => $_POST["numero"],
						"complemento" => $_POST["complemento"],
						"bairro" => $_POST["bairro"],
						"cidade" => $_POST["cidade"],
						"estado" => $_POST["estado"],
						"cep" => $_POST["cep"],
						"telefone" => $_POST["telefone"]
					);
					
					$this->cadastros_model->set(false, $data, "pedidos_endereco");
				}
				
				//moip cliente
				$arr_telefone = explode(" ", $cliente[0]->telefone);
				
				$ddd = $arr_telefone[0];
				$ddd = str_replace("(", "", $ddd);
				$ddd = str_replace(")", "", $ddd);
				
				$telefone = $arr_telefone[1];
				$telefone = str_replace("-", "", $telefone);
				
				$moip_costumer = array('ownId' => $cliente[0]->id,
										'fullname' => $cliente[0]->nome,
										'email' => $cliente[0]->email,
										'birthDate' => $cliente[0]->data_nascimento,
										'taxDocument' => array('type' => "CPF",
															   'number' => $cliente[0]->cpf),
										//telefone										
										'phone' => array('countryCode' => "55",
														 'areaCode' => $ddd,
														 'number' => $telefone),
										//endereço entrega
										'shippingAddress' => array('street' => $cliente[0]->endereco,
																   'streetNumber' => $cliente[0]->numero,
																   'complement' => $cliente[0]->complemento,
																   'district' => $cliente[0]->bairro,
																   'city' => $cliente[0]->cidade,
																   'state' => $cliente[0]->estado,
																   'country' => "BRA",
																   'zipCode' => $cliente[0]->cep));
				
				$postData["customer"] = $moip_costumer;
				
				$response = $this->moip->criar_pedido($postData);
				
				//echo "<br/><br/>response pedido: <pre>";
				//print_r($response);
				
				$link_boleto = "";
				
				if(isset($response["id"]))
				{
					$pedido_moip_id = $response["id"];
					
					//$payment = $_POST["payment"];
					//$cartao_numero = $_POST["cartao_numero"];
					$cartao_titular = $_POST["cartao_titular"];
					//$cartao_validade = $_POST["cartao_validade"];
					//$cartao_seguranca = $_POST["cartao_seguranca"];
					//$cartao_parcela = $_POST["cartao_parcela"];
					$hash = $_POST["encrypted_value"];
					
					if($payment == "cartao")
					{
						$postData = array('installmentCount' => $pedido_parcelamento,
										  'fundingInstrument' => array('method' => "CREDIT_CARD",
																	   'creditCard' => array('hash' => $hash,
																							 //'number' => "4073020000000002",
																							 //'expirationMonth' => "12",
																							 //'expirationYear' => "20",
																							 //'cvc' => "312",
																							 'holder' => array('fullname' => $cartao_titular,
																											   'birthdate' => $cliente[0]->data_nascimento,
																											   'taxDocument' => array('type' => "CPF",
																																	  'number' => $cliente[0]->cpf))),
																	   'phone' => array('countryCode' => "55",
																						'areaCode' => $ddd,
																						'number' => $telefone)));
						
						$response = $this->moip->criar_pagamento($postData, $pedido_moip_id);
						
						//echo "<br/><br/>response pagamento: <pre>";
						//print_r($response);
					}					
					
					$link_boleto = "";
					
					if($payment == "boleto")
					{
						$data_vencimento = date('Y-m-d', strtotime($this->now_date. ' + 5 days'));
						
						$postData = array('installmentCount' => 1,
										  'fundingInstrument' => array('method' => "BOLETO",
																	   'boleto' => array('expirationDate' => $data_vencimento)));
						
						$response = $this->moip->criar_pagamento($postData, $pedido_moip_id);
						
						$link_boleto = $response["_links"]["payBoleto"]["redirectHref"]."/print";
						
						//echo "<br/><br/>response pagamento: <pre>";
						//print_r($response);
					}					
				}
			
				//fixme
				$this->cart->destroy();
				
				$subject = '7 Luas - Recebemos o seu pedido';
				
				$cliente_nome = $this->cliente_sessao[0]->nome;
				$cliente_email = $this->cliente_sessao[0]->email;
				
				$data_mail = array('pedido_id' => $pedido_id,
								   'cliente_nome' => $cliente_nome);
				
				$mensagem_mail = $this->load->view('email/pedido_efetuado', $data_mail, TRUE);
				
				$result = $this->email
					->from('admin@7luas.com', '7 Luas')
					->to($cliente_email)
					->subject($subject)
					->message($mensagem_mail)
					->send();				
				
				$pedido = $this->pedidos_model->detalhes(array("tb_pedido.id" => $pedido_id));
				$pedido_endereco = $this->pedidos_model->get(array("pedido_id" => $pedido_id), "pedidos_endereco");
				$pedido_itens = $this->pedidos_model->get_itens(array("pedido_id" => $pedido_id));
				$cliente = $this->cadastros_model->get(array("id" => $this->cliente_sessao[0]->id));	
				
				$data = array("cliente_sessao" => $this->cliente_sessao,
							  "frete_sessao" => $this->frete_sessao,
							  "pedido" => $pedido,
							  "pedido_endereco" => $pedido_endereco,
							  "pedido_itens" => $pedido_itens,
							  "cliente" => $cliente,
							  "link_boleto" => $link_boleto);				
				
				$this->load->view('pedido_finalizado', $data);
			}			
		}		
	}
	
	public function finalizado()
	{
		$this->load->view('pedido_finalizado');
	}
	
	public function moip_webhook()
	{
		$postData = array('resourceId' => "ORD-8Q0UT2O4NISR",
						  'event' => "ORDER.CREATED");
		
		$response = $this->moip->status_pagamento($postData);
		
		//echo "<br/><br/>response status pagamento: <pre>";
		//print_r($response);
	}
	
	public function criar_notificao_moip()
	{
		$postData = array('events' => array("ORDER.*",
											"PAYMENT.AUTHORIZED",
											"PAYMENT.CANCELLED"),
						  'target' => "http://7luas.mypixel.com.br/pedidos/receive_webhook_moip/",
						  'media' => "WEBHOOK");
		
		$response = $this->moip->criar_notificacao($postData);
		
		//echo "<br/><br/>response criar notificação: <pre>";
		//print_r($response);
	}
	
	public function receive_webhook_moip()
	{
		$raw_post = file_get_contents("php://input");
		$json = json_decode($raw_post, true);
		
		foreach ($json as $key => $val) {
			
			if(is_array($val)) {
				//echo "$key:\n";
				$receive = serialize($key);
				$data = array("receive" => $receive);
			} else {
				$data = array("receive" => $key ."=>". $val);
				//echo "$key => $val\n";
			}			
			
			$this->pedidos_model->set(false, $data, "pedidos_webhook");
		
		}
		
		//$receive = serialize($data);		

	}
    
}