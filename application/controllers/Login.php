<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/cadastros_model');
	}
	
	function _remap($method)
	{
		if (method_exists($this, $method)){
			$this->$method($this->uri->segment(2));
		} else {
			$this->index($method);
		}
	}

	public function index($param)
	{
		$data = array("cliente_sessao" => $this->cliente_sessao,
					  "redirect" => $param);
		
		$this->load->view('login', $data);
	}
	
	public function auth()
	{
		$email = !empty($_POST["email"]) ? $_POST["email"] : false;
		$senha = !empty($_POST["senha"]) ? $_POST["senha"] : false;
		$redirect = $_POST["redirect"];
		
		$retorno = "nao_logado;";
		
		if($email and $senha)
		{
			$data = array('email' => $email, 'senha' => md5($senha), 'ativo' => 1);
			
			$usuario = $this->cadastros_model->auth($data);
			
			if($usuario)
			{
				$retorno = "logado;".$redirect;
				$this->session->set_userdata('cliente_sessao', $usuario);
			}
		}
		
		echo $retorno;
	}

}