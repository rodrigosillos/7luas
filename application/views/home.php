<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>7 Luas - Seu Universo Espiritual</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="/assets/css/home.css">
        <!--CSS HOME-->
          <!-- RS5.0 Main Stylesheet -->
          <link rel="stylesheet" type="text/css" href="/assets/revolution/css/settings.css">
          <!-- RS5.0 Layers and Navigation Styles -->
          <link rel="stylesheet" type="text/css" href="/assets/revolution/css/layers.css">
          <link rel="stylesheet" type="text/css" href="/assets/revolution/css/navigation.css">
        <!--END CSS HOME-->

        <script src="/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- NAV HEADER 7 LUAS-->
        <?php include_once('includes/header.php'); ?>
        <!--END NAV HEADER 7 LUAS-->
    
      <div class="container slider-container">
      <!--BANNER SLIDER-->
        <!--<div id="7-luas_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classic-carousel2" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">-->
        <div id="7-luas_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classic-carousel2" style="margin:20px 0 0 0px !important;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
          <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
          <div id="7-luas" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>
              <!-- SLIDE  -->
              <li data-index="rs-1" data-transition="zoomin" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-saveperformance="off"  data-title="BANNER TITLE AQUI" data-description="" data-link="/produtos/todos/">
                <!-- MAIN IMAGE -->
                <img src="/assets/img/banner-home.png"  alt="ALT AQUI"  data-bgposition="center center" data-kenburns="off" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
              </li>

              <!--
              <li data-index="rs-2" data-transition="zoomin" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-saveperformance="off"  data-title="BANNER TITLE AQUI" data-description="">
                <img src="/assets/img/banner-home.jpg"  alt="ALT AQUI"  data-bgposition="center center" data-kenburns="off" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
              </li>

              <li data-index="rs-3" data-transition="zoomin" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-saveperformance="off"  data-title="BANNER TITLE AQUI" data-description="">
                <img src="/assets/img/banner-home.jpg"  alt="ALT AQUI"  data-bgposition="center center" data-kenburns="off" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
              </li>
              -->

            </ul>
            <div class="tp-static-layers"></div>
          </div>
        </div><!-- END REVOLUTION SLIDER -->
      <!--END BANNER SLIDER-->
      </div>

      <div class="container prod1-container">
        <div class="row">
            <div class="col-md-12">
                <h2>Destaques</h2>
                <hr>
            </div>
        </div>        
        <div class="row destaques">
          <?php foreach($destaques as $produto){ ?>
          <!--<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"><a href="/produtos/detalhes/<?php echo $destaque->id; ?>"><img src="/upload/produtos/<?php echo $destaque->codigo; ?>/<?php echo $destaque->codigo; ?>_300x180_1.jpg" class="img-responsive"></a></div>-->
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <a href="/produtos/detalhes/<?php echo $produto->id; ?>">
                <img src="/upload/produtos/<?php echo $produto->codigo; ?>/<?php echo $produto->codigo; ?>_300x180_1.jpg" class="img-responsive">
                  <h3><?php echo $produto->nome; ?></h3>
                </a>
                <a href="/produtos/categoria/<?php echo $produto->categoria_slug; ?>">
                  <h4><?php echo $produto->categoria; ?></h4>
                </a>
                <h5>De: <strike>R$ <?php echo $produto->valor_venda; ?></strike> Por: R$ <?php echo $produto->valor_promocao; ?></h5>
            </div>          
          <?php } ?>
        </div>
      </div>

<!-- PRODUTOS SLIDER-->

<div class="container prod-slider">
  <div class="row">
    <div class="col-md-12">
      <h2>Mais Vendidos</h2>
      <hr>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
           
<div class="rev_slider_wrapper">
 <div id="prod-slider" class="rev_slider"  data-version="5.0">
  <ul>
   
   <?php foreach($mais_vendidos as $produto){ ?>
   <li data-transition="fade">    
      <div class="tp-resizeme furtherclasses"
      id="layer-id-1"
      data-x="center" data-hoffset="0"
      data-y="middle" data-voffset="-10"
      data-width="none"
      data-height="none"
      data-whitespace="nowrap"
      data-transform_idle="o:1;"
      data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:2000;e:Power3.easeInOut;" 
      data-transform_out="opacity:0;s:500;" 
      data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
      data-start="600"
      data-splitin="none"
      data-splitout="none"
      data-responsive_offset="on"
      style="z-index: 5; border-radius:100px;">
        <a href="/produtos/detalhes/<?php echo $produto->id; ?>">
          <img src="/upload/produtos/<?php echo $produto->codigo; ?>/<?php echo $produto->codigo; ?>_140x180_1.jpg" alt="<?php echo $produto->nome; ?>" title="<?php echo $produto->nome; ?>" class="prod2">
          <h3><?php echo $produto->nome; ?></h3>
        </a>
        <a href="/produtos/detalhes/<?php echo $produto->id; ?>">
          <h4><?php echo $produto->categoria; ?></h4>
        </a>
        <?php if($produto->promocao){ ?>
        <h5>De: <strike>R$ <?php echo $produto->valor_venda; ?></strike> Por: R$ <?php echo $produto->valor_promocao; ?></h5>
        <?php } else { ?>
        <h5>R$ <?php echo $produto->valor_venda; ?></h5>
        <?php } ?>
      </div>
   </li>
   <?php } ?>
    
  </ul>       
 </div>
</div><!-- END OF SLIDER TESTIMONIAL SLIDE -->

          </div>
        </div>

      </div>

<!--END PRODUTOS SLIDER-->


<!-- PRODUTOS SLIDER 2-->

<div class="container prod-slider deals">
  <div class="row">
    <div class="col-md-12">
      <h2>Ofertas</h2>
      <hr>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
           
<div class="rev_slider_wrapper">
 <div id="prod-slider-2" class="rev_slider"  data-version="5.0">
  <ul>
   <?php foreach($ofertas as $produto){ ?>
   <li data-transition="fade">    
      <div class="tp-resizeme furtherclasses"
      id="layer-id-1"
      data-x="center" data-hoffset="0"
      data-y="middle" data-voffset="-10"
      data-width="none"
      data-height="none"
      data-whitespace="nowrap"
      data-transform_idle="o:1;"
      data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:2000;e:Power3.easeInOut;" 
      data-transform_out="opacity:0;s:500;" 
      data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
      data-start="600"
      data-splitin="none"
      data-splitout="none"
      data-responsive_offset="on"
      style="z-index: 5; border-radius:100px;">
        <a href="/produtos/detalhes/<?php echo $produto->id; ?>">
          <img src="/upload/produtos/<?php echo $produto->codigo; ?>/<?php echo $produto->codigo; ?>_140x180_1.jpg" alt="<?php echo $produto->nome; ?>" title="<?php echo $produto->nome; ?>" class="prod2">
          <h3><?php echo $produto->nome; ?></h3>
        </a>
        <a href="/produtos/detalhes/<?php echo $produto->id; ?>">
          <h4><?php echo $produto->categoria; ?></h4>
        </a>
        <?php if($produto->promocao){ ?>
        <h5>De: <strike>R$ <?php echo $produto->valor_venda; ?></strike> Por: R$ <?php echo $produto->valor_promocao; ?></h5>
        <?php } else { ?>
        <h5>R$ <?php echo $produto->valor_venda; ?></h5>
        <?php } ?>
      </div>
   </li>
   <?php } ?>
  </ul>       
 </div>
</div><!-- END OF SLIDER TESTIMONIAL SLIDE -->

          </div>
        </div>

      </div>

<!--END PRODUTOS SLIDER 2-->


<!--NOVIDADES-->
<div class="container new-released">
  <div class="row">
    <div class="col-md-12">
      <h2>Novidades</h2>
      <hr>
    </div>
  </div>
  <div class="row">
    <?php foreach($novidades as $produto){ ?>
    <div class="col-md-3">
      <a href="/produtos/detalhes/<?php echo $produto->id; ?>">
          <img src="/upload/produtos/<?php echo $produto->codigo; ?>/<?php echo $produto->codigo; ?>_220x270_1.jpg" alt="<?php echo $produto->nome; ?>" title="<?php echo $produto->nome; ?>" class="prod2 img-responsive">
          <h3><?php echo $produto->nome; ?></h3>
        </a>
        <a href="/produtos/detalhes/<?php echo $produto->id; ?>">
          <h4><?php echo $produto->categoria; ?></h4>
        </a>
        <?php if($produto->promocao){ ?>
        <h5>De: <strike>R$ <?php echo $produto->valor_venda; ?></strike> Por: R$ <?php echo $produto->valor_promocao; ?></h5>
        <?php } else { ?>
        <h5>R$ <?php echo $produto->valor_venda; ?></h5>
        <?php } ?>
    </div>
    <?php } ?>
    
  </div>  
</div>
<!--END NOVIDADES-->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="/assets/js/vendor/bootstrap.min.js"></script>

<script src="/assets/js/main.js"></script>

<!--JS HOME-->
<script type="text/javascript" src="/assets/revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="/assets/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="/assets/js/home.js"></script>
<!--END JS HOME-->

<!--FOOTER-->
<?php include_once('includes/footer.php'); ?>
<!--END FOOTER-->

        <!--REMOVER NO SERVER
        <script type="text/javascript" src="/assets/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="/assets/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="/assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="/assets/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        END REMOVER NO SERVER-->

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
