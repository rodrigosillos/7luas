<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos extends MY_Controller {
	
	public $now;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('admin/cadastros_model', 'admin/pedidos_model'));
		$this->now = date("Y-m-d H:i:s");
	}
	
	public function pagamento()
	{
		$cliente = $this->cadastros_model->get(array("id" => $this->cliente_sessao[0]->id));
		
		$data = array("cliente_sessao" => $this->cliente_sessao,
					  "frete_sessao" => $this->frete_sessao,
					  "cliente" => $cliente);
		
		$this->load->view('pagamento', $data);
    }
	
	public function finalizar()
	{
		if($_POST)
		{
			$mesmo_endereco = $_POST["mesmo_endereco"];
			$novo_endereco = $_POST["novo_endereco"];
			
			//insere pedido
			$data = array("data" => $this->now,
						  "peso" => $this->frete_sessao["peso_frete"],
						  "prazo" => $this->frete_sessao["prazo_frete"],
						  "valor_frete" => $this->frete_sessao["valor_frete"],
						  "valor_desconto" => 0.00,
						  "valor_subtotal" => $this->cart->total(),
						  "valor_total" => ($this->cart->total() + $this->frete_sessao["valor_frete"]),
						  "forma_envio_id" => 1,
						  "cliente_id" => $this->cliente_sessao[0]->id,
						  "forma_pagamento_id" => 1,
						  "status_id" => 6);
			
			$pedido_id = $this->pedidos_model->set(false, $data);
			
			if($pedido_id)
			{
				//insere itens
				foreach ($this->cart->contents() as $items)
				{
					if ($this->cart->has_options($items['rowid']) == TRUE)
					{
						foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value)
						{
							${"item_$option_name"} = $option_value;
						}
					}
					
					$data = array("quantidade" => $items['qty'],
								"valor_unitario" => $items['price'],
								"valor_total" => ($items['price'] * $items['qty']),
								"pedido_id" => $pedido_id,
								"produto_id" => $items['id'],
								"produto_codigo" => $item_codigo);
					
					$this->pedidos_model->set(false, $data, "pedidos_item");
					
				}
				
				//endereco			
				if($mesmo_endereco == "sim")
				{
					$cliente = $this->cadastros_model->get(array("id" => $this->cliente_sessao[0]->id));
					
					if($cliente)
					{
						$data = array(
							"pedido_id" => $pedido_id,
							"nome" => $cliente[0]->nome,
							"endereco" => $cliente[0]->endereco,
							"numero" => $cliente[0]->numero,
							"complemento" => $cliente[0]->complemento,
							"bairro" => $cliente[0]->bairro,
							"cidade" => $cliente[0]->cidade,
							"estado" => $cliente[0]->estado,
							"cep" => $cliente[0]->cep,
							"telefone" => $cliente[0]->telefone
						);
						
						$this->cadastros_model->set(false, $data, "pedidos_endereco");
					}
				}
				
				if($novo_endereco == "sim")
				{
					$data = array(
						"pedido_id" => $pedido_id,
						"nome" => $_POST["nome"],
						"endereco" => $_POST["endereco"],
						"numero" => $_POST["numero"],
						"complemento" => $_POST["complemento"],
						"bairro" => $_POST["bairro"],
						"cidade" => $_POST["cidade"],
						"estado" => $_POST["estado"],
						"cep" => $_POST["cep"],
						"telefone" => $_POST["telefone"]
					);
					
					$this->cadastros_model->set(false, $data, "pedidos_endereco");				
				}				
			}
		}		
	}
	
	public function finalizado()
	{
		$this->cart->destroy();
		
		$data = array("cliente_sessao" => $this->cliente_sessao,
					  "frete_sessao" => $this->frete_sessao);
		
		$this->load->view('pedido_finalizado', $data);
	}
	
	public function moip_cria_pedido()
	{
		$authToken = "Basic MDEwMTAxMDEwMTAxMDEwMTAxMDEwMTAxMDEwMTAxMDE6QUJBQkFCQUJBQkFCQUJBQkFCQUJBQkFCQUJBQkFCQUJBQkFCQUJBQg==";
		
		// The data to send to the API
		$postData = array(
			'ownId' => "mari@7luas.com",
			'amount' => array('currency' => "BRL",
							  'subtotals' => array('shipping' => 15)),
			'items' => [array('product' => "Produto teste moip",
							 'quantity' => 2,
							 'detail' => "pedido cadastrado via api v2",
							 'price' => 100)],
			'customer' => array('ownId' => 15000,
								'fullname' => "Rodrigo Sillos",
								'email' => "rodrigosillos@gmail.com",
								'birthDate' => "1982-10-04",
								'taxDocument' => array('type' => "CPF",
													   'number' => 30182426882),								
								'phone' => array('countryCode' => "55",
												 'areaCode' => "11",
												 'number' => "949115027"),								
								'shippingAddress' => array('street' => "Rua Cayowaa",
														   'streetNumber' => 2085,
														   'complement' => "",
														   'district' => "Perdizes",
														   'city' => "Sao Paulo",
														   'state' => "SP",
														   'country' => "BRA",
														   'zipCode' => "01258011"
														   )));
		
		$ch = curl_init('https://sandbox.moip.com.br/v2/orders');
		
		curl_setopt_array($ch, array(
			CURLOPT_POST => TRUE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_HTTPHEADER => array(
				'Authorization: '.$authToken,
				'Content-Type: application/json'
			),
			CURLOPT_POSTFIELDS => json_encode($postData)
		));
		
		// Send the request
		$response = curl_exec($ch);
		
		// Check for errors
		if($response === FALSE){
			die(curl_error($ch));
		}
		
		// Decode the response
		$responseData = json_decode($response, TRUE);
		
		// Print the date from the response
		echo "<pre>";
		print_r( $responseData );
		
		//print_r($_POST);
	}
	
	public function moip_cria_pagamento()
	{
		$authToken = "Basic MDEwMTAxMDEwMTAxMDEwMTAxMDEwMTAxMDEwMTAxMDE6QUJBQkFCQUJBQkFCQUJBQkFCQUJBQkFCQUJBQkFCQUJBQkFCQUJBQg==";
		
		$postData = array('installmentCount' => 1,
						  'fundingInstrument' => array('method' => "BOLETO",
													   'boleto' => array('expirationDate' => "2016-04-25")));
		
		$ch = curl_init('https://sandbox.moip.com.br/v2/orders/ORD-ROQ5Q2A77HL0/payments');
		
		curl_setopt_array($ch, array(
			CURLOPT_POST => TRUE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_HTTPHEADER => array(
				'Authorization: '.$authToken,
				'Content-Type: application/json'
			),
			CURLOPT_POSTFIELDS => json_encode($postData)
		));
		
		// Send the request
		$response = curl_exec($ch);
		
		echo "<pre>";
		
		//echo json_encode($postData);
		
		// Check for errors
		if($response === FALSE){
			die(curl_error($ch));
		}
		
		// Decode the response
		$responseData = json_decode($response, TRUE);
		
		// Print the date from the response
		print_r( $responseData );
	}
    
}