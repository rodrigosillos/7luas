<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>7 Luas - Seu Universo Espiritual</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="/assets/css/bootstrap-select.min.css">
        <link rel="stylesheet" href="/assets/css/font-awesome.css">
        <link rel="stylesheet" href="/assets/css/account.css">
        <!--CSS PRODUCT_DETAIL-->
        <link href="/assets/royalslider/royalslider.css" rel="stylesheet">
        <link href="/assets/royalslider/skins/universal/rs-universal.css" rel="stylesheet">
        <!--END CSS PRODUCT_DETAIL-->
        <script src="/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- NAV HEADER 7 LUAS-->
        <?php include_once('includes/header.php'); ?>
        <!--END NAV HEADER 7 LUAS-->

     
<div class="container account">
  <div class="row">
    <div class="col-md-2">
      <ul class="menu">
        <li class="active"><a href="/conta/">Minha Conta</a></li>
        <li><a href="/conta/meus_pedidos/">Meus Pedidos</a></li>
        <li><a href="/conta/meus_dados/">Meus Dados</a></li>
        <li><a href="/conta/lista_desejos/">Lista de Desejos</a></li>
        <li><a href="/conta/alterar_senha/">Alterar Senha</a></li>
      </ul>
    </div>
    <div class="col-md-10">
      <div class="row">
        <div class="col-md-12">
          <h1>Minha Conta</h1>
          <hr>
           <p>Olá, <?php echo empty($cliente[0]->nome) ? $cliente[0]->nome_fantasia : $cliente[0]->nome; ?>, bem vindo à sua conta!</p>
          <hr>
        </div>
        <div class="col-md-12">
          <div class="row h2-header">
            <div class="col-xs-6 col-sm-6 col-md-6"><h2>Últimos Pedidos</h2></div>
            <div class="col-xs-6 col-sm-6 col-md-6 text-right"><h2><a href="/conta/meus_pedidos/">Ver Todos</a></h2></div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th>Pedido Nº</th>
                <th>Data</th>
                <th>Para</th>
                <th>Status</th>
                <th>Total do Pedido</th>
              </tr>
              <?php foreach($ultimos_pedidos as $pedido){ ?>
              <tr>
                <td><?php echo $pedido->pedido_id; ?></td>
                <td><?php echo date("d/m/Y H:i:s", strtotime($pedido->pedido_data)); ?></td>
                <td><?php echo $pedido->cliente_nome; ?></td>
                <td><?php echo $pedido->status_nome; ?></td>
                <td class="price">R$ <?php echo $pedido->pedido_valor; ?></td>
              </tr>
              <?php } ?>
            </table>
          </div>
        </div>
        <div class="col-md-12">
          <div class="row h2-header">
            <div class="col-xs-6 col-sm-6 col-md-6"><h2>Meus Dados</h2></div>
            <div class="col-xs-6 col-sm-6 col-md-6 text-right"><h2><a href="/conta/meus_dados/">Editar Dados</a></h2></div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table">
              <tr>
                <td><strong><?php echo empty($cliente[0]->nome) ? $cliente[0]->nome_fantasia : $cliente[0]->nome; ?></strong></td>
                <td><strong>E-mail</strong>: <?php echo $cliente[0]->email; ?></td>
              </tr>
              <tr>
                <td><?php echo $cliente[0]->endereco; ?>, <?php echo $cliente[0]->numero; ?> - <?php echo $cliente[0]->complemento; ?></td>
                <td><strong>Data de Nascimento</strong>: <?php echo date("d/m/Y", strtotime($cliente[0]->data_nascimento)); ?></td>
              </tr>
              <tr>
                <td><?php echo $cliente[0]->bairro; ?> - <?php echo $cliente[0]->cidade; ?> - <?php echo $cliente[0]->estado; ?></td>
                <td><strong>CPF</strong>: <?php echo empty($cliente[0]->cpf) ? $cliente[0]->cnpj : $cliente[0]->cpf; ?></td>
              </tr>
              <tr>
                <td>CEP <?php echo $cliente[0]->cep; ?></td>
                <td><strong>Telefone</strong>: <?php echo $cliente[0]->telefone; ?></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="col-md-12">
          <div class="row h2-header">
            <div class="col-xs-6 col-sm-6 col-md-6"><h2>Lista de Desejos</h2></div>
            <div class="col-xs-6 col-sm-6 col-md-6 text-right"><h2><a href="/conta/lista_desejos/">Ver todos</a></h2></div>
          </div>
        </div>
        <div class="col-md-12 product">
          <div class="row">
            <?php foreach($lista_desejos as $produto){ ?>
            <div class="col-md-4">
                <a href="/produtos/detalhes/<?php echo $produto->id; ?>">
                    <img src="/upload/produtos/<?php echo $produto->codigo; ?>/<?php echo $produto->codigo; ?>_70x86_1.jpg" alt="<?php echo $produto->nome; ?>" title="<?php echo $produto->nome; ?>" class="prod2 img-responsive">
                </a>
                <h4><?php echo $produto->nome; ?></h4>
                <p><?php echo $produto->categoria; ?></p>
                <p>R$ <?php echo $produto->valor_venda; ?></p>
                <a href="javascript:void(0);" rel="<?php echo $produto->id; ?>" id="btn_comprar">Comprar</a>
            </div>
            <?php } ?>
          </div>
        </div>
      </div><!--row-->
    </div>
  </div>
</div>

  <hr class="space"></hr>
</div>

<!--END NOVIDADES-->

<script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="/assets/js/vendor/bootstrap.min.js"></script>
<script src="/assets/js/bootstrap-select.js"></script>
<script src="/assets/js/defaults-pt_BR.js"></script>
<script src="/assets/js/mousehold.js"></script>
<script src="/assets/js/bootstrap-spinner.js"></script>
<script src="/assets/js/main.js"></script>
<script src="/assets/js/engine_carrinho.js"></script>

<!--JS PRODUCT_DETAIL-->
<script src="/assets/js/product_detail.js"></script>
<!--END JS PRODUCT_DETAIL-->
        
<!--FOOTER-->
<?php include_once('includes/footer.php'); ?>
<!--END FOOTER-->        

    </body>
</html>
