<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>7 Luas - Seu Universo Espiritual</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="/assets/css/bootstrap-select.css">
        <link rel="stylesheet" href="/assets/css/font-awesome.css">
        <link rel="stylesheet" href="/assets/css/payment.css">
        <!--CSS PRODUCT_DETAIL-->
        <link href="/assets/royalslider/royalslider.css" rel="stylesheet">
        <link href="/assets/royalslider/skins/universal/rs-universal.css" rel="stylesheet">
        <!--END CSS PRODUCT_DETAIL-->
        <script src="/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- NAV HEADER 7 LUAS-->
        <?php include_once('includes/header.php'); ?>
        <!--END NAV HEADER 7 LUAS-->
<div class="container account">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="row">
        <div class="col-md-12">
          <h1>Fechamento de Pedido</h1>
          <hr>
        </div>
      </div><!--row-->
      <div class="row payment">
      <form name="form_pedido" method="post" action="/pedidos/finalizar/">
        <div class="col-md-4">
          <div class="box">
            <h3>1. Informações Pessoais</h3>
            <p><strong>Endereço de Cobrança</strong></br></br>
            <?php echo empty($cliente[0]->nome) ? $cliente[0]->nome_fantasia : $cliente[0]->nome; ?></br>
            <?php echo $cliente[0]->endereco; ?>, <?php echo $cliente[0]->numero; ?> - <?php echo $cliente[0]->complemento; ?></br>
            <?php echo $cliente[0]->bairro; ?> - <?php echo $cliente[0]->cidade; ?> - <?php echo $cliente[0]->estado; ?></br>
            CEP <?php echo $cliente[0]->cep; ?></br>
            Telefone <?php echo $cliente[0]->telefone; ?></br></p>
            <hr>
            <p><strong>Endereço de Entrega</strong></p>
              <div class="checkbox">
                <label>
                  <input type="checkbox" id="mesmo_endereco_cobranca" name="mesmo_endereco_cobranca" value="1"> Entregar para o mesmo endereço de cobrança
                </label>
              </div>
            <p><span class="new-add">Novo endereço de entrega</span></p>
            <input type="hidden" name="novo_endereco" id="novo_endereco">
            <div class="row address">
              
<div class="col-md-12">
     <div class="row">
     <div class="col-md-12">
       <div class="form-group">
        <label for="nome"><sup>*</sup> Nome completo</label>
        <input type="text" class="form-control obrigatorio" name="nome" id="nome" title="Nome">
      </div>
     </div>
     <div class="col-md-12">
       <div class="form-group">
        <label for="endereco"><sup>*</sup> Endereço</label>
        <input type="text" class="form-control obrigatorio" name="endereco" id="endereco" title="Endereço">
      </div>
     </div>
     <div class="col-md-3">
      <div class="form-group">
        <label for="numero"><sup>*</sup> Número</label>
        <input type="text" class="form-control obrigatorio" name="numero" id="numero" title="Número">
      </div>
    </div>
    <div class="col-md-4">
       <div class="form-group">
          <label for="complemento">Complemento</label>
          <input type="text" class="form-control" name="complemento" id="complemento" title="Complemento">
        </div>
    </div>
   <div class="col-md-4">
     <div class="form-group">
          <label for="bairro"><sup>*</sup> Bairro</label>
          <input type="text" class="form-control obrigatorio" name="bairro" id="bairro" title="Bairro">
      </div>
   </div>
   <div class="col-md-4">
      <div class="form-group">
        <label for="cidade"><sup>*</sup> Cidade</label>
        <input type="text" class="form-control obrigatorio" name="cidade" id="cidade" title="Cidade">
      </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label for="estado"><sup>*</sup> Estado</label>
      <select class="form-control obrigatorio" name="estado" id="estado" title="Estado">
        <option value="AC">AC</option>
        <option value="AL">AL</option>
        <option value="AP">AP</option>
        <option value="AM">AM</option>
        <option value="BA">BA</option>
        <option value="CE">CE</option>
        <option value="DF">DF</option>
        <option value="ES">ES</option>
        <option value="GO">GO</option>
        <option value="MA">MA</option>
        <option value="MT">MT</option>
        <option value="MS">MS</option>
        <option value="MG">MG</option>
        <option value="PA">PA</option>
        <option value="PB">PB</option>
        <option value="PR">PR</option>
        <option value="PE">PE</option>
        <option value="PI">PI</option>
        <option value="RJ">RJ</option>
        <option value="RN">RN</option>
        <option value="RS">RS</option>
        <option value="RO">RO</option>
        <option value="RR">RR</option>
        <option value="SC">SC</option>
        <option value="SP" selected>SP</option>
        <option value="SE">SE</option>
        <option value="TO">TO</option>
      </select>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label for="cep"><sup>*</sup> CEP</label>
      <input type="text" class="form-control obrigatorio" id="cep" name="cep" title="CEP">
    </div>
  </div>
  <div class="col-md-8">
         <div class="form-group">
              <label for="telefone"><sup>*</sup> Telefone</label>
              <input type="text" class="form-control obrigatorio" id="telefone" name="telefone" title="Telefone">
          </div>
  </div>
  </div>
   </div>


            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="box">
            <h3>2. forma de pagamento</h3>
            <div class="form">
              <div class="radio">
                <label>
                  <input type="radio" name="payment" id="payment1" value="cartao" checked>
                  Cartão de Crédito
                </label>
                </br></br>
                <div class="cartao">
                  <div class="form-group">
                  <label for="car_number"><sup>*</sup> Número do Cartão de Crédito</label>
                  <input type="text" class="form-control" name="cartao_numero" id="cartao_numero" placeholder="">
                </div>
                <div class="form-group">
                  <label for="car_number"><sup>*</sup> Nome impresso no Cartão</label>
                  <input type="text" class="form-control" name="cartao_titular" id="cartao_titular" placeholder="">
                </div>
                <!--
                <div class="form-group">
                  <ul class="card-brand">
                    <li><img src="/assets/img/card.png"></li>
                    <li><img src="/assets/img/card.png"></li>
                    <li><img src="/assets/img/card.png"></li>
                    <li><img src="/assets/img/card.png"></li>
                    <li><img src="/assets/img/card.png"></li>
                  </ul>
                  <span style="font-size:11px;">*A bandeira do seu cartão será selecionada automaticamente</span>
                </div>
                -->
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="val"><sup>*</sup> Data de Validade</label>
                      <input type="text" class="form-control" name="cartao_validade" id="cartao_validade" placeholder="MM/AA">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="cod"><sup>*</sup> Cód de Segurança <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Impresso atrás do seu cartão."></i></label>
                      <input type="text" class="form-control" name="cartao_seguranca" id="cartao_seguranca" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="sexo">*Número de Parcelas:</label>
                      <select class="selectpicker" name="cartao_parcela" id="cartao_parcela" data-width="100%">
                        <?php
                        $valor_total_pedido = ($this->cart->total() + $frete_sessao["valor_frete"]);
                        
                        if($valor_total_pedido > 105.00){
                            $valor_parcela_2 = ($valor_total_pedido / 2);
                            $valor_parcela_3 = ($valor_total_pedido / 3);
                        ?>
                        <option value="1">1x de R$ <?php echo $valor_total_pedido; ?></option>
                        <option value="2">2x de R$ <?php echo number_format($valor_parcela_2, 2); ?></option>
                        <option value="3">3x de R$ <?php echo number_format($valor_parcela_3, 2); ?></option>
                        <?php } else { ?>
                        <option value="1">1x de R$ <?php echo $valor_total_pedido; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                    <!--
                    <textarea name="public_key" id="public_key" style="display:none;">
                    -----BEGIN PUBLIC KEY-----
                    MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApI5EIPSOtnEGqc+ATABi
                    sSzLWJODKJSnzBguikS7RB52+t9ZWayvVDN9SeW6FjQ97gdlzUVlHZtcF2djUzmk
                    7AwKina1m1FHKWq3iD5A/rvkQCXdZbC6OjiBW8pVJ1dfzx2wmwaQTC59D0tqOxqm
                    4U3I0rkVCwHygj5jqqldq8QPBwoXUw8tHUBVW5bbBy1/LsU81xRZSPynOKj21yFD
                    /GUzEFVOXebu4QQ/WTfqpRA323V4oKtOWpssZVwjXHYkXTXuJsdwvrJGv3G6bA+c
                    4yzGs1bChktcAYYHdIWUmU5p7fg2g6rvikqmr2lK/5ywWgmpLWsdynSheyQeDbfl
                    TQIDAQAB
                    -----END PUBLIC KEY-----
                    </textarea>
                    -->
                    <textarea name="public_key" id="public_key" style="display:none;">
                    -----BEGIN PUBLIC KEY-----
                    MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkPGg5yuuoYT1WGbViXdI
                    p+cZ0nf0c4xzYBFLF1oalxzxvxW3JWsNGHdFhqXHIKyCDeLc0ggdHIKYSqfqLVGw
                    fZWcXMFIR2h63zOKsOMtMWhu8SqsXifooW9aSrlFVzOTsCFIfXJ9ys5IL5gehXfo
                    4n3jE5qq/Pyu0ibkn0ea8VudQM0tisKfjvwspwHCZMpU02gx2mmO/hNEQo3FJZv2
                    2FdMPK5DLFWnNf9NEShnJfci0vqd1y3EABBDO5bDPRNwPvhgRPxvk1Rk/n/EmxH2
                    Ot3LoPHf6vUOTUl69BCIKR+0bj5LK8YauWgSWB+1fx9lHkuPT7Q2WAQHOrtc1UOl
                    0QIDAQAB
                    -----END PUBLIC KEY-----
                    </textarea>                                        
                    <input type="hidden" name="encrypted_value" id="encrypted_value">
                    <input type="hidden" id="erro_cartao">
                </div>
                </div>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="payment" id="payment2" value="boleto">
                  Boleto
                </label>
                <div class="boleto">
                <ul>
                  <li>Pagamento à vista</li>
                  <li>Boleto válido por três dias</li>
                  <li>O Boleto será gerado após a finalização do pedido</li>
                  <li>O pagamento do boleto poder ser feito em agências bancárias, casas lotéricas, correios ou internet banking</li>
                </ul>
                </div>
              </div>
              <!--
              <div class="radio">
                <label>
                  <input type="radio" name="payment" id="payment3" value="paypal">
                  Paypal
                </label>
                <div class="paypal">
                  <ul>
                    <li>Após o fechamento do pedido, uma nova janela será aberta para que o pagamento seja realizado diretamente no site do PayPal</li>
                    <li>Ambiente de compra totalmente seguro</li>
                    <li>Parcele em até 12x sem juros</li>
                  </ul>
                </div>
              </div>
              -->
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="box">
            <h3>3. Revisão do pedido</h3>
            <div class="row products">
              <div class="col-md-7"><strong>Produto</strong></div>
              <div class="col-md-2"><strong>QTD</strong></div>
              <div class="col-md-3"><strong>Preço</strong></div>
            </div>
            <div class="row products">
              <?php
              foreach ($this->cart->contents() as $items):
                
                if ($this->cart->has_options($items['rowid']) == TRUE):
                    foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value):
                        ${"item_$option_name"} = $option_value;
                    endforeach;
                endif;
              ?>                
              <div class="col-md-7">
                <a href="/produtos/detalhes/<?php echo $items['id']; ?>"><img src="/upload/produtos/<?php echo $item_codigo; ?>/<?php echo $item_codigo; ?>_70x86_1.jpg" alt="<?php echo $items['name']; ?>" title="<?php echo $items['name']; ?>" class="prod2 img-responsive"></a>
                <h4><?php echo $items['name']; ?></h4>
                <!--
                <p>Tamanho: </p>
                <p>Cor: </p>
                -->
              </div>
              <div class="col-md-2"><?php echo $items['qty']; ?></div>
              <div class="col-md-3">R$ <?php echo $this->cart->format_number($items['price'] * $items['qty']); ?></div>
              <?php endforeach; ?>
              <div class="col-md-12" style="font-size: 12px; padding:10px 0;">Prazo de entrega estimado: <?php echo $frete_sessao["prazo_frete"]; echo $frete_sessao["prazo_frete"] == 1 ? " dia útil" : " dias úteis"; ?></div>
            </div>
            <hr>
            <div class="row totals">
              <div class="col-md-6">Subtotal</div>
              <div class="col-md-6 text-right">R$ <?php echo $this->cart->format_number($this->cart->total()); ?></div>
            </div>
            <div class="row totals">
              <div class="col-md-6">Valor do Frete</div>
              <div class="col-md-6 text-right">R$ <?php echo $frete_sessao["valor_frete"]; ?></div>
            </div>
            <div class="row totals">
              <div class="col-md-6">Desconto</div>
              <div class="col-md-6 text-right">R$ 00,00</div>
            </div>
            <div class="row total">
              <div class="col-md-6"><strong>Total</strong></div>
              <div class="col-md-6 text-right"><strong>R$ <?php echo $this->cart->format_number($this->cart->total() + $frete_sessao["valor_frete"]); ?></strong></div>
            </div>
            <a href="javascript:void(0);" class="finish" id="btn_finalizar_pedido">Finalizar Pedido</a>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
  <hr class="space"></hr>
</div>
<!--END NOVIDADES-->

        <script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="/assets/js/vendor/bootstrap.min.js"></script>
        <script src="/assets/js/bootstrap-select.js"></script>
        <script src="/assets/js/defaults-pt_BR.js"></script>
        <script src="/assets/js/mousehold.js"></script>
        <script src="/assets/js/bootstrap-spinner.js"></script>
        <script src="/assets/js/bootstrap-spinner.js"></script>
        <script src="/assets/js/jquery.maskedinput.min.js"></script>
        <script src="/assets/js/engine_pedido.js"></script>
        <script type="text/javascript" src="//assets.moip.com.br/v2/moip.min.js"></script>
        
        <script type="text/javascript">            
            $(document).ready(function() {                
 
            });            
        </script>
        
        <!--FOOTER-->
        <?php include_once('includes/footer.php'); ?>
        <!--END FOOTER-->

    </body>
</html>
