<?php
  $logado = false;
  
  if(isset($cliente_sessao) and !empty($cliente_sessao))
  {
    $logado = true;
    $nome = $cliente_sessao[0]->nome;
    $nome_fantasia = $cliente_sessao[0]->nome_fantasia;
    $nome_exibicao = empty($nome) ? $nome_fantasia : $nome;
  }
?>
<nav class="navbar-inverse navbar-default nav-7-luas">
<div class="container first-menu">
  <div class="row">
    <div class="col-xs-4 col-sm-2 col-md-2 brand"><a href="<?php echo base_url(); ?>"><img src="/assets/img/7-luas.png" alt="7 Luas" class="img-responsive"></a></div>
    <div class="hidden-xs col-sm-6 col-md-6 slogan"><img src="/assets/img/seu-universo-espiritual.png" alt="Seu Universo Espiritual" class="img-responsive"></div>
    <div class="col-xs-8 col-sm-4 col-md-4">
      <div class="row">
        <?php if($logado){ ?>
          <div class="col-md-12">
            <ul class="access">
              <li>Olá, <?php echo $nome_exibicao; ?></li>
              <li class="divider">|</li>
              <li><a href="<?php //echo str_replace("http://", "https://", base_url()); ?>/conta/">Minha Conta</a> <a href="<?php //echo str_replace("http://", "https://", base_url()); ?>/logout/">( Sair )</a></li>
            </ul>
          </div>
        <?php } else { ?>
          <div class="col-md-12">
            <ul class="access">
              <li><a href="<?php //echo str_replace("http://", "https://", base_url()); ?>/conta/">Acesse sua Conta</a></li>
              <li class="divider">|</li>
              <li><a href="<?php //echo str_replace("http://", "https://", base_url()); ?>/cadastro/">Cadastre-se</a></li>
            </ul>
          </div>                
        <?php } ?>
      </div>
      <div class="row menu-icons">
        <div class="col-md-12">
          <ul class="buttons">
            <li class="star"><a href="<?php //echo str_replace("http://", "https://", base_url()); ?>/conta/lista_desejos/">&nbsp;</a></li>
            <li class="divider"></li>
            <li class="cart"><a href="<?php echo base_url(); ?>carrinho/"><?php echo $this->cart->total_items(); ?></a></li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <form class="form-inline form-search" name="form_busca" method="post" action="/produtos/busca/">
            <div class="form-group">
              <input type="text" class="form-control" title="Busca" name="search" id="search" placeholder="O que você procura?">
               <button type="submit" id="btn_buscar" class="btn btn-default"><img src="/assets/img/search.png"></button>
            </div>                   
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="home" href="<?php echo base_url(); ?>"><img src="/assets/img/home.png"></a>
  </div>
  <div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav nav-cat">
      <li><a href="<?php echo base_url(); ?>produtos/todos/">Produtos</a></li>
      <li><a href="<?php echo base_url(); ?>produtos/religiao/budismo/">Budismo</a></li>      
      <li><a href="<?php echo base_url(); ?>produtos/religiao/candomble/">Candomblé</a></li>
      <li><a href="<?php echo base_url(); ?>produtos/religiao/kardecismo/">Kardecismo</a></li>      
      <li><a href="<?php echo base_url(); ?>produtos/religiao/esoterismo/">Esoterismo</a></li>
      <li><a href="<?php echo base_url(); ?>produtos/religiao/umbanda/">Umbanda</a></li>
      <li><a href="<?php //echo str_replace("http://", "https://", base_url()); ?>/login/">Atacado</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right offers">
      <li><a href="/produtos/todos/">OFERTAS</a></li>
    </ul>
  </div><!--/.nav-collapse -->
</div><!--/.container-fluid -->
</nav>