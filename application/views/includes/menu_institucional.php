<?php
$segment = $this->uri->segment(2);
?>
<ul class="menu">
  <li <?php if($segment == "sobre"){ ?>class="active"<?php } ?>><a href="/institucional/sobre/">Sobre a 7 Luas</a></li>
  <li <?php if($segment == "politica_privacidade"){ ?>class="active"<?php } ?>><a href="/institucional/politica_privacidade/">Política de Privacidade</a></li>
  <li <?php if($segment == "nossos_produtos"){ ?>class="active"<?php } ?>><a href="/institucional/nossos_produtos/">Nossos Produtos</a></li>
  <li <?php if($segment == "trabalhe_conosco"){ ?>class="active"<?php } ?>><a href="/institucional/trabalhe_conosco/">Trabalhe Conosco</a></li>
</ul>