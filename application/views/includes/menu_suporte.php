<?php
$segment = $this->uri->segment(2);
?>
<ul class="menu">
  <li <?php if($segment == "faq"){ ?>class="active"<?php } ?>><a href="/suporte/faq/">Dúvidas frequentes</a></li>
  <li <?php if($segment == "troca_devolucao"){ ?>class="active"<?php } ?>><a href="/suporte/troca_devolucao/">Trocas e devoluções</a></li>
  <li <?php if($segment == "envio_entrega"){ ?>class="active"<?php } ?>><a href="/suporte/envio_entrega/">Envio e entrega</a></li>
  <li <?php if($segment == "como_comprar"){ ?>class="active"<?php } ?>><a href="/suporte/como_comprar/">Como comprar</a></li>
  <li <?php if($segment == "contato"){ ?>class="active"<?php } ?>><a href="/suporte/contato/">Fale Conosco</a></li>
</ul>