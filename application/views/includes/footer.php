<script src="/assets/js/engine_newsletter.js"></script>
<script src="/assets/js/engine_busca.js"></script>
<script src="/assets/js/engine.js"></script>
<script type="text/javascript" src="//seal.alphassl.com/SiteSeal/alpha_image_115-55_en.js"></script>
<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-2 logo">
        <img src="/assets/img/7-luas.png" alt="7 Luas" class="img-responsive">
      </div>
      <div class="col-md-2 v-menu">
        <ul>
          <li>AJUDA E SUPORTE</li>
          <li>- <a href="<?php echo base_url(); ?>suporte/faq/">Dúvidas Frequentes</a></li>
          <li>- <a href="<?php echo base_url(); ?>suporte/troca_devolucao/">Troca e Devolução</a></li>
          <li>- <a href="<?php echo base_url(); ?>suporte/envio_entrega/">Envio e Entrega</a></li>
          <li>- <a href="<?php echo base_url(); ?>suporte/como_comprar/">Como Comprar</a></li>
          <li>- <a href="<?php echo base_url(); ?>suporte/contato/">Fale Conosco</a></li>
        </ul>
      </div>
      <div class="col-md-3 v-menu">
        <ul>
          <li>INSTITUCIONAL</li>
          <li>- <a href="<?php echo base_url(); ?>institucional/sobre/">Sobre a 7 Luas</a></li>
          <li>- <a href="<?php echo base_url(); ?>institucional/politica_privacidade/">Política de Privacidade</a></li>
          <li>- <a href="<?php echo base_url(); ?>institucional/nossos_produtos/">Nossos Produtos</a></li>
          <li>- <a href="<?php echo base_url(); ?>institucional/trabalhe_conosco/">Trabalhe Conosco</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <div class="newsletter">
          <!--Fique por dentro das</br> novidades e Promoções-->
          <form name="form_newsletter" method="post">
            <div class="form-group newsletter">
              <label for="exampleInputEmail1">Fique por dentro das</br> novidades e Promoções</label>
              <input type="text" class="form-control" id="email_newsletter" name="email_newsletter" placeholder="Digite seu e-mail aqui" title="E-mail">
            </div>
            <button type="submit" id="btn_enviar" class="btn btn-default">ENVIAR!</button>
          </form>          
        </div>
        <div class="follow-us">Siga a 7 Luas</div>
        <ul class="social">
          <li><a href="https://www.facebook.com/7-LUAS-1529510930707874/" target="_blank"><img src="/assets/img/facebook.png"></a></li>
          <!--<li><a href="#"><img src="/assets/img/twitter.png"></a></li>-->
          <li><a href="https://www.instagram.com/7luasss/" target="_blank"><img src="/assets/img/instagram.png"></a></li>
          <li><a href="https://br.pinterest.com/7luas0498/" target="_blank"><img src="/assets/img/pinterest.png"></a></li>
        </ul>
      </div>
      <div class="col-md-2">
        <div class="payments">
          FORMAS DE PAGAMENTO
          <img alt="Visa, Master, American Express, Diners, Elo e Hipercard" src="/assets/img/bandeiras.jpg" width="265" height="55">
        </div>
        <div class="payments">
          COMPRE COM SEGURANÇA
          <span id="ss_img_wrapper_115-55_image_en">
            <a href="http://www.alphassl.com/ssl-certificates/wildcard-ssl.html" target="_blank" title="SSL Certificates">
              <img alt="Wildcard SSL Certificates" border=0 id="ss_img" src="//seal.alphassl.com/SiteSeal/images/alpha_noscript_115-55_en.gif" title="SSL Certificate">
            </a>
          </span>
        </div>
      </div>
    </div>
  </div>
<div class="footer-2">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        CNPJ 24.279.342/0001-50 | Rua Dronsfield - 421 - Conjunto 56 - Lapa - São Paulo - SP</br>
        7 Luas © - Todos os Direitos Reservados.
      </div>
       <div class="col-md-4 text-right">
       Desenvolvido por <a href="http://mypixel.com.br/" target="_blank">MyPixel</a>
       </div>
    </div>
  </div>
</div>
</div>