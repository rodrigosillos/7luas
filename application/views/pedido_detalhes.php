<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>7 Luas - Seu Universo Espiritual</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="/assets/css/bootstrap-select.min.css">
        <link rel="stylesheet" href="/assets/css/font-awesome.css">
        <link rel="stylesheet" href="/assets/css/account.css">
        <!--CSS PRODUCT_DETAIL-->
        <link href="/assets/royalslider/royalslider.css" rel="stylesheet">
        <link href="/assets/royalslider/skins/universal/rs-universal.css" rel="stylesheet">
        <!--END CSS PRODUCT_DETAIL-->
        <script src="/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- NAV HEADER 7 LUAS-->
        <?php include_once('includes/header.php'); ?>
        <!--END NAV HEADER 7 LUAS-->

     
<div class="container account">
  <div class="row">
    <div class="col-xs-12 col-sm-2 col-md-2">
      <ul class="menu">
        <li><a href="/conta/">Minha Conta</a></li>
        <li class="active"><a href="/conta/meus_pedidos/">Meus Pedidos</a></li>
        <li><a href="/conta/meus_dados/">Meus Dados</a></li>
        <li><a href="/conta/lista_desejos/">Lista de Desejos</a></li>
        <li><a href="/conta/alterar_senha/">Alterar Senha</a></li>
      </ul>
    </div>
    <div class="col-xs-12 col-sm-10 col-md-10">
      <div class="row order-detail">
        <div class="col-md-12">
          <h1>Pedido <?php echo $pedido[0]->pedido_id; ?> - <?php echo $pedido[0]->status_nome; ?></h1>
          <hr>
          <p>Data do Pedido: <?php echo date("d/m/Y H:i:s", strtotime($pedido[0]->pedido_data)); ?></p>
          <hr>
        </div>
        <div class="col-md-4">
          <strong>Endereço de Cobrança</strong></br>
          <?php echo $cliente[0]->nome; ?></br>
          <?php echo $cliente[0]->endereco; ?>, <?php echo $cliente[0]->numero; ?> - <?php echo $cliente[0]->complemento; ?></br>
          <?php echo $cliente[0]->bairro; ?> - <?php echo $cliente[0]->cidade; ?> - <?php echo $cliente[0]->estado; ?></br>
          CEP <?php echo $cliente[0]->cep; ?></br>
          Telefone <?php echo $cliente[0]->telefone; ?>
          <hr>
          <strong>Endereço de Entrega</strong></br>
          <?php if($pedido_endereco){ ?>
          
          <?php echo $pedido_endereco[0]->nome; ?></br>
          <?php echo $pedido_endereco[0]->endereco; ?>, <?php echo $pedido_endereco[0]->numero; ?> - <?php echo $pedido_endereco[0]->complemento; ?></br>
          <?php echo $pedido_endereco[0]->bairro; ?> - <?php echo $pedido_endereco[0]->estado; ?> - <?php echo $pedido_endereco[0]->estado; ?></br>
          CEP <?php echo $pedido_endereco[0]->cep; ?></br>
          Telefone <?php echo $pedido_endereco[0]->telefone; ?>
          
          <?php } ?>
        </div>
        <div class="col-md-4" style="border-left: 1px solid rgba(0,0,0,0.4);">
          <strong>Forma de Entrega</strong></br>
          Correios - <?php echo $pedido[0]->envio_nome; ?> - <?php echo $pedido[0]->prazo; ?> <?php echo $pedido[0]->prazo == 1 ? " dia útil" : " dias úteis"; ?>
          <hr>
          <strong>Forma de Pagamento</strong></br>
          <?php echo $pedido[0]->pagamento_nome; ?></br>
          <!--
          Bandeira: VISA</br>
          Número do Cartão: XXXX-4362</br>
          Parcelamento: 2x sem juros
          -->
        </div>
        <hr>
        <div class="col-md-12 table-responsive">
          <table class="table order-detail">
            <tr>
              <td>Produtos</td>
              <td>Cod. Produto</td>
              <td>QTD</td>
              <td>Preço Unit.</td>
              <td>Subtotal</td>
            </tr>
            <?php foreach($pedido_itens as $item){ ?>
            <tr>
              <td class="product">
                <div class="row">
                  <div>
                      <a href="/produtos/detalhes/<?php echo $item->produto_id; ?>/<?php echo $item->produto_slug; ?>">
                        <img src="/upload/produtos/<?php echo $item->produto_codigo; ?>/<?php echo $item->produto_codigo; ?>_70x86_1.jpg" alt="<?php echo $item->produto_nome; ?>" title="<?php echo $item->produto_nome; ?>" class="prod2 img-responsive">
                      </a>
                      <h4><?php echo $item->produto_nome; ?></h4>
                      <p><?php echo substr(strip_tags($item->produto_descricao), 0, 150); ?></p>
                      <!--
                      <p>Tamanho: </p>
                      <p>Cor: </p>
                      -->
                  </div>
                </div>
              </td>
              <td><?php echo $item->produto_codigo; ?></td>
              <td><?php echo $item->item_quantidade; ?></td>
              <td class="price">R$ <?php echo $item->item_valor_unitario; ?></td>
              <td class="price">R$ <?php echo $item->item_valor_total; ?></td>
            </tr>
            <?php } ?>
          </table>
        </div>
        <hr>
        <div class="col-md-3 table-responsive" style="float:right;">
          <table class="table order-detail">
            <!--
            <tr>
              <td>Subtotal</td>
              <td>R$ <?php //echo $pedido[0]->pedido_valor_subtotal; ?></td>
            </tr>
            -->
            <tr>
              <td>Frete</td>
              <td>R$ <?php echo $pedido[0]->valor_frete; ?></td>
            </tr>
            <tr>
              <td class="price">Valor Total</td>
              <td class="price">R$ <?php echo $pedido[0]->pedido_valor; ?></td>
            </tr>
          </table>
        </div>
        <div class="col-md-12">
          <a href="/conta/meus_pedidos/" class="back"><< Voltar</a>
        </div>
      </div><!--row-->
    </div>
  </div>
</div>

  <hr class="space"></hr>
</div>

<!--END NOVIDADES-->

<script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="/assets/js/vendor/bootstrap.min.js"></script>
<script src="/assets/js/bootstrap-select.js"></script>
<script src="/assets/js/defaults-pt_BR.js"></script>
<script src="/assets/js/mousehold.js"></script>
<script src="/assets/js/bootstrap-spinner.js"></script>
<script src="/assets/js/main.js"></script>

<!--JS PRODUCT_DETAIL-->
<script src="/assets/js/product_detail.js"></script>
<!--END JS PRODUCT_DETAIL-->

<!--FOOTER-->
<?php include_once('includes/footer.php'); ?>
<!--END FOOTER-->

    </body>
</html>
