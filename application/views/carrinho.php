<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>7 Luas - Seu Universo Espiritual </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="/assets/css/bootstrap-select.min.css">
        <link rel="stylesheet" href="/assets/css/font-awesome.css">
        <link rel="stylesheet" href="/assets/css/cart.css">
        <!--CSS PRODUCT_DETAIL-->
        <link href="/assets/royalslider/royalslider.css" rel="stylesheet">
        <link href="/assets/royalslider/skins/universal/rs-universal.css" rel="stylesheet">
        <!--END CSS PRODUCT_DETAIL-->
        <script src="/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- NAV HEADER 7 LUAS-->
        <?php include_once('includes/header.php'); ?>
        <!--END NAV HEADER 7 LUAS-->
     
<div class="container cart">
  <div class="row">
    <div class="col-md-12">
      <h1>Meu Carrinho</h1>
      <hr></hr>
    </div>
  </div>

  <?php if($this->cart->total_items() <> 0){ ?>
  <div class="row">
    <div class="col-md-4"><h2>Produto</h2></div>
    <div class="col-md-2"><h2>Quantidade</h2></div>
    <div class="col-md-1"></div>
    <div class="col-md-2"><h2>Preço Unitário</h2></div>
    <div class="col-md-1"></div>
    <div class="col-md-2"><h2>Preço Total</h2></div>
  </div>
  <!-- PRODUCT -->
  <form action="/carrinho/atualizar/" name="form_carrinho" method="post">
  <?php
  $peso_total = 0;
  $spinner = 1;
  foreach ($this->cart->contents() as $items):
  ?>
  <div class="row">
    <div class="col-md-4 product">
      <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->cart->has_options($items['rowid']) == TRUE):
                foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value):
                    ${"item_$option_name"} = $option_value;
                endforeach;
            endif;
            $peso_total += ($item_peso * $items['qty']);
            ?>
            <a href="/produtos/detalhes/<?php echo $items['id']; ?>">
              <img src="/upload/produtos/<?php echo $item_codigo; ?>/<?php echo $item_codigo; ?>_70x86_1.jpg" alt="<?php echo $items['name']; ?>" title="<?php echo $items['name']; ?>" class="prod2 img-responsive">
            </a>
            <h4><?php echo $items['name']; ?></h4>
          <p><?php echo substr(strip_tags($item_descricao), 0, 100) . " <a href='/produtos/detalhes/" . $items['id'] . "'>...</a>"; ?></p>
          <!--
          <p>Tamanho: </p>
          <p>Cor: </p>
          -->
        </div>
      </div>
    </div>
    <div class="col-md-2">
        <div class="input-group input-tncrementers">
            <span class="input-group-btn">
                <button type="button" class="btn btn-default" data-value="-1" data-target="#spinner<?php echo $spinner; ?>" data-toggle="spinner">
                    <span class="glyphicon glyphicon-minus"></span>
                </button>
            </span>
            <input type="text" data-ride="spinner" id="spinner<?php echo $spinner; ?>" name="quantidade[]" class="form-control input-number text-center" value="<?php echo $items['qty']; ?>" data-min="0" data-max="10">
            <span class="input-group-btn">
                <button type="button" class="btn btn-default" data-value="1" data-target="#spinner<?php echo $spinner; ?>" data-toggle="spinner" data-on="mousehold">
                    <span class="glyphicon glyphicon-plus"></span>
                </button>
            </span>
        </div>
        <p style="text-align: center;"><a href="/carrinho/excluir_item/<?php echo $items['rowid']; ?>">excluir</a></p>
    <input type="hidden" name="row_id[]" value="<?php echo $items['rowid']; ?>">
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-2"><h2>R$ <?php echo $this->cart->format_number($items['price']); ?></h2></div>
    <div class="col-md-1"></div>
    <div class="col-md-2"><h2>R$ <?php echo $this->cart->format_number($items['price'] * $items['qty']); ?></h2></div>
  </div>
  <?php
    $spinner++;
  endforeach;
  ?>
  </form>
  <input type="hidden" id="peso_total" value="<?php echo $peso_total; ?>">
  <input type="hidden" id="tipo_frete">
  <input type="hidden" id="valor_frete_sessao">
  <input type="hidden" id="prazo_frete">
  <input type="hidden" id="cep_calculo_frete">
  <input type="hidden" id="peso_frete">
  <input type="hidden" id="base_url" value="<?php echo str_replace("http://", "https://", base_url()); ?>">
<!--end PRODUCT-->
  <div class="row">
    <div class="col-md-12">
      <hr>
    </div>
    <div class="col-md-6 continue-shopping">
      <a href="/produtos/todos/">Continuar Comprando</a>
    </div>
    <div class="col-md-6 update-values">
      <a href="javascript:void(0)" id="btn_atualizar_valores">Atualizar Valores</a>
    </div>
    <div class="col-md-12">
      <hr>
    </div>
  </div>

  <div class="row frete">
    <div class="col-md-12">
      <h2><i class="fa fa-truck"></i> Calcular Frete</h2>
    </div>
    <div class="col-md-8">
      <form class="form-inline">
        <div class="form-group">
          <label for="cep">Digite o CEP de entrega para calcular o frete e prazo de entrega:</label>
          <input type="text" class="form-control only_number" id="cep" maxlength="8" placeholder="CEP">
        </div>
        <button type="button" id="btn_calcular" class="btn btn-default">Calcular</button>
        <div id="loading" style="display: none;"><img src="/assets/img/loading_spinner.gif" width="45" height="45"></div>
      </form>
    </div>
    <div class="col-md-2"><h2>Frete</h2></div>
    <div class="col-md-2" id="valor_frete"><h2>R$ 00,00</h2></div>

    <div class="col-md-12 forma-envio" style="display: none;">
    </div>

  </div>

  <div class="row">
    <div class="col-md-12">
      <hr>
      <h2><i class="fa fa-tag"></i> Cupom de Desconto</h2>
    </div>
  </div>
  <div class="col-md-8 cupom">
    <form class="form-inline">
        <div class="form-group">
          <label for="frete">Informe aqui seu cupom promocional:</label>
          <input type="text" class="form-control" id="cupom" placeholder="">
        </div>
        <button type="button" class="btn btn-default">Aplicar</button>
        <!--<p class="help-block"><i class="fa fa-check"></i> Desconto válido!</p>-->
      </form>
  </div>
  <div class="col-md-4">
    <div class="total">
      <div class="row">
        <div class="col-md-6">Subtotal</div>
        <div class="col-md-6">R$ <?php echo $this->cart->format_number($this->cart->total()); ?></div>
      </div>
      <div class="row">
        <div class="col-md-6">Valor do Frete</div>
        <div class="col-md-6" id="resumo_valor_frete">R$ 00,00</div>
      </div>
      <div class="row">
        <div class="col-md-6">Desconto</div>
        <div class="col-md-6">R$ 00,00</div>
      </div>
      <div class="row total">
        <div class="col-md-6">Total</div>
        <div class="col-md-6" id="resumo_valor_total">R$ <?php echo $this->cart->format_number($this->cart->total()); ?></div>
      </div>
    </div>
    <a href="javascript:void(0);" id="btn_finalizar_pedido" class="finish">Finalizar Pedido</a>
    <a href="/produtos/todos/" class="continue">Continuar Comprando</a>
  </div>
  <?php } else { ?>
  <p>Seu carrinho está vazio.</p>
  <?php } ?>

  <hr class="space"></hr>
</div>

<!--END NOVIDADES-->

<script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="/assets/js/vendor/bootstrap.min.js"></script>
<script src="/assets/js/bootstrap-select.js"></script>
<script src="/assets/js/defaults-pt_BR.js"></script>
<script src="/assets/js/mousehold.js"></script>
<script src="/assets/js/bootstrap-spinner.js"></script>
<script src="/assets/js/main.js"></script>

<!--JS PRODUCT_DETAIL
<script src="/assets/js/product_detail.js"></script>
END JS PRODUCT_DETAIL-->
<script src="/assets/js/engine_carrinho.js"></script>

<script>
    function set_frete(tipo_frete, valor_frete, prazo_frete) {
        document.getElementById("valor_frete").innerHTML = "<h2>R$ " + valor_frete + "</h2>";
        document.getElementById("resumo_valor_frete").innerHTML = "R$ " + valor_frete;
        
        document.getElementById("tipo_frete").value = tipo_frete;
        document.getElementById("valor_frete_sessao").value = valor_frete;
        document.getElementById("prazo_frete").value = prazo_frete;
        document.getElementById("cep_calculo_frete").value = document.getElementById("cep").value;
        document.getElementById("peso_frete").value = document.getElementById("peso_total").value;
        
        valor_total = <?php echo $this->cart->format_number($this->cart->total()); ?>;
        
        document.getElementById("resumo_valor_total").innerHTML = "R$ " + parseFloat(parseFloat(valor_total)+parseFloat(valor_frete)).toFixed(2);
    }    
</script>

<!--FOOTER-->
<?php include_once('includes/footer.php'); ?>
<!--END FOOTER-->

    </body>
</html>
