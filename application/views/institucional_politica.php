<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>7 Luas - Seu Universo Espiritual</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="/assets/css/bootstrap-select.css">
        <link rel="stylesheet" href="/assets/css/font-awesome.css">
        <link rel="stylesheet" href="/assets/css/support.css">
        <!--CSS PRODUCT_DETAIL-->
        <link href="/assets/royalslider/royalslider.css" rel="stylesheet">
        <link href="/assets/royalslider/skins/universal/rs-universal.css" rel="stylesheet">
        <!--END CSS PRODUCT_DETAIL-->
        <script src="/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- NAV HEADER 7 LUAS-->
        <?php include_once('includes/header.php'); ?>
        <!--END NAV HEADER 7 LUAS-->
<div class="container account">
  <div class="row">
    <div class="col-xs-12 col-sm-2 col-md-2">
        <?php include_once('includes/menu_institucional.php'); ?>
    </div>
    <div class="col-xs-12 col-sm-10 col-md-10">
      <div class="row">
        <div class="col-md-12">
          <h1>Política de Privacidade</h1>
          <hr>
        </div>
        <div class="col-md-12">
          <div>
            <p><strong>INFORMAÇÕES DO USUÁRIO</strong></p>
            <p>Nossa política de privacidade visa garantir que as informações relativas aos usuários não serão fornecidas, publicadas ou comercializadas em quaisquer circunstâncias. A 7luas obtém informações dos usuários apenas através do cadastro de compras. </p>
            <p><strong>CADASTRO</strong></p>
            <p>A 7luas solicita que o cliente informe dados pessoais, como: Nome completo, CPF, RG, endereço, CEP, estado, cidade, telefone e e-mail com o objetivo de controlar os envios dos produtos comprados, para ações de publicidade, aprimorar o conteúdo sobre os seus produtos e serviços além de possibilitar o rápido contato para as solicitações recebidas. </p>
            <p><strong>SEGURANÇA DAS INFORMAÇÕES</strong></p>
            <p>Os dados pessoais informados no nosso site são armazenadas em um banco de dados com acesso restrito a apenas alguns funcionários qualificados que são obrigados a manter confidencialidade por força de contrato. </p>
            <p>Caso você queira ser retirado de nossa base de dados envie um e-mail para sac@7luas.com solicitando a exclusão de seu nome de nossa base de dados.</p>
          </div>
        </div>
      </div><!--row-->
    </div>
  </div>
</div>
  <hr class="space"></hr>
</div>
<!--END NOVIDADES-->

        <script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="/assets/js/vendor/bootstrap.min.js"></script>
        <script src="/assets/js/bootstrap-select.js"></script>
        <script src="/assets/js/defaults-pt_BR.js"></script>
        <script src="/assets/js/mousehold.js"></script>
        <script src="/assets/js/bootstrap-spinner.js"></script>
        <script src="/assets/js/bootstrap-spinner.js"></script>
        <script src="/assets/js/jquery.maskedinput.min.js"></script>

        <!--JS PRODUCT_DETAIL-->
        <script src="/assets/js/product_detail.js"></script>
        <!--END JS PRODUCT_DETAIL-->

<!--FOOTER-->
<?php include_once('includes/footer.php'); ?>
<!--END FOOTER-->

    </body>
</html>
