<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>7 Luas - Seu Universo Espiritual</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="/assets/css/font-awesome.css">
        <link rel="stylesheet" href="/assets/css/product_detail.css">
        <!--CSS PRODUCT_DETAIL-->
        <link href="/assets/royalslider/royalslider.css" rel="stylesheet">
        <link href="/assets/royalslider/skins/universal/rs-universal.css" rel="stylesheet">
        <!--END CSS PRODUCT_DETAIL-->

        <script src="/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- NAV HEADER 7 LUAS-->
        <?php include_once('includes/header.php'); ?>
        <!--END NAV HEADER 7 LUAS-->

     


<div class="container">
  <div class="row">
    <div class="col-md-12 breadcrumb">
      Você está em: <a href="<?php echo base_url(); ?>">Home</a> > <a href="#"><?php echo $produto_detalhe[0]->categoria; ?></a> > <a href="#"><?php echo $produto_detalhe[0]->subcategoria; ?></a> > <?php echo $produto_detalhe[0]->nome; ?>
    </div>
    <div class="col-md-6">
      <div id="product-detail" class="product-x royalSlider rsUni">
        <?php
        if($imagens){
            $i = 1;
            foreach($imagens as $imagem){
                $codigo_produto = $imagem->codigo;
        ?>
        <a class="rsImg"
        data-zoom-image="/upload/produtos/<?php echo $codigo_produto; ?>/<?php echo $codigo_produto; ?>_832x1022_<?php echo $i; ?>.jpg"
        data-rsbigimg="/upload/produtos/<?php echo $codigo_produto; ?>/<?php echo $codigo_produto; ?>_832x1022_<?php echo $i; ?>.jpg"
        href="/upload/produtos/<?php echo $codigo_produto; ?>/<?php echo $codigo_produto; ?>_832x1022_<?php echo $i; ?>.jpg">
            Image 1
            <img class="rsTmb" src="/upload/produtos/<?php echo $codigo_produto; ?>/<?php echo $codigo_produto; ?>_70x86_<?php echo $i; ?>.jpg">
        </a>
        <?php
                $i++;
            }
        }
        ?>
        <!--
        <a class="rsImg" data-rsbigimg="/assets/img/prod4/02.jpg" href="/assets/img/prod4/02.jpg" data-zoom-ima="/assets/img/prod4/01.jpg"></ge="/assets/img/prod4/02.jpg"  data-rsw="500" data-rsh="625">Image 2<img width="60" height="75" class="rsTmb" src="/assets/img/prod4/02.jpg"></a>
        <a class="rsImg" data-rsbigimg="/assets/img/prod4/03.jpg" href="/assets/img/prod4/03.jpg" data-zoom-image="/assets/img/prod4/03.jpg"  data-rsw="500" data-rsh="625">Image 3<img width="60" height="75" class="rsTmb" src="/assets/img/prod4/03.jpg"></a>
        <a class="rsImg" data-rsbigimg="/assets/img/prod4/01.jpg" href="/assets/img/prod4/01.jpg" data-zoom-image="/assets/img/prod4/01.jpg"  data-rsw="500" data-rsh="625">Image 1<img width="60" height="75" class="rsTmb" src="/assets/img/prod4/01.jpg"></a>
        <a class="rsImg" data-rsbigimg="/assets/img/prod4/02.jpg" href="/assets/img/prod4/02.jpg" data-zoom-image="/assets/img/prod4/02.jpg"  data-rsw="500" data-rsh="625">Image 2<img width="60" height="75" class="rsTmb" src="/assets/img/prod4/02.jpg"></a>
        <a class="rsImg" data-rsbigimg="/assets/img/prod4/03.jpg" href="/assets/img/prod4/03.jpg" data-zoom-image="/assets/img/prod4/03.jpg"  data-rsw="500" data-rsh="625">Image 3<img width="60" height="75" class="rsTmb" src="/assets/img/prod4/03.jpg"></a>
        -->
      </div>
      <div class="text-right" style="padding-right:40px; float: right;"><i class="fa fa-search"></i> Passe o mouse na imagem para ver detalhes</div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-5 prod-description">
      <div class="h1"><?php echo $produto_detalhe[0]->nome; ?></div>
      <p>
        Peso: <?php echo $produto_detalhe[0]->peso; ?> gramas<br/>
        Altura x Largura x Profundidade: <?php echo $produto_detalhe[0]->medidas; ?>
      </p>      
      <p><?php echo $produto_detalhe[0]->descricao; ?></p>
      </br>
        <?php
        if($produto_detalhe[0]->cor_id){
        ?>
        <p>Escolha a cor do produto:</p>
        <ul class="colors">
            <li data-color="001"><img title="<?php echo $produto_detalhe[0]->cor_nome; ?>" src="/assets/img/color/<?php echo $produto_detalhe[0]->cor_slug; ?>.png"></li>
        </ul>
        <?php
        }
        ?>      
      <!--<p>Fabricado por: <?php //echo $produto_detalhe[0]->fabricante; ?></p>-->
      <hr>
      <input type="hidden" id="color" name="color" value="" />
      <input type="hidden" id="produto_id" name="produto_id" value="<?php echo $produto_detalhe[0]->id; ?>" />
      <!--<h3><strong>R$ <?php //echo $produto_detalhe[0]->valor_venda; ?></strong></h3>-->

      <?php if($produto_detalhe[0]->promocao){ ?>
      <h3><strong>De: <strike>R$ <?php echo $produto_detalhe[0]->valor_venda; ?></strike> Por: R$ <?php echo $produto_detalhe[0]->valor_promocao; ?></strong></h3>
      <?php } else { ?>
      <h3><strong>R$ <?php echo $produto_detalhe[0]->valor_venda; ?></strong></h3>
      <?php } ?>
      
      <?php
      if($produto_detalhe[0]->promocao){
        if($produto_detalhe[0]->valor_promocao >= 105.00){
      ?>
      <h4>3x de R$ <?php echo ($produto_detalhe[0]->valor_promocao / 3); ?> sem juros no cartão</h4></br>
      <?php
        }
      } else {
        if($produto_detalhe[0]->valor_venda >= 105.00){
      ?>
      <h4>3x de R$ <?php echo ($produto_detalhe[0]->valor_venda / 3); ?> sem juros no cartão</h4></br>
      <?php
        }
      }
      ?>
      
      <a href="javascript:void(0)" id="btn_comprar" rel="<?php echo $produto_detalhe[0]->id; ?>" class="buy">Comprar</a></br></br>
      <a href="javascript:void(0)" id="btn_add_wishlist" class="star">Adicionar à minha lista de desejos</a>
      <hr>
      <!--
      Compartilhe:
      <ul class="social">
        <li><a href="#"><img src="/assets/img/facebook-dark.png"></a></li>
        <li><a href="#"><img src="/assets/img/twitter-dark.png"></a></li>
        <li><a href="#"><img src="/assets/img/instagram-dark.png"></a></li>
        <li><a href="#"><img src="/assets/img/pinterest-dark.png"></a></li>
      </ul>
      -->
    </div>
  </div>
</div>

<!--TAMBEM VIEW-->
<div class="container prod-more">
  <div class="row">
    <div class="col-md-12">
      <hr>
      <h2>Quem viu este item, também viu</h2>
    </div>
  </div>
  <div class="row">
    
    <?php foreach($quem_viu as $produto){ ?>
    <div class="col-md-2">
      <a href="/produtos/detalhes/<?php echo $produto->id; ?>">
          <img src="/upload/produtos/<?php echo $produto->codigo; ?>/<?php echo $produto->codigo; ?>_140x180_1.jpg" alt="<?php echo $produto->nome; ?>" title="<?php echo $produto->nome; ?>" class="prod2 img-responsive">
          <h3><?php echo $produto->nome; ?></h3>
        </a>
        <a href="/produtos/categoria/<?php echo $produto->categoria_slug; ?>">
          <h4><?php echo $produto->categoria; ?></h4>
        </a>
        <h5>R$ <?php echo $produto->valor_venda; ?></h5>
    </div>
    <?php } ?>
  
  </div>  
  <div class="row">
    <div class="col-md-12">
      <hr>
      <h2>Detalhes do Produto</h2>
    </div>
    <div class="col-md-12 prod-detail">
      <p>
        Peso: <?php echo $produto_detalhe[0]->peso; ?> gramas<br/>
        Altura x Largura x Profundidade: <?php echo $produto_detalhe[0]->medidas; ?>
      </p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <hr>
      <h2>Vistos recentemente</h2>
    </div>
  </div>
  <div class="row">
    <?php foreach($vistos_recentes as $produto){ ?>
    <div class="col-md-2">
      <a href="/produtos/detalhes/<?php echo $produto->id; ?>">
          <img src="/upload/produtos/<?php echo $produto->codigo; ?>/<?php echo $produto->codigo; ?>_140x180_1.jpg" alt="<?php echo $produto->nome; ?>" title="<?php echo $produto->nome; ?>" class="prod2 img-responsive">
          <h3><?php echo $produto->nome; ?></h3>
        </a>
        <a href="/produtos/categoria/<?php echo $produto->categoria_slug; ?>">
          <h4><?php echo $produto->categoria; ?></h4>
        </a>
        <h5>R$ <?php echo $produto->valor_venda; ?></h5>
    </div>
    <?php } ?>
  </div>  
  <hr class="space"></hr>
</div>
<!--END NOVIDADES-->



        <script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="/assets/js/vendor/bootstrap.min.js"></script>
        <script src="/assets/js/main.js"></script>

        <!--JS PRODUCT_DETAIL-->
        
        <script src="/assets/royalslider/jquery-1.8.3.min.js" />
        <script>var $j = jQuery.noConflict(true);</script>
        <script src="/assets/royalslider/jquery.royalslider.min.js" />
        
        <script src="/assets/js/product_detail.js"></script>        
        <script src="/assets/js/jquery.elevateZoom-3.0.8.min.js"></script>
        <script src="/assets/js/engine_carrinho.js"></script>
        
<!--FOOTER-->
<?php include_once('includes/footer.php'); ?>
<!--END FOOTER-->        
        
<script>

  $(document).ready(function() {
  $('#product-detail').royalSlider({
    fullscreen: {
      enabled: false,
      nativeFS: true
    },
    controlNavigation: 'thumbnails',
    thumbs: {
      orientation: 'vertical',
      paddingBottom: 0,
      appendSpan: false,
    },
    transitionType:'fade',
    autoScaleSlider: true, 
    autoScaleSliderWidth: 1200,     
    autoScaleSliderHeight: 1170,
    imgWidth: 500,
    imgHeight: 625,
    loop: true,
    arrowsNav: false,
    keyboardNavEnabled: true,
    imageScaleMode: 'fit',

  });

$(document).on('mouseenter', 'div.product-x img.rsImg', function() {
        
        //var src = $(this).attr('src').split('?');
        var large = $(this).attr('src').split('?');
        $(this).attr('data-zoom-image', large)

        $(this).elevateZoom({
            zoomType: "inner",
            cursor: "crosshair",
            zoomWindowFadeIn: function(a) {
                $(this).css({
                    'background-color': '#fff'
                });
                return 400;
            },
            zoomWindowFadeOut: 600,
            arrowsNav:false,
            scrollZoom: true,
            easing: true,
        });
        $('.zoomContainer').remove();
});


});
</script>
        <!--END JS PRODUCT_DETAIL-->

    </body>
</html>
