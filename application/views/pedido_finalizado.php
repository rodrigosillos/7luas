<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>7 Luas - Seu Universo Espiritual</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="/assets/css/bootstrap-select.min.css">
        <link rel="stylesheet" href="/assets/css/font-awesome.css">
        <link rel="stylesheet" href="/assets/css/account.css">
        <!--CSS PRODUCT_DETAIL-->
        <link href="/assets/royalslider/royalslider.css" rel="stylesheet">
        <link href="/assets/royalslider/skins/universal/rs-universal.css" rel="stylesheet">
        <!--END CSS PRODUCT_DETAIL-->
        <script src="/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- NAV HEADER 7 LUAS-->
        <?php include_once('includes/header.php'); ?>
        <!--END NAV HEADER 7 LUAS-->
     
<div class="container order-done">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12 text-center">
          <h1>Pedido <?php echo $pedido[0]->pedido_id; ?> Realizado com Sucesso!</h1>
        </div>
        <div class="col-md-12">
          <div class="row h2-header text-center">
            <div class="col-xs-12 col-sm-12 col-md-12"><h2>Detalhes do Pedido</h2></div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table" border="0">
              <tr>
                <th class="title">Pedido</th>
                <th class="title">QTD</th>
                <th class="title">Preço</th>
              </tr>
              <?php foreach($pedido_itens as $item){ ?>
              <tr>
                <td>
                    <img src="/upload/produtos/<?php echo $item->produto_codigo; ?>/<?php echo $item->produto_codigo; ?>_70x86_1.jpg" alt="ALT AQUI" title="TITLE AQUI" class="prod2 img-responsive">
                    <h4><?php echo $item->produto_nome; ?></h4>
                    <p><?php echo substr(strip_tags($item->produto_descricao), 0, 150); ?></p>
                    <!--
                    <p>Tamanho: </p>
                    <p>Cor: </p>
                    -->
                </td>
                <td><?php echo $item->item_quantidade; ?></td>
                <td class="price">R$ <?php echo $item->item_valor_unitario; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td class="title">Frete</td>
                <td></td>
                <td class="price"></td>
              </tr>
              <tr>
                <td><?php echo $pedido[0]->envio_nome; ?> (Prazo de entrega estimado: <?php echo $pedido[0]->prazo; ?> <?php echo $pedido[0]->prazo == 1 ? " dia útil" : " dias úteis"; ?>)</td>
                <td></td>
                <td class="price">R$ <?php echo $pedido[0]->valor_frete; ?></td>
              </tr>
              <tr>
                <td class="title">Forma de Pagamento</td>
                <td></td>
                <td class="price"></td>
              </tr>
              <tr>
                <td><?php echo $pedido[0]->pagamento_nome; ?> <?php if($pedido[0]->parcelas > 1){ echo "em " . $pedido[0]->parcelas . " vezes"; } ?></td>
                <td class="title">Total</td>
                <td class="price">R$ <?php echo $pedido[0]->pedido_valor; ?></td>
              </tr>
              <tr>
                <td colspan="3"><hr></td>
              </tr>
              <tr>
                <td colspan="3">
                  <div class="row">
                    <div class="col-md-6 text-right" style="padding: 0 30px;">
                      <strong>Endereço de Cobrança</strong></br>
                      <?php echo $cliente[0]->nome; ?></br>
                      <?php echo $cliente[0]->endereco; ?>, <?php echo $cliente[0]->numero; ?> - <?php echo $cliente[0]->complemento; ?></br>
                      <?php echo $cliente[0]->bairro; ?> - <?php echo $cliente[0]->cidade; ?> - <?php echo $cliente[0]->estado; ?></br>
                      CEP <?php echo $cliente[0]->cep; ?></br>
                      Telefone <?php echo $cliente[0]->telefone; ?></br>
                    </div>
                    <div class="col-md-6 text-left" style="border-left:1px solid #999; padding: 0 30px;">
                      <strong>Endereço de Entrega</strong></br>
                      <?php if($pedido_endereco){ ?>
                      <?php echo $pedido_endereco[0]->nome; ?></br>
                      <?php echo $pedido_endereco[0]->endereco; ?>, <?php echo $pedido_endereco[0]->numero; ?> - <?php echo $pedido_endereco[0]->complemento; ?></br>
                      <?php echo $pedido_endereco[0]->bairro; ?> - <?php echo $pedido_endereco[0]->cidade; ?> - <?php echo $pedido_endereco[0]->estado; ?></br>
                      CEP <?php echo $pedido_endereco[0]->cep; ?></br>
                      Telefone <?php echo $pedido_endereco[0]->telefone; ?></br>
                      <?php } ?>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td colspan="3" class="text-center">
                    <a href="<?php echo base_url(); ?>" class="back">Voltar ao Site</a>
                    <?php if($link_boleto){ ?>
                    <a href="<?php echo $link_boleto; ?>" target="_blank" class="back">Imprimir Boleto</a>
                    <?php } ?>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div><!--row-->
    </div>
  </div>
</div>

  <hr class="space"></hr>
</div>

<!--END NOVIDADES-->

    <script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="/assets/js/vendor/bootstrap.min.js"></script>
    <script src="/assets/js/bootstrap-select.js"></script>
    <script src="/assets/js/defaults-pt_BR.js"></script>
    <script src="/assets/js/mousehold.js"></script>
    <script src="/assets/js/bootstrap-spinner.js"></script>
    <script src="/assets/js/main.js"></script>

    <!--JS PRODUCT_DETAIL-->
    <script src="/assets/js/product_detail.js"></script>
    <!--END JS PRODUCT_DETAIL-->
        
<!--FOOTER-->
<?php include_once('includes/footer.php'); ?>
<!--END FOOTER-->         

    </body>
</html>
