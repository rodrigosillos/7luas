<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>7 Luas - Seu Universo Espiritual</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="/assets/css/bootstrap-select.min.css">
        <link rel="stylesheet" href="/assets/css/font-awesome.css">
        <link rel="stylesheet" href="/assets/css/account.css">
        <!--CSS PRODUCT_DETAIL-->
        <link href="/assets/royalslider/royalslider.css" rel="stylesheet">
        <link href="/assets/royalslider/skins/universal/rs-universal.css" rel="stylesheet">
        <!--END CSS PRODUCT_DETAIL-->
        <script src="/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- NAV HEADER 7 LUAS-->
        <?php include_once('includes/header.php'); ?>
        <!--END NAV HEADER 7 LUAS-->

     
<div class="container account">
  <div class="row">
    <div class="col-xs-12 col-sm-2 col-md-2">
      <ul class="menu">
        <li><a href="/conta/">Minha Conta</a></li>
        <li class="active"><a href="/conta/meus_pedidos/">Meus Pedidos</a></li>
        <li><a href="/conta/meus_dados/">Meus Dados</a></li>
        <li><a href="/conta/lista_desejos/">Lista de Desejos</a></li>
        <li><a href="/conta/alterar_senha/">Alterar Senha</a></li>
      </ul>
    </div>
    <div class="col-xs-12 col-sm-10 col-md-10">
      <div class="row">
        <div class="col-md-12">
          <h1>Meus Pedidos</h1>
          <hr>
        </div>
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table table-order">
              <tr>
                <th>Pedido Nº</th>
                <th>Data</th>
                <th>Para</th>
                <th>Status</th>
                <th>Total do Pedido</th>
                <th></th>
              </tr>
              <?php foreach($pedidos as $pedido){ ?>
              <tr>
                <td><?php echo $pedido->pedido_id; ?></td>
                <td><?php echo date("d/m/Y H:i:s", strtotime($pedido->pedido_data)); ?></td>
                <td><?php echo $pedido->cliente_nome; ?></td>
                <td><?php echo $pedido->status_nome; ?></td>
                <td class="price">R$ <?php echo $pedido->pedido_valor; ?></td>
                <td><a href="/conta/pedido_detalhes/<?php echo $pedido->pedido_id; ?>"><img src="/assets/img/view.png"></a></td>
              </tr>
              <?php } ?>
            </table>
          </div>
        </div>
      </div><!--row-->
    </div>
  </div>
</div>

  <hr class="space"></hr>
</div>

<!--END NOVIDADES-->

<script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="/assets/js/vendor/bootstrap.min.js"></script>
<script src="/assets/js/bootstrap-select.js"></script>
<script src="/assets/js/defaults-pt_BR.js"></script>
<script src="/assets/js/mousehold.js"></script>
<script src="/assets/js/bootstrap-spinner.js"></script>
<script src="/assets/js/main.js"></script>

<!--JS PRODUCT_DETAIL-->
<script src="/assets/js/product_detail.js"></script>
<!--END JS PRODUCT_DETAIL-->

<!--FOOTER-->
<?php include_once('includes/footer.php'); ?>
<!--END FOOTER-->

    </body>
</html>
