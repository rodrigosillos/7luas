<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>7 Luas - Seu Universo Espiritual</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" h7 Luas - Seu Universo Espiritualref="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="/assets/css/bootstrap-select.min.css">
        <link rel="stylesheet" href="/assets/css/font-awesome.css">
        <link rel="stylesheet" href="/assets/css/login.css">
        <!--CSS PRODUCT_DETAIL-->
        <link href="/assets/royalslider/royalslider.css" rel="stylesheet">
        <link href="/assets/royalslider/skins/universal/rs-universal.css" rel="stylesheet">
        <!--END CSS PRODUCT_DETAIL-->
        <script src="/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- NAV HEADER 7 LUAS-->
        <?php include_once('includes/header.php'); ?>
        <!--END NAV HEADER 7 LUAS-->

     
<div class="container login">
 <div class="row">
   <div class="col-md-6">
      <form method="post" name="form_login">
        <h2>Já sou cadastrado</h2>
        <div class="form-group">
          <label for="exampleInputEmail1">E-mail</label>
          <input type="text" class="form-control" id="email" name="email" placeholder="E-mail">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Senha</label>
          <input type="password" class="form-control" id="senha" name="senha" placeholder="Senha">
          <!--<p class="help-block"><a href="#">Esqueceu sua senha?</a></p>-->
          <div class="checkbox">
          <!--  
          <label>
            <input type="checkbox"> Mantenha-me conectado
          </label>
          -->
        </div>
        </div>
        <input type="hidden" name="redirect" value="<?php echo isset($redirect) ? $redirect : false; ?>">
        <button type="submit" class="btn btn-default" id="btn_login">Comprar</button>
      </form>
   </div>
   <div class="col-md-6">
     <form>
        <h2>Quero me cadastrar</h2>
        <div class="form-group">
          <label for="exampleInputEmail1">Informe seu E-mail</label>
          <input type="text" class="form-control" id="email_cadastrar" name="email_cadastrar" placeholder="Email">
        </div>
        <div class="form-group">
          <ul>
            <li>Compras 100% seguras</li>
            <li>Agilize o processo de pagamento das suas encomendas futuras</li>
            <li>Verifique o status de seus pedidos e acompanhe-os online</li>
            <li>Receba ofertas especiais</li>
          </ul>
        </div>
        <button type="button" class="btn btn-default" id="btn_cadastrar">Cadastrar</button>
      </form>
   </div>
 </div>

  <hr class="space"></hr>
</div>

<!--END NOVIDADES-->

    <script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    
    <script src="/assets/js/vendor/bootstrap.min.js"></script>
    <script src="/assets/js/bootstrap-select.js"></script>
    <script src="/assets/js/defaults-pt_BR.js"></script>
    <script src="/assets/js/mousehold.js"></script>
    <script src="/assets/js/bootstrap-spinner.js"></script>
    <script src="/assets/js/main.js"></script>
    
    <!--JS-->
    <script src="/assets/js/engine_login.js"></script>
    <!--END JS-->
        
    <!--FOOTER-->
    <?php include_once('includes/footer.php'); ?>
    <!--END FOOTER-->        

    </body>
</html>
