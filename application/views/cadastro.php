<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>7 Luas - Seu Universo Espiritual</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="/assets/css/bootstrap-select.min.css">
        <link rel="stylesheet" href="/assets/css/font-awesome.css">
        <link rel="stylesheet" href="/assets/css/sign-up.css">
        <!--CSS PRODUCT_DETAIL-->
        <link href="/assets/royalslider/royalslider.css" rel="stylesheet">
        <link href="/assets/royalslider/skins/universal/rs-universal.css" rel="stylesheet">
        <!--END CSS PRODUCT_DETAIL-->
        <script src="/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- NAV HEADER 7 LUAS-->
        <?php include_once('includes/header.php'); ?>
        <!--END NAV HEADER 7 LUAS-->

     
<div class="container sign-up">
 <div class="row">
   <div class="col-md-6 hd">
     <h2>Cadastre-se</h2>
   </div>
   <div class="col-md-6 hd">
     <p>*campos obrigatórios</p>
   </div>
   <div class="col-md-12">
     <hr>
   </div>
 </div>
 <form name="form_cadastro" action="/cadastro/salvar/" method="post">
    <div class="row forms">
      <div class="col-md-6 f1">
         <div class="form-type">
           <label class="radio-inline">
             <input type="radio" name="tipo" id="fisica" value="fisica" checked="checked"> Pessoa Física
           </label>
           <label class="radio-inline">
             <input type="radio" name="tipo" id="juridica" value="juridica"> Pessoa Jurídica
           </label>
         </div>
         <div class="form-pf toHide" id="pessoa-fisica">
             <div class="form-group">
                 <label for="name"><sup>*</sup> Nome completo</label>
                 <input type="text" class="form-control obrigatorio" name="nome" id="nome" title="Nome" placeholder="">
             </div>
             <div class="row">
               <div class="col-md-4">
                 <div class="form-group">
                     <label for="name"><sup>*</sup> CPF</label>
                     <input type="text" class="form-control obrigatorio only_number" maxlength="11" name="cpf" id="cpf" title="CPF" placeholder="">
                 </div>
               </div>
               <div class="col-md-4">
                 <div class="form-group">
                     <label for="name"><sup>*</sup> Data de Nascimento</label>
                     <input type="text" class="form-control obrigatorio" name="data_nascimento" maxlength="10" id="data_nascimento" title="Data de Nascimento">
                 </div>
               </div>
               <div class="col-md-4">
                 <div class="form-group">
                     <label for="name"><sup>*</sup> Sexo</label>
                     <select class="form-control" name="sexo" id="sexo">
                       <option value="masculino">Masculino</option>
                       <option value="feminino">Feminino</option>
                     </select>
                 </div>
               </div>
             </div>
             <div class="row greybox">
               <div class="col-md-12">
                 <div class="form-group">
                     <label for="name"><sup>*</sup> E-mail</label>
                     <input type="text" class="form-control obrigatorio" id="email" name="email" title="E-mail" placeholder="">
                 </div>
               </div>
               <div class="col-md-6">
                 <div class="form-group">
                     <label for="name"><sup>*</sup> Senha</label>
                     <input type="password" class="form-control obrigatorio" id="senha" name="senha" title="Senha" placeholder="">
                 </div>
               </div>
               <div class="col-md-6">
                 <div class="form-group">
                     <label for="name"><sup>*</sup> Confirme sua Senha</label>
                     <input type="password" class="form-control obrigatorio" id="confirma_senha" name="confirma_senha" title="Confirme sua Senha" placeholder="">
                 </div>
               </div>
             </div>
         </div><!--end form pf-->
         <div class="form-pj toHide" id="pessoa-juridica">
           <div class="row">
             <div class="col-md-6">
                 <div class="form-group">
                     <label for="name"><sup>*</sup> Razão Social</label>
                     <input type="text" class="form-control obrigatorio" id="razao_social" name="razao_social" title="Razão Social" placeholder="">
                 </div>
               </div>
               <div class="col-md-6">
                 <div class="form-group">
                     <label for="name"><sup>*</sup> Nome Fantasia</label>
                     <input type="text" class="form-control obrigatorio" id="nome_fantasia" name="nome_fantasia" title="Nome Fantasia" placeholder="">
                 </div>
               </div>
             </div>
             <div class="row">
               <div class="col-md-4">
                 <div class="form-group">
                     <label for="name"><sup>*</sup> CNPJ</label>
                     <input type="text" class="form-control obrigatorio" maxlength="14" id="cnpj" name="cnpj" title="CNPJ" placeholder="">
                 </div>
               </div>
               <div class="col-md-4">
                 <div class="form-group">
                     <label for="name"><sup>*</sup> Inscrição Municipal</label>
                     <input type="text" class="form-control obrigatorio" id="im" name="im" title="Inscrição Municipal" placeholder="">
                 </div>
               </div>
               <div class="col-md-4">
                 <div class="form-group">
                     <label for="name"><sup>*</sup> Inscrição Estadual</label>
                     <input type="text" class="form-control obrigatorio" id="ie" name="ie" title="Inscrição Estadual" placeholder="">
                 </div>
               </div>
             </div>
             <div class="row greybox">
               <div class="col-md-12">
                 <div class="form-group">
                     <label for="name"><sup>*</sup> E-mail</label>
                     <input type="text" class="form-control obrigatorio" id="email_juridica" name="email_juridica" title="E-mail" placeholder="">
                 </div>
               </div>
               <div class="col-md-6">
                 <div class="form-group">
                     <label for="name"><sup>*</sup> Senha</label>
                     <input type="password" class="form-control obrigatorio" id="senha_juridica" name="senha_juridica" title="Senha" placeholder="">
                 </div>
               </div>
               <div class="col-md-6">
                 <div class="form-group">
                     <label for="name"><sup>*</sup> Confirme sua Senha</label>
                     <input type="password" class="form-control obrigatorio" id="confirma_senha_juridica" name="confirma_senha_juridica" title="Confirme sua Senha" placeholder="">
                 </div>
               </div>
             </div>
         </div><!--end form pj-->
     </div>
      <div class="col-md-6 f2">
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
                 <label for="name"><sup>*</sup> CEP</label>
                 <input type="text" class="form-control obrigatorio only_number" maxlength="8" id="cep" name="cep" title="CEP" placeholder="">
             </div>
          </div>
          <div class="col-md-7">
            <div class="form-group">
                 <label for="name"><sup>*</sup> Endereço</label>
                 <input type="text" class="form-control obrigatorio" id="endereco" name="endereco" title="Endereço" placeholder="">
             </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
                 <label for="name"><sup>*</sup> Número</label>
                 <input type="text" class="form-control obrigatorio" id="numero" name="numero" title="Número" placeholder="">
             </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
                 <label for="name">Complemento</label>
                 <input type="text" class="form-control" id="complemento" name="complemento" placeholder="">
             </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
                 <label for="name"><sup>*</sup> Tipo de Endereço</label>
                 <select class="form-control" id="tipo_endereco" name="tipo_endereco">
                       <option value="residencial">Residencial</option>
                       <option value="comercial">Comercial</option>
                     </select>
             </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
                 <label for="name"><sup>*</sup> Bairro</label>
                 <input type="text" class="form-control obrigatorio" id="bairro" name="bairro" title="Bairro" placeholder="">
             </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
                 <label for="name"><sup>*</sup> Cidade</label>
                 <input type="text" class="form-control obrigatorio" id="cidade" name="cidade" title="Cidade" placeholder="">
             </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
                 <label for="name"><sup>*</sup> Estado</label>
                 <select class="form-control obrigatorio" id="estado" name="estado" title="Estado">
                       <option value="AC">AC</option>
                       <option value="AL">AL</option>
                       <option value="AP">AP</option>
                       <option value="AM">AM</option>
                       <option value="BA">BA</option>
                       <option value="CE">CE</option>
                       <option value="DF">DF</option>
                       <option value="ES">ES</option>
                       <option value="GO">GO</option>
                       <option value="MA">MA</option>
                       <option value="MT">MT</option>
                       <option value="MS">MS</option>
                       <option value="MG">MG</option>
                       <option value="PA">PA</option>
                       <option value="PB">PB</option>
                       <option value="PR">PR</option>
                       <option value="PE">PE</option>
                       <option value="PI">PI</option>
                       <option value="RJ">RJ</option>
                       <option value="RN">RN</option>
                       <option value="RS">RS</option>
                       <option value="RO">RO</option>
                       <option value="RR">RR</option>
                       <option value="SC">SC</option>
                       <option value="SP">SP</option>
                       <option value="SE">SE</option>
                       <option value="TO">TO</option>
                    </select>
             </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
                 <label for="name"><sup>*</sup> Telefone</label>
                 <input type="text" class="form-control obrigatorio" id="telefone" name="telefone" title="Telefone" placeholder="">
             </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
                <label for="name">Religião</label>
                <select class="form-control obrigatorio" id="religiao_id" name="religiao_id" title="Estado">
                    <option value="8">Todas</option>
                    <option value="7" selected>Não tenho religião</option>
                    <option value="1">Budismo</option>
                    <option value="2">Candomblé</option>
                    <option value="6">Esoterismo</option>
                    <option value="5">Kardecismo</option>
                    <option value="3">Umbanda</option>
                </select>
             </div>
          </div>          
        </div>
        <div class="row">
          <div class="col-md-12">
             <div class="checkbox">
               <label>
               <input type="checkbox" name="newsletter" id="newsletter" value="1">
                 Quero receber newsletter com notícias, eventos e informações sobre os produtos.
               </label>
               </div>
               <div class="checkbox">
               <label>
                 <input type="checkbox" class="obrigatorio" name="termos_uso" id="termos_uso" title="Você precisa concordar com os termos." value="1">
                 Concordo com os <a href="#">Termos de Uso do site</a>.
               </label>
             </div>
          </div>
          <div class="col-md-12 text-right">
            <input type="hidden" id="erro_duplicidade">
            <button type="submit" class="btn btn-default" id="btn_salvar">Salvar</button>
          </div>
        </div>
      </div>
    </div>
 </form>
  <hr class="space"></hr>
</div>

<!--END NOVIDADES-->

<script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="/assets/js/vendor/bootstrap.min.js"></script>
<script src="/assets/js/bootstrap-select.js"></script>
<script src="/assets/js/defaults-pt_BR.js"></script>
<script src="/assets/js/mousehold.js"></script>
<script src="/assets/js/bootstrap-spinner.js"></script>
<script src="/assets/js/main.js"></script>
<script src="/assets/js/sign-up.js"></script>
<script src="/assets/js/jquery.maskedinput.min.js"></script>

<!--JS-->
<script src="/assets/js/engine_cadastro.js"></script>
<!--END JS-->

<script type="text/javascript">
$("#cpf").mask("99999999999");
$("#cnpj").mask("99999999999999");
$("#data_nascimento").mask("99/99/9999");
$("#cep").mask("99999999");
$("#telefone").mask("(99) 9999-9999?9");
</script>
        
<!--FOOTER-->
<?php include_once('includes/footer.php'); ?>
<!--END FOOTER-->        

    </body>
</html>