<!DOCTYPE html>
<html>
<head>
	<title>7 Luas</title>
	<meta charset="utf-8">
</head>
<body bgcolor="#e2e2e2">
<table border="0" cellpadding="0" cellspacing="0" width="750" align="center" bgcolor="#FFFFFF" style="background: url('http://7luas.com/assets/email/img/stars.png') #FFFFFF;">
	<tr cellpadding="0">
		<td width="100%" ><img src="http://7luas.com/assets/email/img/header.jpg" width="750" height="144"></td>
	</tr>
	<tr>
		<td colspan="2" style="padding:40px 0; text-align: center;" align="center">
		<table cellpadding="0" cellspacing="0" align="center" width="650" style="background: rgba(255,255,255,0.8);">
			<tr>
				<td>
					<table>
						<tr>
						<td align="left"><img src="http://7luas.com/assets/email/img/done.png" width="70" height="70"></td>
						<td align="left"><font face="arial,verdana,sans-serif" size="5" color="#0b375c"><strong>RECEBEMOS SEU PEDIDO <?php echo $pedido_id; ?></strong></font></td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="left">
					<font face="arial,verdana,sans-serif" size="4">
					Olá <?php echo $cliente_nome; ?>, bem-vindo a 7 Luas.</br></br>
					Recebemos seu pedido número <?php echo $pedido_id; ?> e aguardamos a confirmação do pagamento.</br>
					Você receberá novos comunicados, por e-mail, sobre o andamento do pedido.</br>
					<img src="http://7luas.com/assets/email/img/processo_1.jpg" style="padding: 20px 0;"></br></br>
					<strong>PRAZO DE ENTREGA</strong></br></br>
					Começa a contar após a confirmação de pagamento pelo banco. Essa confirmação ocorre em até dois dias corridos e você será informado por e-mail quando isso acontecer.</br></br>
					Lembramos que, a partir deste momento, não será possível:</br>
					• Alterar a forma de pagamento</br>
					• Alterar o endereço de entrega.</br>
					Para cancelar o pedido acesse "Minha Conta".</br>
					</font>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%;" bgcolor="#020913" cellpadding="2" border="0">
				<tr>
					<td>
						<font face="arial,verdana,sans-serif" size="2" color="#FFFFFF">ESTE É UM E-MAIL AUTOMÁTICO, NÃO É NECESSÁRIO RESPONDE-LO.</font></br>
						<font face="arial,verdana,sans-serif" size="2" color="#FFFFFF"><strong>FIQUE POR DENTRO DAS NOVIDADES E PROMOÇÕES.</strong></font></br>
					</td>
					<td><font face="arial,verdana,sans-serif" size="2" color="#FFFFFF">SIGA A 7 LUAS:</font></td>
					<td><a href="#"><img src="http://7luas.com/assets/email/img/facebook.png" width="30" height="30" border="0"></a></td>
					<td><a href="#"><img src="http://7luas.com/assets/email/img/twitter.png" width="30" height="30" border="0"></a></td>
					<td><a href="#"><img src="http://7luas.com/assets/email/img/instagram.png" width="30" height="30" border="0"></a></td>
					<td><a href="#"><img src="http://7luas.com/assets/email/img/pinterest.png" width="30" height="30" border="0"></a></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>