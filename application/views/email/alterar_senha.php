<!DOCTYPE html>
<html>
<head>
	<title>7 Luas</title>
	<meta charset="utf-8">
</head>
<body bgcolor="#e2e2e2">
<table  border="0" cellpadding="0" cellspacing="0" width="750" align="center" bgcolor="#FFFFFF" style="background: url('http://7luas.com/assets/email/img/stars.png') #FFFFFF;">
	<tr cellpadding="0">
		<td width="100%" ><img src="http://7luas.com/assets/email/img/header.jpg" width="750" height="144"></td>
	</tr>
	<tr>
		<td colspan="2" style="padding:40px 0; text-align: center;" align="center">
		<table cellpadding="0" cellspacing="0" align="center" width="650" style="background: rgba(255,255,255,0.8);">
			<tr>
				<td align="left">
					<font face="arial,verdana,sans-serif" size="4">
					Olá <?php echo $nome_cliente; ?>, bem-vindo a 7 Luas.</br></br>
					Conforme sua solicitação, sua senha no 7 Luas foi alterada com sucesso.</br>
					Se preferir, acesse o 7 Luas e solicite uma nova senha por meio do link</br>
					"<a href="#" style="color: #000; text-decoration: none;">Esqueci Minha Senha</a>" em "<a href="#" style="color: #000; text-decoration: none;">Minha Conta</a>".</br></br>
					Qualquer dúvida, entre em contato com nossa central de atendimento.</br></br>
					Obrigado por nos visitar, e boas compras.</br>
					Equipe de atendimento ao cliente 7 Luas.</br>
					<a href="http://7luas.com" style="color: #000; text-decoration: none;">www.7luas.com</a>
					</font>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%;" bgcolor="#020913" cellpadding="2" border="0">
				<tr>
					<td>
						<font face="arial,verdana,sans-serif" size="2" color="#FFFFFF">ESTE É UM E-MAIL AUTOMÁTICO, NÃO É NECESSÁRIO RESPONDE-LO.</font></br>
						<font face="arial,verdana,sans-serif" size="2" color="#FFFFFF"><strong>FIQUE POR DENTRO DAS NOVIDADES E PROMOÇÕES.</strong></font></br>
					</td>
					<td><font face="arial,verdana,sans-serif" size="2" color="#FFFFFF">SIGA A 7 LUAS:</font></td>
					<td><a href="#"><img src="http://7luas.com/assets/email/img/facebook.png" width="30" height="30" border="0"></a></td>
					<td><a href="#"><img src="http://7luas.com/assets/email/img/twitter.png" width="30" height="30" border="0"></a></td>
					<td><a href="#"><img src="http://7luas.com/assets/email/img/instagram.png" width="30" height="30" border="0"></a></td>
					<td><a href="#"><img src="http://7luas.com/assets/email/img/pinterest.png" width="30" height="30" border="0"></a></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>