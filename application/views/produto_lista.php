<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>7 Luas - Seu Universo Espiritual</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="/assets/css/bootstrap-select.css">
        <link rel="stylesheet" href="/assets/css/font-awesome.css">
        <link rel="stylesheet" href="/assets/css/product_list.css">
        <!--CSS PRODUCT_DETAIL-->
        <link href="/assets/royalslider/royalslider.css" rel="stylesheet">
        <link href="/assets/royalslider/skins/universal/rs-universal.css" rel="stylesheet">
        <!--END CSS PRODUCT_DETAIL-->
        <script src="/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- NAV HEADER 7 LUAS-->
        <?php include_once('includes/header.php'); ?>
        <!--END NAV HEADER 7 LUAS-->

     
<!--TAMBEM VIEW-->

<div class="container prod-list">
<div class="col-md-3">
  <div class="row">
    <div class="col-md-12">
      <h3><strong>Filtre os Resultados:</strong></h3>
      <h2>Categorias</h2>
      <ul class="sub">
        <?php
        $segment2 = $this->uri->segment(2);
		$segment3 = $this->uri->segment(3);
        $segment4 = $this->uri->segment(4);
        $segment5 = $this->uri->segment(5);
        
        $segment_religiao = isset($segment3) ? $segment3."/" : "false/";
        $segment_categoria = isset($segment4) ? $segment4."/" : "false/";
        $segment_subcategoria = isset($segment5) ? $segment5."/" : "false/";
        
		$religiao_id = "";
		$categoria_id = "";
		$subcategoria_id = "";        
		
		if(isset($segment3))
		{
			$religiao = $this->produtos_model->get(array('slug' => $segment3), "religioes");
            if($religiao){
                $religiao_id = $religiao[0]->id;
            }
		}

		if(isset($segment4))
		{
			$categoria = $this->produtos_model->get(array('slug' => $segment4), "categorias");
			if($categoria){
                $categoria_id = $categoria[0]->id;
            }
		}
		
		if($segment5)
		{
			$subcategoria = $this->produtos_model->get(array('slug' => $segment5), "subcategorias");
			if($subcategoria){
                $subcategoria_id = $subcategoria[0]->id;
            }
		}
        
        foreach($categorias as $categoria){
            
            #fixme
            $arr_subcategoria = array();
            
            $data = array("categoria_id" => $categoria->id);
			
			if($religiao_id)
            {
                $data = array("religiao_id" => $religiao_id, "categoria_id" => $categoria->id);				
            }
			
			$produtos_subcat = $this->produtos_model->get($data, "produtos");
            
            foreach($produtos_subcat as $produto)
            {
                array_push($arr_subcategoria, $produto->subcategoria_id);
            }
            
			$subcategorias = array();
			
            if($arr_subcategoria)
            {
                $subcategorias = $this->produtos_model->get(array_unique($arr_subcategoria), "subcategorias", true);
            }       

        ?>
        <li class="cat"><a href="/produtos/categoria/<?php echo $segment_religiao . $categoria->slug; ?>"><?php echo $categoria->nome; ?></a></li>            
            <?php foreach($subcategorias as $subcategoria_id){ ?>
                <!--<li class="sub"><a href="/produtos/subcategoria/<?php echo $categoria->slug; ?>/<?php echo $subcategoria_id->slug; ?>"><?php echo $subcategoria_id->nome; ?></a></li>-->
                <li class="sub"><a href="/produtos/subcategoria/<?php echo $segment_religiao . $categoria->slug; ?>/<?php echo $subcategoria_id->slug; ?>"><?php echo $subcategoria_id->nome; ?></a></li>
            <?php } ?>
        <?php
        }
        ?>
      </ul>
    </div>
    <div class="col-md-12">
      <h2>Preço</h2>
      <ul class="sub">
        <li><a href="/produtos/preco/<?php echo $segment_religiao.$segment_categoria.$segment_subcategoria; ?>20/">Até R$ 20</a></li>
        <li><a href="/produtos/preco/<?php echo $segment_religiao.$segment_categoria.$segment_subcategoria; ?>20~50/">R$ 20 - 50</a></li>
        <li><a href="/produtos/preco/<?php echo $segment_religiao.$segment_categoria.$segment_subcategoria; ?>50~100/">R$ 50 - 100</a></li>
        <li><a href="/produtos/preco/<?php echo $segment_religiao.$segment_categoria.$segment_subcategoria; ?>100~150/">R$ 100 - R$ 150</a></li>
        <li><a href="/produtos/preco/<?php echo $segment_religiao.$segment_categoria.$segment_subcategoria; ?>150~200/">R$ 150 - R$ 200</a></li>
        <li><a href="/produtos/preco/<?php echo $segment_religiao.$segment_categoria.$segment_subcategoria; ?>200~250/">R$ 200 - R$ 250</a></li>
      </ul>
    </div>
  </div>
</div>
<div class="col-md-9">
  <div class="row">
    <div class="col-md-12 breadcrumb">
        <!--Você está em: <a href="#">Home</a> > <a href="#">Categoria 1</a> > <a href="#">Sub-categoria 3</a> > Nome do Produto-->
    </div>
    <div class="col-md-8 nav-filter">
        <?php echo $itens_encontrados; ?> item(s) encontrado(s) | Ordenar por:
        <select class="selectpicker" id="sort">
          <option value="a_z/<?php echo $segment_religiao.$segment_categoria.$segment_subcategoria; ?>" <?php if($segment2=="a_z"){ echo "selected"; }?>>A-Z</option>
		  <option value="mais_populares/<?php echo $segment_religiao.$segment_categoria.$segment_subcategoria; ?>" <?php if($segment2=="mais_populares"){ echo "selected"; }?>>Mais Populares</option>
          <option value="mais_vendidos/<?php echo $segment_religiao.$segment_categoria.$segment_subcategoria; ?>" <?php if($segment2=="mais_vendidos"){ echo "selected"; }?>>Mais Vendidos</option>
          <option value="menor_preco/<?php echo $segment_religiao.$segment_categoria.$segment_subcategoria; ?>" <?php if($segment2=="menor_preco"){ echo "selected"; }?>>Menor Preço</option>
        </select>
    </div>
    <div class="col-md-12 nav-filter text-right">
      <!--<div>Mostrando 1 - 24 de 42.</div>-->
        <nav>
        <?php echo $this->pagination->create_links(); ?>
        </nav>
    </div>    
    <?php foreach($produtos as $produto){ ?>
    <div class="col-md-4">
      <a href="/produtos/detalhes/<?php echo $produto->id; ?>/<?php echo $produto->slug; ?>">
          <img src="/upload/produtos/<?php echo $produto->codigo; ?>/<?php echo $produto->codigo; ?>_220x270_1.jpg" alt="<?php echo $produto->nome; ?>" title="<?php echo $produto->nome; ?>" class="prod2 img-responsive">
          <h3><?php echo $produto->nome; ?></h3>
        </a>
        <a href="/produtos/categoria/<?php echo $segment_religiao.$produto->categoria_slug."/".$segment_subcategoria; ?>">
          <h4><?php echo $produto->categoria; ?></h4>
        </a>
        <?php if($produto->promocao){ ?>
        <h5>De: <strike>R$ <?php echo $produto->valor_venda; ?></strike> Por: R$ <?php echo $produto->valor_promocao; ?></h5>
        <?php } else { ?>
        <h5>R$ <?php echo $produto->valor_venda; ?></h5>
        <?php } ?>
    </div>
    <?php } ?>
  </div>
  <div class="row">
    <div class="col-md-12 text-right">
      <nav>
        <?php echo $this->pagination->create_links(); ?>
        </nav>
    </div>
  </div>
</div>
  <hr class="space"></hr>
</div>

<!--END NOVIDADES-->

<script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="/assets/js/vendor/bootstrap.min.js"></script>
<script src="/assets/js/bootstrap-select.js"></script>
<script src="/assets/js/defaults-pt_BR.js"></script>
<script src="/assets/js/main.js"></script>

<!--JS PRODUCT_DETAIL-->
<script src="/assets/js/product_detail.js"></script>
<!--END JS PRODUCT_DETAIL-->

<!--FOOTER-->
<?php include_once('includes/footer.php'); ?>
<!--END FOOTER-->        

    </body>
</html>
