<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>7 Luas - Seu Universo Espiritual</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="/assets/css/bootstrap-select.css">
        <link rel="stylesheet" href="/assets/css/font-awesome.css">
        <link rel="stylesheet" href="/assets/css/account.css">
        <!--CSS PRODUCT_DETAIL-->
        <link href="/assets/royalslider/royalslider.css" rel="stylesheet">
        <link href="/assets/royalslider/skins/universal/rs-universal.css" rel="stylesheet">
        <!--END CSS PRODUCT_DETAIL-->
        <script src="/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- NAV HEADER 7 LUAS-->
        <?php include_once('includes/header.php'); ?>
        <!--END NAV HEADER 7 LUAS-->
<div class="container account">
  <div class="row">
    <div class="col-xs-12 col-sm-2 col-md-2">
      <ul class="menu">
        <li><a href="/conta/">Minha Conta</a></li>
        <li><a href="/conta/meus_pedidos/">Meus Pedidos</a></li>
        <li><a href="/conta/meus_dados/">Meus Dados</a></li>
        <li><a href="/conta/lista_desejos/">Lista de Desejos</a></li>
        <li class="active"><a href="/conta/alterar_senha/">Alterar Senha</a></li>
      </ul>
    </div>
    <div class="col-xs-12 col-sm-10 col-md-10">
      <div class="row">
        <div class="col-md-12">
          <h1>Alterar Senha</h1>
          <hr>
        </div>
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-4">
              <form>
                <div class="form-group">
                  <label for="exampleInputPassword1">Senha Atual</label>
                  <input type="password" class="form-control" id="senha_atual" placeholder="">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword2">Nova Senha</label>
                  <input type="password" class="form-control" id="nova_senha" placeholder="">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword3">Repita a Nova Senha</label>
                  <input type="password" class="form-control" id="confirma_nova_senha" placeholder="">
                </div>
                <div class="form-group text-right">
                  <button type="button" class="btn btn-default btn-salvar" id="btn_alterar_senha">Confirmar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--row-->
    </div>
  </div>
</div>
  <hr class="space"></hr>
</div>
<!--END NOVIDADES-->

<script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
<script src="/assets/js/vendor/bootstrap.min.js"></script>
<script src="/assets/js/bootstrap-select.js"></script>
<script src="/assets/js/defaults-pt_BR.js"></script>
<script src="/assets/js/mousehold.js"></script>
<script src="/assets/js/engine_cadastro.js"></script>

<script type="text/javascript">
<?php if($this->session->flashdata('mensagem')){ ?>
alert("<?php echo $this->session->flashdata('mensagem'); ?>");
<?php } ?>     
</script>
        
<!--FOOTER-->
<?php include_once('includes/footer.php'); ?>
<!--END FOOTER-->        

    </body>
</html>
