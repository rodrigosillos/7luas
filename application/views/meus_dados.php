<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>7 Luas - Seu Universo Espiritual</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="/assets/css/bootstrap-select.css">
        <link rel="stylesheet" href="/assets/css/font-awesome.css">
        <link rel="stylesheet" href="/assets/css/bootstrap-select.min.css">
        <link rel="stylesheet" href="/assets/css/account.css">
        <!--CSS PRODUCT_DETAIL-->
        <link href="/assets/royalslider/royalslider.css" rel="stylesheet">
        <link href="/assets/royalslider/skins/universal/rs-universal.css" rel="stylesheet">
        <!--END CSS PRODUCT_DETAIL-->
        <script src="/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- NAV HEADER 7 LUAS-->
        <?php include_once('includes/header.php'); ?>
        <!--END NAV HEADER 7 LUAS-->
<div class="container account">
  <div class="row">
    <div class="col-xs-12 col-sm-2 col-md-2">
      <ul class="menu">
        <li><a href="/conta/">Minha Conta</a></li>
        <li><a href="/conta/meus_pedidos/">Meus Pedidos</a></li>
        <li class="active"><a href="/conta/meus_dados/">Meus Dados</a></li>
        <li><a href="/conta/lista_desejos/">Lista de Desejos</a></li>
        <li><a href="/conta/alterar_senha/">Alterar Senha</a></li>
      </ul>
    </div>
    <div class="col-xs-12 col-sm-10 col-md-10">
      <div class="row">
        <div class="col-md-12">
          <h1>Meus Dados</h1>
          <hr>
        </div>
        <div class="col-md-12">
          <div>
            <form name="form_cadastro" action="/cadastro/salvar/" method="post">
              <input type="hidden" name="tipo" id="tipo" value="fisica">
              <input type="hidden" name="cliente_id" id="cliente_id" value="<?php echo $cliente[0]->id; ?>">
              <div class="row">
                <div class="form-group col-md-12">
                  <label for="email">*E-mail</label>
                  <input type="email" class="form-control obrigatorio" id="email" name="email" title="E-mail" value="<?php echo $cliente[0]->email; ?>" placeholder="enderecoinformado@mail.com.br" style="max-width: 340px;">
                </div>
              </div>
              <div class="row">
              <div class="form-group col-md-12">
                <label for="cpf">*CPF</label>
                <input type="text" class="form-control obrigatorio" id="cpf" name="cpf" title="CPF" value="<?php echo $cliente[0]->cpf; ?>" style="max-width: 150px;">
              </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <hr>
                </div>
              </div>
              <div class="row">
              <div class="form-group col-md-12">
                <label for="name">*Nome Completo:</label>
                <input type="text" class="form-control obrigatorio" id="nome" name="nome" title="Nome" value="<?php echo $cliente[0]->nome; ?>" style="max-width: 500px;">
              </div>
              </div>
              <div class="row">
              <div class="form-group col-md-2">
                <label for="date">*Data de Nascimento:</label>
                <input type="text" class="form-control obrigatorio" id="data_nascimento" name="data_nascimento" title="Data de Nascimento" value="<?php echo date("d/m/Y", strtotime($cliente[0]->data_nascimento)); ?>" placeholder="DD/MM/YYYY">
              </div>
              <div class="form-group col-md-1">
                <label for="sexo">*Sexo:</label>
                <select class="selectpicker" id="sexo" name="sexo">
                  <option value="masculino"<?php if($cliente[0]->sexo == "masculino"){ echo "selected"; } ?>>Masculino</option>
                  <option value="feminino"<?php if($cliente[0]->sexo == "feminino"){ echo "selected"; } ?>>Feminino</option>
                </select>
              </div>              
              </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <hr>
                </div>
              </div>
              <div class="row">
              <div class="form-group col-md-2">
                <label for="cep">*CEP:</label>
                <input type="text" class="form-control obrigatorio" id="cep" name="cep" title="CEP" value="<?php echo $cliente[0]->cep; ?>">
              </div>
              <div class="form-group col-md-4">
                <label for="endereco">*Endereço:</label>
                <input type="text" class="form-control obrigatorio" id="endereco" name="endereco" title="Endereço" value="<?php echo $cliente[0]->endereco; ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="numero">*Número:</label>
                <input type="text" class="form-control obrigatorio" id="numero" title="Número" value="<?php echo $cliente[0]->numero; ?>">
              </div>
              </div>
              <div class="row">
              <div class="form-group col-md-2">
                <label for="cep">Complemento:</label>
                <input type="text" class="form-control" id="complemento" name="complemento" title="Complemento" value="<?php echo $cliente[0]->complemento; ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="sexo">*Tipo de Endereço:</label>
                <select class="selectpicker" data-width="100%" id="tipo_endereco" name="tipo_endereco">
                  <option value="residencial" <?php if($cliente[0]->tipo_endereco == "residencial"){ echo "selected"; } ?>>Residencial</option>
                  <option value="comercial" <?php if($cliente[0]->tipo_endereco == "comercial"){ echo "selected"; } ?>>Comercial</option>
                </select>
              </div>
              <div class="form-group col-md-4">
                <label for="numero">*Bairro:</label>
                <input type="text" class="form-control obrigatorio" id="bairro" name="bairro" title="Bairro" value="<?php echo $cliente[0]->bairro; ?>" placeholder="Jardim do Bairro">
              </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                   <div class="form-group">
                        <label for="name"><sup>*</sup> Cidade</label>
                        <input type="text" class="form-control obrigatorio" id="cidade" name="cidade" title="Cidade" value="<?php echo $cliente[0]->cidade; ?>" placeholder="">
                    </div>
                 </div>
                 <div class="col-md-2">
                   <div class="form-group">
                    <label for="name"><sup>*</sup> Estado</label>
                    <select class="selectpicker" data-width="100%" id="estado" name="estado">
                       <option value="AC" <?php if($cliente[0]->estado == "AC"){ echo "selected"; } ?>>AC</option>
                       <option value="AL" <?php if($cliente[0]->estado == "AL"){ echo "selected"; } ?>>AL</option>
                       <option value="AP" <?php if($cliente[0]->estado == "AP"){ echo "selected"; } ?>>AP</option>
                       <option value="AM" <?php if($cliente[0]->estado == "AM"){ echo "selected"; } ?>>AM</option>
                       <option value="BA" <?php if($cliente[0]->estado == "BA"){ echo "selected"; } ?>>BA</option>
                       <option value="CE" <?php if($cliente[0]->estado == "CE"){ echo "selected"; } ?>>CE</option>
                       <option value="DF" <?php if($cliente[0]->estado == "DF"){ echo "selected"; } ?>>DF</option>
                       <option value="ES" <?php if($cliente[0]->estado == "ES"){ echo "selected"; } ?>>ES</option>
                       <option value="GO" <?php if($cliente[0]->estado == "GO"){ echo "selected"; } ?>>GO</option>
                       <option value="MA" <?php if($cliente[0]->estado == "MA"){ echo "selected"; } ?>>MA</option>
                       <option value="MT" <?php if($cliente[0]->estado == "MT"){ echo "selected"; } ?>>MT</option>
                       <option value="MS" <?php if($cliente[0]->estado == "MS"){ echo "selected"; } ?>>MS</option>
                       <option value="MG" <?php if($cliente[0]->estado == "MG"){ echo "selected"; } ?>>MG</option>
                       <option value="PA" <?php if($cliente[0]->estado == "PA"){ echo "selected"; } ?>>PA</option>
                       <option value="PB" <?php if($cliente[0]->estado == "PB"){ echo "selected"; } ?>>PB</option>
                       <option value="PR" <?php if($cliente[0]->estado == "PR"){ echo "selected"; } ?>>PR</option>
                       <option value="PE" <?php if($cliente[0]->estado == "PE"){ echo "selected"; } ?>>PE</option>
                       <option value="PI" <?php if($cliente[0]->estado == "PI"){ echo "selected"; } ?>>PI</option>
                       <option value="RJ" <?php if($cliente[0]->estado == "RJ"){ echo "selected"; } ?>>RJ</option>
                       <option value="RN" <?php if($cliente[0]->estado == "RN"){ echo "selected"; } ?>>RN</option>
                       <option value="RS" <?php if($cliente[0]->estado == "RS"){ echo "selected"; } ?>>RS</option>
                       <option value="RO" <?php if($cliente[0]->estado == "RO"){ echo "selected"; } ?>>RO</option>
                       <option value="RR" <?php if($cliente[0]->estado == "RR"){ echo "selected"; } ?>>RR</option>
                       <option value="SC" <?php if($cliente[0]->estado == "SC"){ echo "selected"; } ?>>SC</option>
                       <option value="SP" <?php if($cliente[0]->estado == "SP"){ echo "selected"; } ?>>SP</option>
                       <option value="SE" <?php if($cliente[0]->estado == "SE"){ echo "selected"; } ?>>SE</option>
                       <option value="TO" <?php if($cliente[0]->estado == "TO"){ echo "selected"; } ?>>TO</option>
                    </select>
                    </div>
                 </div>
                 <div class="col-md-2">
                   <div class="form-group">
                        <label for="phone"><sup>*</sup> Telefone</label>
                        <input type="text" class="form-control obrigatorio" id="telefone" name="telefone" title="Telefone" value="<?php echo $cliente[0]->telefone; ?>" placeholder="(        ) XXXX-XXXX">
                    </div>
                 </div>
              </div>
              <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-default btn-salvar">Salvar</button>
              </div>
            </form>
          </div>
        </div>
      </div><!--row-->
    </div>
  </div>
</div>
  <hr class="space">
</div>
<!--END NOVIDADES-->

<script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
<script src="/assets/js/vendor/bootstrap.min.js"></script>
<script src="/assets/js/bootstrap-select.js"></script>
<script src="/assets/js/defaults-pt_BR.js"></script>
<script src="/assets/js/mousehold.js"></script>
<script src="/assets/js/bootstrap-spinner.js"></script>
<script src="/assets/js/bootstrap-spinner.js"></script>
<script src="/assets/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript">
$("#cpf").mask("99999999999");
$("#cnpj").mask("99999999999999");
$("#data_nascimento").mask("99/99/9999");
$("#cep").mask("99999999");
$("#telefone").mask("(99) 9999-9999?9");

<?php if($this->session->flashdata('msg_cadastro')){ ?>
alert("<?php echo $this->session->flashdata('msg_cadastro'); ?>");
<?php } ?>

</script>

<!--JS-->
<script src="/assets/js/engine_cadastro.js"></script>
<!--END JS-->
        
<!--FOOTER-->
<?php include_once('includes/footer.php'); ?>
<!--END FOOTER-->        

    </body>
</html>
