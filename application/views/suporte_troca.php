<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>7 Luas - Seu Universo Espiritual</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="/assets/css/bootstrap-select.css">
        <link rel="stylesheet" href="/assets/css/font-awesome.css">
        <link rel="stylesheet" href="/assets/css/support.css">
        <!--CSS PRODUCT_DETAIL-->
        <link href="/assets/royalslider/royalslider.css" rel="stylesheet">
        <link href="/assets/royalslider/skins/universal/rs-universal.css" rel="stylesheet">
        <!--END CSS PRODUCT_DETAIL-->
        <script src="/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- NAV HEADER 7 LUAS-->
        <?php include_once('includes/header.php'); ?>
        <!--END NAV HEADER 7 LUAS-->
<div class="container account">
  <div class="row">
    <div class="col-xs-12 col-sm-2 col-md-2">
        <?php include_once('includes/menu_suporte.php'); ?>
    </div>
    <div class="col-xs-12 col-sm-10 col-md-10">
      <div class="row">
        <div class="col-md-12">
          <h1>Trocas e Devoluções</h1>
          <hr>
        </div>
        <div class="col-md-12">
            <div>
                <p>A 7 Luas oferece produtos de qualidade, afim de manter a credibilidade conquistada e alcançar sempre a satisfação de seus clientes, adotando uma Política Justa de Troca e Devolução.</p>
                <p><strong>CONDIÇÕES GERAIS</strong></p>
                <p>
                Todas as ocorrências que envolvam troca ou devolução devem ser comunicadas por intermédio do e-mail: sac@7luas.com
                Caso o Produto seja devolvido sem essa comunicação, fora do prazo de 7 dias corridos ou com ausência de itens/acessórios que o acompanham, será reencaminhado ao cliente pela 7 Luas.</p>
                <p>
                Se ocorrer qualquer das hipóteses abaixo, recuse o recebimento do produto:<br/> 
                • embalagem violada ou avariada <br/>
                • produto avariado <br/>
                • produto em desacordo com o pedido <br/>
                • falta de acessórios <br/>
                </p>
                <p>Obs.: Se, ainda assim, os produto forem recebidos, por favor, entre em contato conosco pelo e-mail: sac@7luas.com em até 7 dias corridos</p>
                <p><strong>PARA EVITAR PROBLEMAS COM SUAS COMPRAS:</strong></p>
                <p>
                • Observe a descrição exata do produto que você quer comprar <br/>
                • O tamanho do produto (verifique se corresponde com a sua utilização) <br/>
                • A cor do produto (alguns produtos possuem cores sortidas e a foto no site é meramente ilustrativa podendo apresentar variações em sua tonalidade) <br/>
                </p>
                <p><strong>TROCA POR DEFEITO</strong></p>
                <p>
                • Até 7 (sete) dias corridos após a entrega. <br/>
                • Se o produto apresentar problemas de funcionamento passados 7 (sete) dias da data da retirada/entrega, deverá ser encaminhado ao nosso centro de distribuição, entrando em contato antecipado pelo e-mail: sac@7luas.com.
                </p>
            </div>
        </div>
      </div><!--row-->
    </div>
  </div>
</div>
  <hr class="space"></hr>
</div>
<!--END NOVIDADES-->

        <script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="/assets/js/vendor/bootstrap.min.js"></script>
        <script src="/assets/js/bootstrap-select.js"></script>
        <script src="/assets/js/defaults-pt_BR.js"></script>
        <script src="/assets/js/mousehold.js"></script>
        <script src="/assets/js/bootstrap-spinner.js"></script>
        <script src="/assets/js/bootstrap-spinner.js"></script>
        <script src="/assets/js/jquery.maskedinput.min.js"></script>

        <!--JS PRODUCT_DETAIL-->
        <script src="/assets/js/product_detail.js"></script>
        <!--END JS PRODUCT_DETAIL-->

<!--FOOTER-->
<?php include_once('includes/footer.php'); ?>
<!--END FOOTER-->

    </body>
</html>
