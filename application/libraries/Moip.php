<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Moip {

    //public $authToken = "Basic R1JSNDUxMUVLNzdOREhFS1U2UzlNRDFZVTFBWFZFWlQ6R1I0TFBEVk0xOE1UQ1Y0R1NKOEhOTEVTQlNXS0ZNMDZaTEZHUkQ3SQ==";
	public $authToken = "Basic R1pNM0c5OFZYV1NaWU05NTZJUEdVTkZBMlBLQllTNlQ6VlA3T0xVQlhEUkRLS0ZINkFTRVNWVUU0UkxBQ0Y3SVVEN1VVV0wxQw==";
	
    public function curl_post($endpoint, $postData)
    {
		//echo "<br/><br/>endpoint: " . $endpoint;
		
		$ch = curl_init($endpoint);
		
		$http_header = array(
			'Authorization: '.$this->authToken,
			'Content-Type: application/json'
		);
		
		//echo "<br/><br/>header: ";
		//print_r ($http_header);
        
        curl_setopt_array($ch, array(
			CURLOPT_POST => TRUE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_HTTPHEADER => $http_header,
			CURLOPT_POSTFIELDS => json_encode($postData)
		));
		
		//echo "<br/><br/>json: " . json_encode($postData);
		
		// Send the request
		$response = curl_exec($ch);
		
		// Check for errors
		if($response === FALSE){
			die(curl_error($ch));
		}
		
		// Decode the response
		$responseData = json_decode($response, TRUE);
		
		//echo "<pre>";
		//print_r($responseData);
		
		return $responseData;        
    }
    
    public function criar_pedido($postData)
    {
		//$endpoint = 'https://sandbox.moip.com.br/v2/orders';
		$endpoint = 'https://api.moip.com.br/v2/orders';
		$responseData = $this->curl_post($endpoint, $postData);
		
        return $responseData;
    }
    
    public function criar_pagamento($postData, $pedido)
    {
		//$endpoint = 'https://sandbox.moip.com.br/v2/orders/'.$pedido.'/payments';
		$endpoint = 'https://api.moip.com.br/v2/orders/'.$pedido.'/payments';
		$responseData = $this->curl_post($endpoint, $postData);
        
        return $responseData;
    }
	
    public function status_pagamento($postData)
    {
		//$endpoint = 'https://sandbox.moip.com.br/v2/webhooks/';
		$endpoint = 'https://api.moip.com.br/v2/webhooks/';
		$responseData = $this->curl_post($endpoint, $postData);
        
        return $responseData;
    }
	
    public function criar_notificacao($postData)
    {
		//$endpoint = 'https://sandbox.moip.com.br/v2/preferences/notifications';
		$endpoint = 'https://api.moip.com.br/preferences/v2/notifications';
		$responseData = $this->curl_post($endpoint, $postData);
        
        return $responseData;
    }	

}