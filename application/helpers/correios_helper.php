<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('correios_frete')){
  
  function correios_frete($servico, $cep_origem, $cep_destino, $peso, $altura='4', $largura='12', $comprimento='16', $valor='1.00'){
    
    /*
     * 41106 PAC
     * 40010 SEDEX
     * 40045 SEDEX a Cobrar
     * 40215 SEDEX 10
    */
    
    $ch = curl_init();
    
    $url = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem=".$cep_origem."&sCepDestino=".$cep_destino."&nVlPeso=".$peso."&nCdFormato=1&nVlComprimento=".$comprimento."&nVlAltura=".$altura."&nVlLargura=".$largura."&sCdMaoPropria=n&nVlValorDeclarado=".$valor."&sCdAvisoRecebimento=n&nCdServico=".$servico."&nVlDiametro=0&StrRetorno=xml";
    
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    
    $output = curl_exec($ch);
    
    curl_close($ch);
    
    $xml = simplexml_load_string($output);
    
    return number_format((float)$xml->cServico->Valor, 2, '.', '') . "|" . $xml->cServico->PrazoEntrega;
    
  }
  
}

if(!function_exists('correios_endereco')){
  
  function correios_endereco($cep){
    $resultado = @file_get_contents('http://republicavirtual.com.br/web_cep.php?cep='.urlencode($cep).'&formato=query_string');  
    if(!$resultado){  
        $resultado = "&resultado=0&resultado_txt=erro+ao+buscar+cep";  
    }  
    parse_str($resultado, $retorno);   
    return $retorno;  
  }   
  
}


/* End of file correios.php */
/* Location: ./application/helpers/correios.php */