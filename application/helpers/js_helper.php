<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('load_js')){
  
  function load_js($files)
  {
    if(!is_array($files))
    {
      return false;
    }
     
    $return = '';
    
    foreach($files as $js)
    {
      $return .= '<script src="' . base_url() . 'assets/js/' . $js . '"></script>' . "\n";
    }
    
    return $return;
  }
  
}

/* End of file js_helper.php */
/* Location: ./application/helpers/js_helper.php */