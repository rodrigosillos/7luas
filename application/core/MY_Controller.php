<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller {
  
  public $result_per_page = 72;
  public $cliente_sessao;
  public $usuario_sessao;
  public $frete_sessao;

  public function __construct()
  {
    ini_set("allow_url_fopen", 1);
    setlocale(LC_MONETARY, "pt_BR", "ptb");

    $this->load->database();
    $this->load->library(array('session', 'pagination', 'cart', 'email'));
    $this->load->helper(array('url', 'form', 'date', 'js'));
    
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['prev_link'] = '<img src="/assets/img/left-pag.png">';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['next_link'] = '<img src="/assets/img/right-pag.png">';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';		 
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';		 
    $config['first_link'] = '';
    $config['last_link'] = '';
    
    $this->pagination->initialize($config);
    
    $this->cliente_sessao = $this->session->userdata("cliente_sessao");
	$this->usuario_sessao = $this->session->userdata("usuario_sessao");
    $this->frete_sessao = $this->session->userdata("frete_sessao");
    
	//loja
	if($this->uri->segment(2)=='pagamento')
	{
      if(empty($this->cliente_sessao))
      {
        //$base_url = str_replace("http://", "https://", base_url());
		redirect(base_url() . 'login/redirect_pagamento');
      }	  
	}
	
	if($this->uri->segment(1)=='conta' AND $this->uri->segment(2)=='lista_desejos')
	{
      if(empty($this->cliente_sessao))
      {
        //$base_url = str_replace("http://", "https://", base_url());
		redirect(base_url() . 'login/redirect_desejos');
      }
	}	
	
    if($this->uri->segment(1)=='conta' OR
    $this->uri->segment(2)=='meus_pedidos' OR
    $this->uri->segment(2)=='meus_dados' OR
    $this->uri->segment(2)=='alterar_senha')
    {          
      if(empty($this->cliente_sessao))
      {
        //$base_url = str_replace("http://", "https://", base_url());
		redirect( base_url() . 'login/');
      }
    }
	
	//admin
    if($this->uri->segment(1)=='admin')
    {
	  if($this->uri->segment(2)=='dashboard' OR
	  $this->uri->segment(2)=='produtos' OR
	  $this->uri->segment(2)=='pedidos' OR
	  $this->uri->segment(2)=='clientes' OR
	  $this->uri->segment(2)=='banners' OR
	  $this->uri->segment(2)=='configuracoes')
      {
		if(empty($this->usuario_sessao))
		{
		  redirect('/admin/login/');
		}        
      }
    }
    
  }

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */